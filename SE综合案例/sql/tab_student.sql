/*
Navicat MySQL Data Transfer

Source Server         : jja第一阶段
Source Server Version : 50554
Source Host           : localhost:3306
Source Database       : 作业

Target Server Type    : MYSQL
Target Server Version : 50554
File Encoding         : 65001

Date: 2019-09-22 10:46:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tab_student
-- ----------------------------
DROP TABLE IF EXISTS `tab_student`;
CREATE TABLE `tab_student` (
  `student_on` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `gender` char(2) DEFAULT NULL,
  `brith` date DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`student_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_student
-- ----------------------------
