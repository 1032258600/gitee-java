/*
Navicat MySQL Data Transfer

Source Server         : jja第一阶段
Source Server Version : 50554
Source Host           : localhost:3306
Source Database       : 作业

Target Server Type    : MYSQL
Target Server Version : 50554
File Encoding         : 65001

Date: 2019-09-22 10:46:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tab_course
-- ----------------------------
DROP TABLE IF EXISTS `tab_course`;
CREATE TABLE `tab_course` (
  `course_no` int(255) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_score` float(255,0) NOT NULL,
  PRIMARY KEY (`course_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_course
-- ----------------------------

-- ----------------------------
-- Table structure for tab_score
-- ----------------------------
DROP TABLE IF EXISTS `tab_score`;
CREATE TABLE `tab_score` (
  `score_id` int(255) NOT NULL AUTO_INCREMENT,
  `student_no` varchar(255) NOT NULL,
  `course_no` int(255) NOT NULL,
  `score` float(255,0) NOT NULL,
  `pub_date` datetime NOT NULL,
  PRIMARY KEY (`score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_score
-- ----------------------------

-- ----------------------------
-- Table structure for tab_student
-- ----------------------------
DROP TABLE IF EXISTS `tab_student`;
CREATE TABLE `tab_student` (
  `student_on` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `gender` char(2) DEFAULT NULL,
  `brith` date DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`student_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_student
-- ----------------------------

-- ----------------------------
-- Table structure for tab_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tab_teacher`;
CREATE TABLE `tab_teacher` (
  `teacher_id` int(255) NOT NULL AUTO_INCREMENT,
  `teacher_no` varchar(255) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_teacher
-- ----------------------------
