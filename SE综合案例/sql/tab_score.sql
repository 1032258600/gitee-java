/*
Navicat MySQL Data Transfer

Source Server         : jja第一阶段
Source Server Version : 50554
Source Host           : localhost:3306
Source Database       : 作业

Target Server Type    : MYSQL
Target Server Version : 50554
File Encoding         : 65001

Date: 2019-09-22 10:46:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tab_score
-- ----------------------------
DROP TABLE IF EXISTS `tab_score`;
CREATE TABLE `tab_score` (
  `score_id` int(255) NOT NULL AUTO_INCREMENT,
  `student_no` varchar(255) NOT NULL,
  `course_no` int(255) NOT NULL,
  `score` float(255,0) NOT NULL,
  `pub_date` datetime NOT NULL,
  PRIMARY KEY (`score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_score
-- ----------------------------
