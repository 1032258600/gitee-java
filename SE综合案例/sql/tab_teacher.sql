/*
Navicat MySQL Data Transfer

Source Server         : jja第一阶段
Source Server Version : 50554
Source Host           : localhost:3306
Source Database       : 作业

Target Server Type    : MYSQL
Target Server Version : 50554
File Encoding         : 65001

Date: 2019-09-22 10:46:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tab_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tab_teacher`;
CREATE TABLE `tab_teacher` (
  `teacher_id` int(255) NOT NULL AUTO_INCREMENT,
  `teacher_no` varchar(255) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tab_teacher
-- ----------------------------
