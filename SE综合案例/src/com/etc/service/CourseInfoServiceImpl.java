package com.etc.service;

import java.util.List;

import com.etc.dao.CourseInfoDao;
import com.etc.dao.CourseInfoDaoImpl;
import com.etc.pojo.CourseInfo;

public class CourseInfoServiceImpl implements CourseInfoService {

	CourseInfoDao courseInfoDao = new CourseInfoDaoImpl();
	
	@Override
	public List<CourseInfo> getCourse() {
		
		
		return courseInfoDao.queryCourse();
	}

	@Override
	public List<CourseInfo> getCourseBySno(String studentNo) {
		
		return courseInfoDao.queryCourseBySno(studentNo);
	}

}
