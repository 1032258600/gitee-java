package com.etc.service;

import java.util.List;
import java.util.Map;

import com.etc.dao.ScoreInfoDao;
import com.etc.dao.ScoreInfoDaoImpl;
import com.etc.pojo.ScoreInfo;

public class ScoreInfoServiceImpl implements ScoreInfoService{

	ScoreInfoDao scoreInfoDao = new ScoreInfoDaoImpl();
	
	@Override
	public List<Map<String, Object>> getScoreDetailBySno(String studentNo) {
		
		return scoreInfoDao.queryScoreDetailBySno(studentNo);
	}

	@Override
	public List<Map<String, Object>> getScoreDetailByCname(String courseName) {
		
		return scoreInfoDao.queryScoreDetailByCname(courseName);
	}

	@Override
	public Map<String, Object> getScoreDetailBySnoAndCname(String studentNo, String courseName) {
		
		return scoreInfoDao.queryScoreDetailBySnoAndCname(studentNo, courseName);
	}

	@Override
	public List<Map<String, Object>> getScoreDetail() {
		
		return scoreInfoDao.queryScoreDetail();
	}

	@Override
	public int addScore(ScoreInfo scoreInfo) {
		
		return scoreInfoDao.insert(scoreInfo);
	}

}
