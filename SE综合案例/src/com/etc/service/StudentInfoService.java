package com.etc.service;
import java.util.ArrayList;
import java.util.List;

import com.etc.pojo.StudentInfo;

public interface StudentInfoService {
//	学生登录业务
	public StudentInfo login(String username, String password);
//	修改学生
	public StudentInfo  modifyInfo(StudentInfo  studentInfo);
//	注销学生
	public boolean delete(String studentNo);
//	注册学生
	public StudentInfo register(StudentInfo  studentInfo);
//	查询(根据学号)
	public StudentInfo getStudentNo(String studentNo);
//	查询(根据姓名)
	ArrayList<StudentInfo> getStudentLikeName(String studentNameKey);
// 所有学生
	public List<StudentInfo> getStuList();
	
}
