package com.etc.service;

import java.util.ArrayList;
import java.util.List;

import com.etc.dao.StudentInfoDao;
import com.etc.dao.StudentInfoDaoImpl;
import com.etc.dao.TeacherInfoDao;
import com.etc.dao.TeacherInfoDaoInfo;
import com.etc.pojo.TeacherInfo;


import com.etc.pojo.StudentInfo;

public class TecaherInfoServiceImpl implements TeacherInfoService {

	private TeacherInfoDao teacherInfoDao = new TeacherInfoDaoInfo();
	private StudentInfoDao studentInfoDao = new StudentInfoDaoImpl();
	
	@Override
	public TeacherInfo login(String teacher_no, String password) {
		
		return teacherInfoDao.queryByTeacherNoAndPassword(teacher_no, password);
	}
	
	
	
	
	@Override
	public StudentInfo getByStudentNo(String studentNo) {
		return studentInfoDao.queryByNo(studentNo);
	}
	
	@Override
	public List<StudentInfo> getStudentByName(String studentNameKey) {
		return studentInfoDao.queryByNameKey(studentNameKey);
	}
	
	@Override
	public boolean addStudent(StudentInfo studentInfo) {
		
		int n = studentInfoDao.Insert(studentInfo);
		return n > 0 ? true : false;
	}
	
	@Override
	public boolean updateById(StudentInfo studentInfo) {
		
		int n = studentInfoDao.Update(studentInfo);
		return n > 0 ? true : false;
	}
	
	
}
