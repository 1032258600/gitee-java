	package com.etc.service;

import java.util.List;

import com.etc.pojo.StudentInfo;
import com.etc.pojo.TeacherInfo;

public interface TeacherInfoService {
	
	public TeacherInfo login(String teacher_no, String password);
	
	public StudentInfo getByStudentNo(String studentNo);
	
	public List<StudentInfo> getStudentByName(String studentNameKey);
	
	public boolean addStudent(StudentInfo studentInfo);
	
	public boolean updateById(StudentInfo studentInfo);
}
