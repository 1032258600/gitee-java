package com.etc.service;
import java.util.ArrayList;
import java.util.List;

import com.etc.dao.StudentInfoDao;
import com.etc.dao.StudentInfoDaoImpl;
import com.etc.pojo.StudentInfo;



public class StudentInfoServiceImpl implements StudentInfoService{
private StudentInfoDao stuDao =new StudentInfoDaoImpl();

	@Override
	public StudentInfo login(String studentNo, String password) {
		
		return stuDao.Login(studentNo, password);
	}

	@Override
	public StudentInfo modifyInfo(StudentInfo studentInfo) {
		int n= stuDao.Update(studentInfo);
		if (n>0) {
			return stuDao.queryByNo(studentInfo.getStudentNo()) ;
		}
		return null;
	}

	@Override
	public boolean delete(String studentNo) {
		
		return stuDao.delete(studentNo) > 0 ? true : false;
	}

	@Override
	public StudentInfo register(StudentInfo studentInfo) {
		StudentInfo stu = stuDao.queryByStudentName(studentInfo.getStudentName());
		if (stu == null) { 
			stuDao.Insert(studentInfo);
			stu = stuDao.queryByStudentName(studentInfo.getStudentName());
			return stu; 
		}
		return null;
		
	}

	@Override
	public StudentInfo getStudentNo(String studentNo) {
		
		return stuDao.queryByNo(studentNo);
		
	}

	@Override
	public ArrayList<StudentInfo> getStudentLikeName(String studentNameKey) {
		
		return stuDao.queryByNameKey(studentNameKey);
	}

	@Override
	public List<StudentInfo> getStuList() {
		
		return stuDao.queryStuList();
	}

}
