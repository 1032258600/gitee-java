package com.etc.service;

import java.util.List;
import java.util.Map;

import com.etc.pojo.ScoreInfo;

public interface ScoreInfoService {

	
	public List<Map<String, Object>> getScoreDetailBySno(String studentNo);
	
	public List<Map<String, Object>> getScoreDetailByCname(String courseName);
	
	public Map<String, Object> getScoreDetailBySnoAndCname(String studentNo, String courseName);
	
	public List<Map<String, Object>> getScoreDetail();
	
	public int addScore(ScoreInfo scoreInfo);
	
}
