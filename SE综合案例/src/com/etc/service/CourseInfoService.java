package com.etc.service;

import java.util.List;

import com.etc.pojo.CourseInfo;

public interface CourseInfoService {

	public List<CourseInfo> getCourse();
	public List<CourseInfo> getCourseBySno(String studentNo);
}
