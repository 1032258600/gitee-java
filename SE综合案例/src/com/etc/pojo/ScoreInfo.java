package com.etc.pojo;

public class ScoreInfo {

	private Integer score_id;//成绩编号
	private String student_no;//学生学号
	private Integer course_no;//课程号
	private Float score;//成绩
	private String pub_date;//录入时间
	public ScoreInfo(Integer score_id, String student_no, Integer course_no, Float score, String pub_date) {
		super();
		this.score_id = score_id;
		this.student_no = student_no;
		this.course_no = course_no;
		this.score = score;
		this.pub_date = pub_date;
	}
	
	
	public ScoreInfo() {
		super();
	}

	public ScoreInfo( String student_no, Integer course_no, Float score) {
		super();
		this.student_no = student_no;
		this.course_no = course_no;
		this.score = score;
	}
	
	public Integer getScore_id() {
		return score_id;
	}


	public void setScore_id(Integer score_id) {
		this.score_id = score_id;
	}


	public String getStudent_no() {
		return student_no;
	}


	public void setStudent_no(String student_no) {
		this.student_no = student_no;
	}


	public Integer getCourse_no() {
		return course_no;
	}


	public void setCourse_no(Integer course_no) {
		this.course_no = course_no;
	}


	public Float getScore() {
		return score;
	}


	public void setScore(Float score) {
		this.score = score;
	}


	public String getPub_date() {
		return pub_date;
	}


	public void setPub_date(String pub_date) {
		this.pub_date = pub_date;
	}
	
	
}
