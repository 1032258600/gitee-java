package com.etc.pojo;

public class TeacherInfo {

	private Integer teacher_id;//教师编号
	private String teacher_no;//教工号
	private String teacher_name;//教师姓名
	private String password;//账户密码
	public TeacherInfo(Integer teacher_id, String teacher_no, String teacher_name, String password) {
		super();
		this.teacher_id = teacher_id;
		this.teacher_no = teacher_no;
		this.teacher_name = teacher_name;
		this.password = password;
	}
	
	public TeacherInfo() {
		super();
	}

	public Integer getTeacher_id() {
		return teacher_id;
	}

	public void setTeacher_id(Integer teacher_id) {
		this.teacher_id = teacher_id;
	}

	public String getTeacher_no() {
		return teacher_no;
	}

	public void setTeacher_no(String teacher_no) {
		this.teacher_no = teacher_no;
	}

	public String getTeacher_name() {
		return teacher_name;
	}

	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	
}
