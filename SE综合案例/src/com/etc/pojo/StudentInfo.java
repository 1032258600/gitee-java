package com.etc.pojo;



public class StudentInfo {
	private String studentNo;
	private String studentName;
	  private String gender;
	  private String brith;
	  private int status;
	  private String password;
	  
	  public StudentInfo() {
		super();
	}
	  
	
	

	public StudentInfo(String studentNo, String studentName, String gender, String brith, int status, String password) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.gender = gender;
		this.brith = brith;
		this.status = status;
		this.password = password;
	}




	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBrith() {
		return brith;
	}
	public void setBrith(String brith) {
		this.brith = brith;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	
	
  
  
}
