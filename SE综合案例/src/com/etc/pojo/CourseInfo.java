package com.etc.pojo;

public class CourseInfo {

	private Integer course_no;//课程编号
	private String course_name;//课程名
	private Float course_score;//学分 
	public CourseInfo(Integer course_no, String course_name, Float course_score) {
		super();
		this.course_no = course_no;
		this.course_name = course_name;
		this.course_score = course_score;
	}
	
	public CourseInfo() {
		
	}

	public Integer getCourse_no() {
		return course_no;
	}

	public void setCourse_no(Integer course_no) {
		this.course_no = course_no;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public Float getCourse_score() {
		return course_score;
	}

	public void setCourse_score(Float course_score) {
		this.course_score = course_score;
	}
	
	
	
}
