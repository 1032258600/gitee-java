package com.etc.dao;

import java.util.List;
import java.util.Map;
import com.etc.pojo.ScoreInfo;


public interface ScoreInfoDao {

	public List<Map<String,Object>> queryScoreDetail();
	
	public List<Map<String,Object>> queryScoreDetailBySno(String studentNo);
	
	public List<Map<String,Object>> queryScoreDetailByCname(String courseName);
	
	public Map<String,Object> queryScoreDetailBySnoAndCname(String studentNo,String courseName);
	
	public int insert(ScoreInfo scoreInfo);
	
	
	
	
	
	
}
