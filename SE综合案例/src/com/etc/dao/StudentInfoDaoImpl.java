package com.etc.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.etc.dao.StudentInfoDao;
import com.etc.pojo.StudentInfo;
import com.etc.utils.DBUtil;

public class StudentInfoDaoImpl implements StudentInfoDao{

	@Override
	public StudentInfo Login(String studentNo, String password) {
		  StudentInfo studentInfo = null;
		    ResultSet rs = DBUtil.doQuery("select * from tab_student where student_no=? and password=?" , studentNo,password);		
	   try {
		     while (rs.next()) {			
			    String gender = rs.getString("gender");
			    String brith = rs.getString("brith");
				String studentName = rs.getString("student_name");
				int status = rs.getInt("status");
				studentInfo = new StudentInfo(studentNo, studentName, gender, brith, status, password);
		      }
	         } catch (SQLException e) {
		    // TODO Auto-generated catch block
		        e.printStackTrace();
	}				    
		   
	          return studentInfo;
	}

	@Override
	public int Update(StudentInfo studentInfo) {
		return DBUtil.doUpdate("update tab_student set student_name=?,gender=?,brith=?,password=?,status=?",studentInfo.getStudentName(),studentInfo.getGender(),studentInfo.getBrith(),studentInfo.getStatus(),studentInfo.getPassword());
	}

	@Override
	public int delete(String studentNo) {
		return DBUtil.doUpdate("delete from tab_student where student_no = ?", studentNo);
	}

	@Override
	public int Insert(StudentInfo studentInfo) {
		return DBUtil.doUpdate("insert into tab_student values(?,?,?,?,?,?)", studentInfo.getStudentNo(),studentInfo.getStudentName(),studentInfo.getGender(),studentInfo.getBrith(),studentInfo.getStatus(),studentInfo.getPassword());
	}
	@Override
	public StudentInfo queryByNo(String studentNo) {
		StudentInfo studentInfo = null;
		ResultSet resultSet = DBUtil.doQuery("select * from tab_student where student_no = ? ", studentNo);

		try {
			while (resultSet.next()) {
				String studentName = resultSet.getString("student_name");
				String gender = resultSet.getString("gender");
				String brith = resultSet.getString("brith");
				String password = resultSet.getString("password");
				int status = resultSet.getInt("status");
				studentInfo = new StudentInfo(studentNo, studentName, gender, brith, status, password);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return studentInfo;
	}

	
	

	@Override
	public ArrayList<StudentInfo> queryByNameKey(String studentNameKey) {
		ArrayList<StudentInfo> list =new ArrayList<>();
		ResultSet resultSet =DBUtil.doQuery("select * from tab_student where student_name = ? ", "%"+studentNameKey+"%");
		try {
			while (resultSet.next()) {
			String studentNo=resultSet.getString("student_no");
			String studentName = resultSet.getString("student_name");
			String gender = resultSet.getString("gender");
			String brith = resultSet.getString("brith");
			String password = resultSet.getString("password");
			int status = resultSet.getInt("status");
			list.add(new StudentInfo(studentNo, studentName, gender, brith, status, password));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}							
		return list;
	}

	@Override
	public StudentInfo queryByStudentName(String studentName) {
		StudentInfo studentInfo = null;
		ResultSet resultSet = DBUtil.doQuery("select * from tab_student where student_name = ?", studentName);

		try {
			while (resultSet.next()) {
				String studentNo=resultSet.getString("student_no");
				String gender = resultSet.getString("gender");
				String brith = resultSet.getString("brith");
				String password = resultSet.getString("password");
				int status = resultSet.getInt("status");
				studentInfo = new StudentInfo(studentNo, studentName, gender, brith, status, password);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return studentInfo;
	}

	@Override
	public List<StudentInfo> queryStuList() {
		List<StudentInfo> list = new ArrayList<StudentInfo>();
		ResultSet rs = DBUtil.doQuery("select student_no,student_name,gender,brith,password,status from tab_student ");
		try {
			while (rs.next()) {
				String studentNo=rs.getString("student_no");
				String studentName = rs.getString("student_name");
				String gender = rs.getString("gender");
				String brith = rs.getString("brith");
				String password = rs.getString("password");
				int status = rs.getInt("status");
				list.add(new StudentInfo(studentNo, studentName, gender, brith, status, password));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
