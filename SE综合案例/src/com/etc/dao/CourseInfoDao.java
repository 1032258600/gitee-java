package com.etc.dao;

import java.util.List;

import com.etc.pojo.CourseInfo;

public interface CourseInfoDao {

	public List<CourseInfo> queryCourse();
	public List<CourseInfo> queryCourseBySno(String studentNo);
}
