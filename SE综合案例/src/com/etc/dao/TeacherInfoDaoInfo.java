package com.etc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.etc.pojo.TeacherInfo;
import com.etc.utils.DBUtil;

public class TeacherInfoDaoInfo implements TeacherInfoDao {
	
	@Override
	public TeacherInfo queryByTeacherNoAndPassword(String teacher_no, String password) {
		TeacherInfo teacherInfo = null;
		ResultSet rs = DBUtil.doQuery("select teacher_id,teacher_no,teacher_name from tab_teacher where teacher_no = ? and password = ?", teacher_no, password);
		
		try {
			while (rs.next()) {
				
				int teacher_id = rs.getInt("teacher_id");
				String teacher_name = rs.getString("teacher_name");
				teacherInfo = new TeacherInfo(teacher_id, teacher_no, teacher_name, null);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return teacherInfo;
	}

}
