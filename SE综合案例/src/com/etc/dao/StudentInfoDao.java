package com.etc.dao;
import java.util.ArrayList;
import java.util.List;

import com.etc.pojo.StudentInfo;
public interface StudentInfoDao {
//	学生登录
   public StudentInfo Login(String studentNo,String password);
//   修改学生
   public int Update(StudentInfo studentInfo);
   // 注销学生
	public int delete(String studentNo);
//	添加学生
	public int Insert(StudentInfo student);
//	根据学号查询学生
	public StudentInfo queryByNo(String studentNo);
//	根据姓名关键字查询学生
	public ArrayList<StudentInfo> queryByNameKey(String studentNameKey);
//	根据姓名查询学生
	public StudentInfo queryByStudentName(String studentName);
	
// 所有学生
	public List<StudentInfo> queryStuList();

	
}
