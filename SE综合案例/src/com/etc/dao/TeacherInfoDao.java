package com.etc.dao;

import com.etc.pojo.TeacherInfo;

public interface TeacherInfoDao {

	/**
	 * 根据教工号和密码进行查询
	 * @param teacher_no 教工号
	 * @param password 账户密码
	 * @return TeacherInfo
	 */
	public TeacherInfo queryByTeacherNoAndPassword(String teacher_no, String password);
}
