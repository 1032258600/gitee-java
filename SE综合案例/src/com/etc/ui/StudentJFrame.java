package com.etc.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.etc.pojo.StudentInfo;
import com.etc.service.ScoreInfoService;
import com.etc.service.ScoreInfoServiceImpl;
import com.sun.glass.ui.Size;

public class StudentJFrame {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTable Scoretableinfo;
	
	ScoreInfoService scoreInfoService = new ScoreInfoServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					StudentJFrame window = new StudentJFrame(new StudentInfo("500", "**", "男", "11月2日", 0, "11111"));
					
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StudentJFrame(StudentInfo studentInfo) {
		initialize(studentInfo);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(StudentInfo studentInfo) {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("宋体", Font.PLAIN, 22));
		frame.setBounds(100, 100, 637, 443);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel StudentNo = new JLabel("学号：");
		StudentNo.setFont(new Font("宋体", Font.PLAIN, 22));
		StudentNo.setBounds(15, 29, 73, 31);
		frame.getContentPane().add(StudentNo);
		
		textField = new JTextField();
		textField.setBounds(82, 32, 163, 27);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField.setText(studentInfo.getStudentNo());
		
		JLabel StudentName = new JLabel("姓名：");
		StudentName.setFont(new Font("宋体", Font.PLAIN, 22));
		StudentName.setBounds(15, 90, 81, 21);
		frame.getContentPane().add(StudentName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(82, 90, 163, 27);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_1.setText(studentInfo.getStudentName());
		
		JLabel ScoreList = new JLabel("个人课程成绩列表");
		ScoreList.setFont(new Font("宋体", Font.PLAIN, 22));
		ScoreList.setBounds(220, 149, 197, 21);
		frame.getContentPane().add(ScoreList);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(82, 185, 475, 203);
		frame.getContentPane().add(scrollPane);
		//获取学号和姓名
		StudentNo.setText(studentInfo.getStudentNo());
		StudentName.setText(studentInfo.getStudentName());
		
		Scoretableinfo = new JTable();
		
		List<Map<String, Object>> scoreDetail = scoreInfoService.getScoreDetail();
		
		Object[][] data = new Object[scoreDetail.size()][3];
		for (int i = 0; i < scoreDetail.size(); i++) {
			
			Map<String, Object> hashMap = scoreDetail.get(i); 
			data[i][0] = hashMap.get("course_name");
			data[i][1] = hashMap.get("course_score");
			data[i][2] = hashMap.get("score");
		}
		
		Scoretableinfo.setFont(new Font("宋体", Font.PLAIN, 22));
		String[] cols = {"课程名", "课程学分", "成绩"};
		Scoretableinfo.setModel(new DefaultTableModel(data,cols));
		Scoretableinfo.getColumnModel().getColumn(0).setMinWidth(50);
		scrollPane.setViewportView(Scoretableinfo);
		//窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		//窗口不允许调整大小
		frame.setResizable(false);
		//设置窗口名称
		frame.setTitle("学生个人页面");
		
		frame.setVisible(true);
	}
}
