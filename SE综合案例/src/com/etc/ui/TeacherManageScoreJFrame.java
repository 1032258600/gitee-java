package com.etc.ui;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.etc.pojo.CourseInfo;
import com.etc.pojo.ScoreInfo;
import com.etc.pojo.StudentInfo;
import com.etc.service.CourseInfoService;
import com.etc.service.CourseInfoServiceImpl;
import com.etc.service.ScoreInfoService;
import com.etc.service.ScoreInfoServiceImpl;
import com.etc.service.StudentInfoService;
import com.etc.service.StudentInfoServiceImpl;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;

public class TeacherManageScoreJFrame {

	private JFrame frame;
	private JTextField textStuNo;
	private JLabel Course;
	private JButton btnClear;
	private JButton btnquery;
	private JScrollPane scrollPane;
	private JTable tabStudentCourseScore;
	private JTabbedPane AddScore;
	private JPanel addCourseScore;
	private JLabel lblStudentNo;
	private JTextField textAddStudentNo;
	private JLabel lblCourse;
	private JComboBox comboBox;
	private JLabel lblCourseScore;
	private JTextField textScore;
	private JButton btnAdd;
	
	ScoreInfoService scoreInfoService = new ScoreInfoServiceImpl();
	CourseInfoService courseInfoService = new CourseInfoServiceImpl();
	StudentInfoService studentInfoService = new StudentInfoServiceImpl();
	List<CourseInfo> clist;
	List<Map<String, Object>> scoreDetail;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherManageScoreJFrame window = new TeacherManageScoreJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TeacherManageScoreJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 750, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("教师成绩管理窗口");

		JLabel StuNo = new JLabel("学号");
		StuNo.setBounds(15, 25, 81, 21);
		frame.getContentPane().add(StuNo);

		textStuNo = new JTextField();
		textStuNo.setBounds(68, 22, 96, 27);
		frame.getContentPane().add(textStuNo);
		textStuNo.setColumns(10);

		Course = new JLabel("课程：");
		Course.setBounds(194, 25, 81, 21);
		frame.getContentPane().add(Course);

		// 需要获取课程列表项
		List<CourseInfo> courseList = courseInfoService.getCourse();
		String[] courseItem = new String[courseList.size() + 1];
		courseItem[0] = "全部课程";
		for (int i = 0; i < courseList.size(); i++) {
			courseItem[i+1] = courseList.get(i).getCourse_name();
		}
		JComboBox SelectCourse = new JComboBox();
		SelectCourse.setModel(new DefaultComboBoxModel(courseItem));

		SelectCourse.setBounds(256, 22, 111, 27);
		frame.getContentPane().add(SelectCourse);

		btnClear = new JButton("清除搜索...");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnClear.setBounds(405, 21, 138, 29);
		frame.getContentPane().add(btnClear);

		btnquery = new JButton("查询");
		
		btnquery.setBounds(575, 21, 123, 29);
		frame.getContentPane().add(btnquery);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(54, 75, 593, 211);
		frame.getContentPane().add(scrollPane);

		tabStudentCourseScore = new JTable();
		
		scoreDetail = scoreInfoService.getScoreDetail();
		
		showscoreList(scoreDetail);
		
		AddScore = new JTabbedPane(JTabbedPane.TOP);
		AddScore.setBounds(54, 325, 593, 172);
		frame.getContentPane().add(AddScore);
		
		addCourseScore = new JPanel();
		AddScore.addTab("录入成绩", null, addCourseScore, null);
		addCourseScore.setLayout(null);
		
		lblStudentNo = new JLabel("学号：");
		lblStudentNo.setBounds(53, 15, 63, 21);
		addCourseScore.add(lblStudentNo);
		
		textAddStudentNo = new JTextField();
		textAddStudentNo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
					String sno = textAddStudentNo.getText().trim();
					StudentInfo stuInfo = studentInfoService.getStudentNo(sno);
					
					if(stuInfo != null) {
						if(!"".equals(sno)) {
							clist = courseInfoService.getCourseBySno(sno);
							String[] citem = new String[clist.size() + 1];
							citem[0] = "请选择…";
							for (int i = 0; i < clist.size(); i++) {
								citem[i+1] = clist.get(i).getCourse_name();
							}
							comboBox.setModel(new DefaultComboBoxModel(citem));
						}
							
					}else {
							JOptionPane.showMessageDialog(null, "学生不存在，请添加学生！");
					}
					
					
			}
		});
		textAddStudentNo.setBounds(163, 12, 219, 27);
		addCourseScore.add(textAddStudentNo);
		textAddStudentNo.setColumns(10);
		
		lblCourse = new JLabel("课程：");
		lblCourse.setBounds(53, 64, 81, 21);
		addCourseScore.add(lblCourse);
		comboBox = new JComboBox();
		
		comboBox.setBounds(163, 54, 219, 27);
		comboBox.setModel(new DefaultComboBoxModel(new String[]{"请选择…"}));
		addCourseScore.add(comboBox);
		
		lblCourseScore = new JLabel("成绩：");
		lblCourseScore.setBounds(53, 101, 81, 21);
		addCourseScore.add(lblCourseScore);
		
		textScore = new JTextField();
		textScore.setBounds(163, 98, 219, 27);
		addCourseScore.add(textScore);
		textScore.setColumns(10);
		
		btnAdd = new JButton("录入");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String student_no = textAddStudentNo.getText().trim();
					
						Integer i = comboBox.getSelectedIndex() - 1;
						Integer course_no = clist.get(i).getCourse_no();
						Float score = Float.parseFloat(textScore.getText().trim());
						int n = scoreInfoService.addScore(new ScoreInfo(student_no, course_no, score));
						if(n > 0) {
							JOptionPane.showMessageDialog(null, "添加成绩成功！");
						}else {
							JOptionPane.showMessageDialog(null, "添加成绩失败！");
						}
					
			}
		});
		btnAdd.setBounds(437, 53, 123, 29);
		addCourseScore.add(btnAdd);
		
		btnquery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1 获取用户输入的学号
				String studentNo = textStuNo.getText().trim();
				// 2 获取用户选中的课程编号
				String courseName = "";
				// 如何知道当前选中的是哪一个下拉菜单项目
				int selectedIndex = SelectCourse.getSelectedIndex(); 
				if (selectedIndex==0) {// 选中"全部课程"
					courseName ="";
				}else {
					// 从课程列表集合中获取对应的对象
					courseName = SelectCourse.getSelectedItem().toString();
				}
				
				if(!"".equals(studentNo) && "".equals(courseName)) {
					List<Map<String, Object>> scoreDetail = scoreInfoService.getScoreDetailBySno(studentNo);
					showscoreList(scoreDetail);
				}else if(!"".equals(studentNo) && !"".equals(courseName)) {
					
					Map<String, Object> scoreDetail = scoreInfoService.getScoreDetailBySnoAndCname(studentNo, courseName);
					List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
					list.add(scoreDetail);
					showscoreList(list);
				}else if("".equals(studentNo) && !"".equals(courseName)){
					List<Map<String, Object>> scoreDetail = scoreInfoService.getScoreDetailByCname(courseName);
					showscoreList(scoreDetail);
				}else {
					showscoreList(scoreDetail);
				}
			}
		});
		
		frame.setVisible(true);
		
	}
	
	/**
	 * 为表格填充数据
	 */
	
	public void showscoreList(List<Map<String, Object>> scoreDetail) {
		
			Object[][] data = new Object[scoreDetail.size()][5];
		
			for (int i = 0; i < scoreDetail.size(); i++) {
			
			Map<String, Object> hashMap = scoreDetail.get(i); 
			data[i][0] = hashMap.get("score_id");
			data[i][1] = hashMap.get("student_no");
			data[i][2] = hashMap.get("student_name");
			data[i][3] = hashMap.get("course_name");
			data[i][4] = hashMap.get("score");
		}
			
			String[] cols = { "成绩编号", "学号", "学生姓名", "课程名", "成绩" };
			tabStudentCourseScore.setModel(new DefaultTableModel(data, cols));
			scrollPane.setViewportView(tabStudentCourseScore);
	}
}
