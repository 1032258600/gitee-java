package com.etc.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.etc.dao.StudentInfoDao;
import com.etc.dao.StudentInfoDaoImpl;
import com.etc.pojo.StudentInfo;
import com.etc.pojo.TeacherInfo;
import com.etc.service.TeacherInfoService;
import com.etc.service.TecaherInfoServiceImpl;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class LoginWindowJFrame {

	private JFrame frame;
	private JTextField textField;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPasswordField passwordField;
    
	TeacherInfoService teacherInfoService = new TecaherInfoServiceImpl();
	StudentInfoDao studentInfoDao = new StudentInfoDaoImpl();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindowJFrame window = new LoginWindowJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginWindowJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 545, 431);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		//窗口不允许调整大小
		frame.setResizable(false);
		//设置窗口名称
		frame.setTitle("登录窗口");
		
		JLabel StudentManage = new JLabel("学生信息管理系统");
		StudentManage.setBounds(192, 18, 221, 45);
		frame.getContentPane().add(StudentManage);
		
		JLabel stuIdorTeaNo = new JLabel("学号/工号：");
		stuIdorTeaNo.setBounds(30, 81, 112, 21);
		frame.getContentPane().add(stuIdorTeaNo);
		
		textField = new JTextField();
		textField.setBounds(149, 78, 221, 27);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel Password = new JLabel("密码：");
		Password.setBounds(30, 133, 81, 21);
		frame.getContentPane().add(Password);
		
		JLabel Role = new JLabel("角色：");
		Role.setBounds(30, 186, 81, 21);
		frame.getContentPane().add(Role);
		
		JRadioButton btnStu = new JRadioButton("学生");
		buttonGroup.add(btnStu);
		btnStu.setBounds(149, 182, 93, 29);
		frame.getContentPane().add(btnStu);
		
		JRadioButton btnTea = new JRadioButton("教师管理员");
		buttonGroup.add(btnTea);
		btnTea.setBounds(254, 182, 209, 29);
		frame.getContentPane().add(btnTea);
		
		JButton btnLogin = new JButton("登录");
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String username = textField.getText().trim();
				
				char[] p = passwordField.getPassword();
				
				String password = new String(p);
				if(btnTea.isSelected()) {
					TeacherInfo teacherInfo = teacherInfoService.login(username, password);
					
					if (teacherInfo != null) {
						JOptionPane.showMessageDialog(null, "登录成功");
						
						TeacherManageStudentJFrame window = new TeacherManageStudentJFrame();
						
						frame.dispose();
					}else {
						JOptionPane.showMessageDialog(null, "登录失败,用户名或密码错误");
					}
				}else {
					
					StudentInfo studentInfo = studentInfoDao.Login(username, password);
					
					if (studentInfo != null) {
						JOptionPane.showMessageDialog(null, "登录成功");
						
						StudentJFrame window = new StudentJFrame(studentInfo);
						
						frame.dispose();
					}else {
						JOptionPane.showMessageDialog(null, "登录失败,用户名或密码错误");
					}
				}
				
				
				
			}
		});
		btnLogin.setBounds(149, 253, 221, 29);
		frame.getContentPane().add(btnLogin);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(149, 130, 221, 27);
		frame.getContentPane().add(passwordField);
		
		
		frame.setVisible(true);
	}
}
