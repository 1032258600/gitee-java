package com.etc.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.etc.pojo.StudentInfo;
import com.etc.service.StudentInfoService;
import com.etc.service.StudentInfoServiceImpl;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TeacherManageStudentJFrame {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;
	private JTextField textStudentNo;
	private JTextField textStudentName;
	private JTextField textBirthDate;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textUpdateStuName;
	private JTextField textUpdateBirthDate;
	private JPasswordField textUpdatePassword;
	private JScrollPane scrollPane;

	StudentInfoService studentInfoService = new StudentInfoServiceImpl();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherManageStudentJFrame window = new TeacherManageStudentJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TeacherManageStudentJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 871, 465);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel AccordingStuNo = new JLabel("按学号：");
		AccordingStuNo.setBounds(33, 15, 81, 21);
		frame.getContentPane().add(AccordingStuNo);

		textField = new JTextField();
		textField.setBounds(109, 12, 109, 27);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel AccordingNameKey = new JLabel("按姓名关键字：");
		AccordingNameKey.setBounds(267, 15, 139, 21);
		frame.getContentPane().add(AccordingNameKey);

		textField_1 = new JTextField();
		textField_1.setBounds(394, 12, 96, 27);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		JButton btnQuery = new JButton("查询");
		btnQuery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 获取输入的学号
				String studentNo = textField.getText().trim();
				// 获取输入的姓名关键字
				String key = textField_1.getText().trim();
				
				if(!"".equals(studentNo) && "".equals(key)) { // 只填写学号 没有填写关键字
					StudentInfo studentInfo = studentInfoService.getStudentNo(studentNo);
					
						if(studentInfo != null) {
							// 填充表格数据
							List<StudentInfo> stulist = new ArrayList<>();
							stulist.add(studentInfo);
							showStuList(stulist);
						}else {
							JOptionPane.showMessageDialog(null, "查无此人");
						}
				}else if("".equals(studentNo) && !"".equals(key)) { // 没有填写学号 只填写关键字
					List<StudentInfo> stulist = studentInfoService.getStudentLikeName(key);
					
					if(stulist.size() > 0) {
						// 填充表格数据
						showStuList(stulist);
					}else {
						JOptionPane.showMessageDialog(null, "查无相关数据");
					}
				}else {
					//查所有
					List<StudentInfo> stuList = studentInfoService.getStuList();
					showStuList(stuList);
				}
				
			}
		});
		
		
		
		btnQuery.setBounds(538, 11, 123, 29);
		frame.getContentPane().add(btnQuery);

		JButton ScoreManage = new JButton("进入成绩管理");
		ScoreManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new TeacherManageScoreJFrame();
				frame.dispose();
			}
		});
		ScoreManage.setBounds(689, 11, 161, 29);
		frame.getContentPane().add(ScoreManage);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(58, 62, 749, 137);
		frame.getContentPane().add(scrollPane);

		table = new JTable();

		List<StudentInfo> stuList = studentInfoService.getStuList();
		showStuList(stuList);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(58, 246, 749, 164);
		frame.getContentPane().add(tabbedPane);

		JPanel AddStudent = new JPanel();
		tabbedPane.addTab("新增学生", null, AddStudent, null);
		AddStudent.setLayout(null);

		JLabel StudentNo = new JLabel("学号：");
		StudentNo.setBounds(15, 15, 81, 21);
		AddStudent.add(StudentNo);

		textStudentNo = new JTextField();
		
		textStudentNo.setBounds(70, 12, 96, 27);
		AddStudent.add(textStudentNo);
		textStudentNo.setColumns(10);

		JLabel StudentName = new JLabel("学生姓名：");
		StudentName.setBounds(211, 15, 103, 21);
		AddStudent.add(StudentName);

		textStudentName = new JTextField();
		textStudentName.setBounds(303, 12, 96, 27);
		AddStudent.add(textStudentName);
		textStudentName.setColumns(10);

		JLabel gender = new JLabel("性别：");
		gender.setBounds(446, 15, 81, 21);
		AddStudent.add(gender);

		JRadioButton btnMan = new JRadioButton("男");
		buttonGroup.add(btnMan);
		btnMan.setBounds(531, 15, 75, 21);
		AddStudent.add(btnMan);

		JRadioButton btnWoman = new JRadioButton("女");
		buttonGroup.add(btnWoman);
		btnWoman.setBounds(613, 11, 75, 29);
		AddStudent.add(btnWoman);

		JLabel BirthDate = new JLabel("出生日期：");
		BirthDate.setBounds(15, 69, 110, 21);
		AddStudent.add(BirthDate);

		textBirthDate = new JTextField();
		textBirthDate.setBounds(113, 66, 119, 27);
		AddStudent.add(textBirthDate);
		textBirthDate.setColumns(10);

		JLabel Format = new JLabel("格式：2008-01-01");
		Format.setBounds(261, 69, 144, 21);
		AddStudent.add(Format);

		JLabel InitialPassword = new JLabel("密码：123456");
		InitialPassword.setBounds(446, 69, 119, 21);
		AddStudent.add(InitialPassword);

		JButton btnAddStudent = new JButton("添加学生");
		
		btnAddStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				// 获取新增表单中输入的数据
				String studentNo = textStudentNo.getText().trim();
				String studentName = textStudentName.getText().trim();
				String gender;
				if (btnMan.isSelected()) {
					gender = "男";
				} else {
					gender = "女";
				}
				
				String birth = textBirthDate.getText().trim();
				
				// 输入合法
				if(!"".equals(studentNo) && !"".equals(studentName) && !"".equals(studentName)) {
					char[] p = textUpdatePassword.getPassword();

					String password = new String(p);

					StudentInfo studentInfo = studentInfoService.register(new StudentInfo(studentNo, studentName, gender, birth, 0, password));
					if (studentInfo != null) {
						JOptionPane.showMessageDialog(null, "新增学生成功");
					} else {
						JOptionPane.showMessageDialog(null, "新增失败");
					}
				}else {
					JOptionPane.showMessageDialog(null, "请输入完整的数据");
				}

				
			}
		});
		btnAddStudent.setBounds(592, 65, 123, 29);
		AddStudent.add(btnAddStudent);

		//对学号做失去焦点事件
		textStudentNo.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				
				String studentNo = textStudentNo.getText().trim();
				StudentInfo studentInfo = studentInfoService.getStudentNo(studentNo);
				
				if(studentInfo != null) {
						JOptionPane.showMessageDialog(null, "学号已经存在，请重新输入");
						btnAddStudent.setEnabled(false);
					}else {
						btnAddStudent.setEnabled(true);
					}
			}
		});
		
		JPanel UpdateStudent = new JPanel();
		tabbedPane.addTab("修改学生", null, UpdateStudent, null);
		UpdateStudent.setLayout(null);

		JLabel StuNo = new JLabel("学号：");
		StuNo.setBounds(15, 15, 81, 21);
		UpdateStudent.add(StuNo);

		JLabel stuName = new JLabel("学生姓名：");
		stuName.setBounds(315, 15, 115, 21);
		UpdateStudent.add(stuName);

		textUpdateStuName = new JTextField();
		textUpdateStuName.setBounds(421, 12, 155, 27);
		UpdateStudent.add(textUpdateStuName);
		textUpdateStuName.setColumns(10);

		JLabel UpdateGender = new JLabel("性别：");
		UpdateGender.setBounds(15, 51, 81, 21);
		UpdateStudent.add(UpdateGender);

		JRadioButton SelectMan = new JRadioButton("男");
		buttonGroup.add(SelectMan);
		SelectMan.setBounds(71, 47, 81, 29);
		UpdateStudent.add(SelectMan);

		JRadioButton SelectWoman = new JRadioButton("女");
		buttonGroup.add(SelectWoman);
		SelectWoman.setBounds(157, 47, 81, 29);
		UpdateStudent.add(SelectWoman);

		JLabel UpdateBirthDate = new JLabel("出生日期：");
		UpdateBirthDate.setBounds(315, 51, 115, 21);
		UpdateStudent.add(UpdateBirthDate);

		textUpdateBirthDate = new JTextField();
		textUpdateBirthDate.setBounds(421, 51, 155, 27);
		UpdateStudent.add(textUpdateBirthDate);
		textUpdateBirthDate.setColumns(10);

		textUpdatePassword = new JPasswordField();
		textUpdatePassword.setBounds(81, 87, 112, 27);
		UpdateStudent.add(textUpdatePassword);

		JLabel UpdatePassword = new JLabel("密码：");
		UpdatePassword.setBounds(15, 90, 81, 21);
		UpdateStudent.add(UpdatePassword);
		
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				// 获取当前表格中点中的行
				int selectedRow = table.getSelectedRow();
				// 根据点中的行获取该行的每个列 
				String studentNo = (String) table.getValueAt(selectedRow, 0);
				String studentName = (String) table.getValueAt(selectedRow, 1);
				String gender = (String) table.getValueAt(selectedRow, 2);
				String birth = (String) table.getValueAt(selectedRow, 3);
				String password = (String) table.getValueAt(selectedRow, 4);
				
				// 将取到的值赋值到指定的位置上
				
				stu_no.setText(studentNo);
				textUpdateStuName.setText(studentName);
				textUpdateBirthDate.setText(birth);
				textUpdatePassword.setText(password);
				if ("男".equals(gender)) {
					SelectMan.setSelected(true);
				}else {
					SelectWoman.setSelected(true);
				}
				
			}
		});

		JButton btnUpdateStudent = new JButton("修改学生");
		btnUpdateStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String studentNo = textStudentNo.getText();
				String studentName = textStudentName.getText().trim();
				String gender;
				if (btnMan.isSelected()) {
					gender = "男";
				} else {
					gender = "女";
				}
				
				String birth = textBirthDate.getText().trim();

				char[] p = textUpdatePassword.getPassword();

				String password = new String(p);

				studentInfoService.register(new StudentInfo(studentNo, studentName, gender, birth, 0, password));
			}
		});
		btnUpdateStudent.setBounds(290, 86, 123, 29);
		UpdateStudent.add(btnUpdateStudent);

		JButton btnDeletestudent = new JButton("注销学生");
		btnDeletestudent.setBounds(499, 86, 123, 29);
		UpdateStudent.add(btnDeletestudent);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("教师管理主页面—学生管理");

		// 显示界面
		frame.setVisible(true);
	}

	/**
	 * 为表格填充数据
	 */
	
	public void showStuList(List<StudentInfo> stuList) {
		
		Object[][] data = new Object[stuList.size()][6];

		for (int i = 0; i < stuList.size(); i++) {

			StudentInfo stu = stuList.get(i);
			data[i][0] = stu.getStudentNo();
			data[i][1] = stu.getStudentName();
			data[i][2] = stu.getGender();
			data[i][3] = stu.getBrith();
			data[i][4] = stu.getPassword();
			data[i][5] = stu.getStatus();
		}
		

		String[] cols = { "学生学号", "学生姓名", "性别", "出生日期", "密码", "状态" };
		table.setModel(new DefaultTableModel(data, cols));
		scrollPane.setViewportView(table);
	}
	
	
}
