package part07抽象;
/**
 * 抽象类
 * 一个类中如果一旦有了抽象方法，那么这个类就必须声明抽象类
 * 抽象类中既可以普通方法，又可以有抽象方法。
 * 抽象类中可以没有抽象方法。
 * 抽象类不能被实例化，只能被继承，由子类实例化
 * 一个类如果继承抽象类，那么该类必须实现父类抽象类中的所有的抽象方法。
 * 如果该类不实现父类中的抽象方法，那么该类也必须定义为抽象类，也不能实例化。
 * @author Administrator
 *
 */
public abstract class 抽象类 {
   
	/**
	 * 抽象方法 用abstract关键字修饰的方法，没有方法体。
	 */
	 // 在某些情况下，父类无法(或者没有必要)提高被覆盖方法的具体实现，那么就可以将此方法声明为抽象方法
     public abstract void play();
     public abstract void play2();
    
     // 抽象类有构造方法
     public 抽象类() {
    	 
     }
     /**
      * 普通方法
      */
     public void show() {
		System.out.println("抽象类中的普通方法");
	}
}
