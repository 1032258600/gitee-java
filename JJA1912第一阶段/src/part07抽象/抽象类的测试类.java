package part07抽象;

public class 抽象类的测试类 {

	public static void main(String[] args) {
		
		// 实例化对象
		// 抽象类 t = new 抽象类(); // 编译失败，抽象类不能被实例化，只能被继承，由子类实例化
		
		// 父类的引用指向子类
		抽象类 t = new 抽象类的子类(); // 由子类实例化
		t.play();
		t.play2();
		
	}
}
