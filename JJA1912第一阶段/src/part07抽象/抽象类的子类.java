package part07抽象;

/**
 *  一个类如果继承抽象类，那么该类必须实现父类抽象类中所有的抽象方法。
 * @author Administrator
 *
 */
public class 抽象类的子类  extends 抽象类{

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("子类重写父类中的抽象方法");
	}

	@Override
	public void play2() {
		// TODO Auto-generated method stub
		System.out.println("子类重写父类中的抽象方法");
	}

	
}
