package part07抽象.练习;

public abstract class 形状 {

	private int x; // x轴
	private int y; // Y轴
	private String color;// 图案颜色
	
	public 形状(int x, int y, String color) {
		super();
		this.x = x;
		this.y = y;
		this.color = color;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Override
	public String toString() {
		return "形状 [x=" + x + ", y=" + y + ", color=" + color + "]";
	}
	
	public abstract void draw(); // 抽象方法
}
