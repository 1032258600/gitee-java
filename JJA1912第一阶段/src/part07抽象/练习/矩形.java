package part07抽象.练习;

public class 矩形 extends 形状{
	
	private int width; // 宽
	private int height; // 高

	public 矩形(int x, int y, String color,int width,int height) {
		super(x, y, color);
		this.height = height;
		this.width = width;
	}

	@Override
	public void draw() {
	
		System.out.println(super.toString() +  "矩形 宽度：" +  this.width + " 高度： " + this.height );
		
	}

}
