package part07抽象.练习;

public class 线条 extends 形状 {
	
	private int endX; // 终点x轴
	private int endY; // 终点Y轴
	
	public 线条(int x, int y, String color,int endX, int endY) {
		super(x, y, color);
		this.endX = endX;
		this.endY = endY;
		
	}

	@Override
	public void draw() {
		
		System.out.println(super.toString() + this.toString());
	}

	@Override
	public String toString() {
		return "线条 [endX=" + endX + ", endY=" + endY + "]";
	}

	

}
