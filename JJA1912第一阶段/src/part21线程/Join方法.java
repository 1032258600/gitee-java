package part21线程;

public class Join方法 {
/**
 * 13.笔试题: 定义三个线程对象，每隔一定的时间来执行线程对象1（5s），线程对象2（7s），线程对象3（9s）;
 * 
 */
	public static void main(String[] args) {
		// 新建三个线程对象
		MyThread myThread1 = new MyThread(5000);
		MyThread myThread2 = new MyThread(7000);
		MyThread myThread3 = new MyThread(9000);
		
		// 进入就绪状态
		myThread1.start();
		
		try {
			myThread1.join();// 强制执行join方法
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myThread2.start();
		try {
			myThread2.join();// 强制执行join方法
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myThread3.start();
	}
}

class MyThread extends Thread{
	
	private  long time;
	public MyThread(long time) {
		this.time = time;
	}
	@Override
	public void run() {
		
		System.out.println(Thread.currentThread().getName() + "开始执行");
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName() + "结束执行");
	}
}
