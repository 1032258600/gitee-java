package part21线程.线程同步;

/**
 * 使用synchronized关键字实现同步锁
 * @author Administrator
 *
 */
public class 线程同步方法_卖票_Runnable2 {

	public static void main(String[] args) {
		
		// 1 新建线程
		卖票2 m = new  卖票2();
		// 2 就绪状态
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
	}
}

class 卖票2 implements Runnable {

	// 声明票属性
	private int piao = 10; // 10张票
	
	@Override
	public void run() {
		
		// 一个线程最多可以抢到10张
		for (int i = 0; i < 10; i++) {
			// 同步方法_卖票
			syncMethod();
		}
	}
	/**
	 * 同步方法
	 */
	public synchronized void syncMethod() {
		if (piao > 0) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 提示
			System.out.println(Thread.currentThread().getName() + "抢到票号：" + (piao--));
		}
	}
}

