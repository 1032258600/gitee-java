package part21线程.线程同步;

/**
 * 使用synchronized关键字实现同步锁
 * @author Administrator
 *
 */
public class 线程同步块_卖票_Thread {

	public static void main(String[] args) {
		// 创建票对象
		Piao piao = new Piao();
		
		// 1 新建线程
		卖票3 m = new  卖票3(piao); // 通过构造传递票对象，传递同一个票对象实现Thread类中的资源共享
		// 2 就绪状态
		m.start();
		
		卖票3 m2 = new  卖票3(piao);// 通过构造传递票对象，传递同一个票对象实现Thread类中的资源共享
		m2.start();
		
		卖票3 m3 = new  卖票3(piao);// 通过构造传递票对象，传递同一个票对象实现Thread类中的资源共享
		m3.start();
	}
}
/**
 * 票对象
 */
class Piao{
	int count = 10;
}
/**
 * 卖票类实现线程
 * 同步锁，锁票对象
 * @author Administrator
 *
 */
class 卖票3 extends Thread {
	
	// 票对象传递进来
	private Piao piao;
	
	public 卖票3(Piao piao) {
		super();
		this.piao = piao;
	}


	@Override
	public void run() {
		
		// 一个线程最多可以抢到10张
		for (int i = 0; i < 10; i++) {
			// 同步块
			synchronized (piao) { // 锁票对象
				if (piao.count > 0) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// 提示
					System.out.println(Thread.currentThread().getName() + "抢到票号：" + (piao.count--));
				}
			}
		}
	}
}

