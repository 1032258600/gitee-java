package part21线程;

public class 泡茶_多线程_Runnable {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		烧水3 s = new 烧水3();
		// Runnable接口没有提供start方法，必须借助Thread类启动线程
		new Thread(s).start();
		
		洗杯子3 x= new 洗杯子3();
		new Thread(x).start();
		
		long end = System.currentTimeMillis();
		System.out.println(end-start); //
	}
}
// 线程类
class 烧水3 implements Runnable{
	/**
	 * 烧水20秒
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("开始烧水~~");
		try {
			Thread.sleep(20000); // 让当前线程先暂停指定的毫秒数
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
class 洗杯子3 implements Runnable{
	/**
	 * 洗杯子10个，每个2秒
	 */
	@Override
	public void run() {
		System.out.println("开始洗杯子~~");
		for (int i = 1; i <= 10; i++) {
			System.out.println("洗第" + i + "个杯子");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
