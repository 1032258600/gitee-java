package part21线程;

public class 泡茶_多线程_Thread {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		
		烧水2 s = new 烧水2();
		s.start(); // 启动线程,Java虚拟机调用此线程的run方法。 
		洗杯子2 x= new 洗杯子2();
		x.start();
		
		long end = System.currentTimeMillis();
		System.out.println(end-start); //
	}
}
// 线程类
class 烧水2 extends Thread{
	/**
	 * 烧水20秒
	 */
	@Override
	public void run() {
		System.out.println("开始烧水~~");
		try {
			Thread.sleep(20000); // 让当前线程先暂停指定的毫秒数
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
class 洗杯子2 extends Thread{
	/**
	 * 洗杯子10个，每个2秒
	 */
	@Override
	public void run() {
		System.out.println("开始洗杯子~~");
		for (int i = 1; i <= 10; i++) {
			System.out.println("洗第" + i + "个杯子");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
