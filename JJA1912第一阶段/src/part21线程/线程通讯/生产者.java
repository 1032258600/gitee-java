package part21线程.线程通讯;
/**
 * 生产者 线程类模拟多个生产者
 * @author Administrator
 *
 */
public class 生产者 extends Thread{

	// 声明框
	private 框 kuang;
	
	public  生产者(框 kuang) {
		this.kuang = kuang;
	}
	
	@Override
	public void run() {
		
		while (true) {
			// 最多一次生产6个馒头
			for (int i = 1; i <= 6; i++) {
				// 创建一个馒头对象
				馒头 mt = new 馒头(i);
				System.out.println(Thread.currentThread().getName() + "生产馒头:" + mt);
				// 生产者专门生产馒头，丢到框中
				kuang.push(mt);

				// 生产一个馒头睡眠2秒
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}
	}
}
