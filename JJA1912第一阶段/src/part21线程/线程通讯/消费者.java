package part21线程.线程通讯;
/**
 * 消费者 线程类模拟多个消费者
 * @author Administrator
 *
 */
public class 消费者 extends Thread{

	// 声明框
	private 框 kuang;
	
	public  消费者(框 kuang) {
		this.kuang = kuang;
	}
	
	@Override
	public void run() {
		
		while (true) {
			// 最多一次取6个馒头
			for (int i = 1; i <= 6; i++) {

				// 消费者负责取馒头
				馒头 mt = kuang.pop();
				System.out.println(Thread.currentThread().getName() + "获取的馒头：" + mt);

				// 消费馒头睡眠5秒
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}
	}
}
