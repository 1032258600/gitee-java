package part21线程;

public class 泡茶_单线程 {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		烧水.ss();
		洗杯子.xbz();
		long end = System.currentTimeMillis();
		System.out.println(end-start); //40005毫秒 40多秒
	}
}

class 烧水{
	/**
	 * 烧水要20秒
	 */
	public static void ss() {
		System.out.println("开始烧水~~");
		try {
			Thread.sleep(20000); // 让当前线程先暂停指定的毫秒数
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
class 洗杯子{
	/**
	 * 洗杯子10个，每个2秒
	 */
	public static void xbz() {
		System.out.println("开始洗杯子~~");
		for (int i = 1; i <= 10; i++) {
			System.out.println("洗第" + i + "个杯子");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
