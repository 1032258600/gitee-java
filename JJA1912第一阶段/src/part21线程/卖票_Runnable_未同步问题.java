package part21线程;

import java.util.Vector;

public class 卖票_Runnable_未同步问题 {

	public static void main(String[] args) {
		
		// 1 新建线程
		卖票0 m = new  卖票0();
		// 2 就绪状态
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
		new Thread(m).start(); // 传递同一个m对象，piao资源共享
	}
}

class 卖票0 implements Runnable {
	// 声明票属性
	private int piao = 10; // 10张票
	@Override
	public void run() {
		
		// 一个线程最多可以抢到10张
		for (int i = 0; i < 10; i++) {
			if (piao > 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// 提示
				System.out.println(Thread.currentThread().getName() + "抢到票号：" + (piao--));
			}
		}
	}
	
}

