package part21线程;

/**
 * 使用synchronized关键字实现同步锁
 * @author Administrator
 *
 */
public class 线程同步块_卖票_Runnable {

	public static void main(String[] args) {
		
		// 1 新建线程
		卖票 m = new  卖票();
		
		// 2 就绪状态
		new Thread(m, "1号窗口").start(); // 传递同一个m对象，piao资源共享
		new Thread(m, "2号窗口").start(); // 传递同一个m对象，piao资源共享
		new Thread(m, "3号窗口").start(); // 传递同一个m对象，piao资源共享
	}
}
class 卖票 implements Runnable {
	// 声明票属性
	private int piao = 10; // 10张票
	@Override
	public void run() {
		
		// 一个线程最多可以抢到10张
		for (int i = 0; i < 10; i++) {
			// 同步块
			synchronized (this) {
				if (piao > 0) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// 提示
					System.out.println(Thread.currentThread().getName() + "抢到票号：" + (piao--));
				}
			}
		}
	}
}

