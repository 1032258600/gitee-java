package part21线程;

public class 主线程 {

	public static void main(String[] args) {
	
		// 输出主线程的属性信息
		Thread currentThread = Thread.currentThread();
		// 设置线程别名
		currentThread.setName("主线程");
		System.out.println(currentThread);// Thread[线程别名,优先级,线程名]
		System.out.println(currentThread.getName());
		System.out.println(currentThread.getPriority());
		
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			
			try {
				// sleep()使当前正在执行的线程以指定的毫秒数暂停, 自动唤醒
				Thread.sleep(2000); // 参数是毫秒 2000相当于2秒
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
