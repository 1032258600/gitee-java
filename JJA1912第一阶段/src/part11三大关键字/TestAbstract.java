package part11三大关键字;

/**
 * 抽象关键字abstract：
 * 只能修饰类和方法,
 * 不能修饰构造方法
 * @author Administrator
 *
 */
public abstract class TestAbstract {
	
	public TestAbstract() {
		
	}
	// 抽象方法
	public void name() {
		
	}
	// 抽象方法
	public abstract void name2();

}
class A extends TestAbstract{

	@Override
	public void name2() {
		// TODO Auto-generated method stub
		
	}
}