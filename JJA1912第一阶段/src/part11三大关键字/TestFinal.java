package part11三大关键字;

public final class TestFinal { // ④final修饰类：表示不能被继承，没有子类。
	
	// ①final修饰变量--》常量
	final int P = 1;
	
	// ②final修饰方法：表示不能被重写，但是可以被继承，子类可以调用。
	public final void t1() {
		System.out.println("父类中final修饰方法t1");
	}
   
	// ③final修饰参数：表示只可以调用，不可以改变参数的值
	public void t2(final int num) {
		// num = 10; 编译错误，final修饰参数不能被改变
		System.out.println(num);
	}
	
	// ④final不能修饰构造方法
	/*public final TestFinal() {
		
	}*/
	
	public static void main(String[] args) {
		TestFinal testFinal = new TestFinal();
		testFinal.t2(500);
	}	
}
/*class A3  extends TestFinal{
   
	@Override
	public void t2(int num) {
		// TODO Auto-generated method stub
		super.t2(num);
	}
	public static void main(String[] args) {
		A3 a3 = new A3();
		a3.t1(); // 不能重写父类中的final修饰的t1方法,但是可以调用
	}
}*/
