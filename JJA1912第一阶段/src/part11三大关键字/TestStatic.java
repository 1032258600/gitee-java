package part11三大关键字;

import com.sun.accessibility.internal.resources.accessibility;

public  class TestStatic {

	// ①static修饰变量：表示静态变量，在内存只有一份拷贝。不依赖类的实例，被类的所有实例所共享。
	static int num; // 静态变量
	static final int num2 = 100;// 静态常量
	int a; // 普通变量  依赖类的实例
	
	// ②static修饰方法：表示静态方法
	public static void staticMethod() {
		System.out.println("静态方法");
		// 静态方法中是否可以调用静态变量或者静态方法? 可以
		System.out.println("静态方法调用静态变量： " + num);
		
		// 静态方法中是否可以调用普通变量或者普通方法? 不可以		
//		System.out.println("静态方法调用普通变量： " + a); 编译出错
//		method(); 编译出错
	}
	// 普通方法
	public void method() {
		System.out.println("普通方法");
		System.out.println("普通方法调用普通变量： " + a);
		// 普通方法中是否可以调用静态变量或者静态方法? 可以
		System.out.println("普通方法调用静态变量： " + num);
		staticMethod();
	}
	public static void s1() {
		System.out.println("静态方法s1");
	}
	// ③静态块：在程序运行过程中，最早的执行,只能调用静态方法，不能调用普通方法。
	static {
		num = 500; // 对静态变量赋值
		s1();
	}	

	public static void main(String[] args) {
		TestStatic t1 = new TestStatic();
		t1.a = 150;
		TestStatic t2 = new TestStatic();
		t2.a = 250;
		System.out.println(t1.a);// 150
		System.out.println(t2.a);// 250
		t2.a = 550;
		System.out.println(t1.a);// 150
		System.out.println(t2.a);// 550
		
		System.out.println("--静态变量：不依赖类的实例--");
		t1.num = 500;
		t2.num = 1000;
		System.out.println(t1.num);// 1000
		System.out.println(t2.num);// 1000
		t1.num = 2000;
		System.out.println(t1.num);// 2000
		System.out.println(t2.num);// 2000
		
		// 静态变量调用： 类名.静态变量名
		TestStatic.num = 10000;
		// 静态方法调用： 类名.静态方法名()
		TestStatic.staticMethod();
		
		t1.method(); // 普通方法调用： 对象.普通方法名()
		
	}

}
