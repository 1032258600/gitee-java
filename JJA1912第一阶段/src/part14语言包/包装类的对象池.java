package part14语言包;

public class 包装类的对象池 {

	public static void main(String[] args) {
		// 创建Integer 
		Integer int1 = new Integer(100);
		Integer int2 = new Integer(100); // 通过new关键词创建的对象一定会在内存中重新开辟空间
		// int1 和 int2 在内存中是否是同一个对象
		// 判断两个对象是否相等。==判断的是对象的地址
		System.out.println(int1==int2);// false
		
		// 包装类的对象(常量)池
		// Byte、Short、Integer、Long、Character这5中包装类的有对象池，对象池的范围是-128到127.
		// 也就是说超出这个对象池范围，就都会另外再开辟内存空间
		Integer int3 = 100;
		Integer int4 = 100;
		System.out.println(int3 == int4);// true
		
		Integer int5 = 129; 
		Integer int6 = 129;
		System.out.println(int5 == int6);// false 超出池范围
		
		Integer int7 = -128;
		Integer int8 = -128;
		System.out.println(int7 == int8);// true
		
		// 如果创建一个包装类的值 在 -128到127的范围内，在池中就会有这个值
		// 如果再创建一个值和池中的值一致，这个时候会直接指向池中的值。不会再另外创建一个。所以地址就会一样。
		Integer int9 = 100;
		Integer int10 = new Integer(100);
		System.out.println(int9 == int10);// false
		
	}
}
