package part14语言包;

public class Character类常用方法 {
	public static void main(String[] args) {
		// 创建Character类
		Character character = new Character('A');
		// 判断字母 数字 大写 小写
		System.out.println(character.isLetter('A')); // 判断字母true
		System.out.println(character.isDigit('A')); // 判断数字false
		System.out.println(character.isUpperCase('A')); //判断大写true
		System.out.println(character.isLowerCase('A')); //判断小写false
	}

}
