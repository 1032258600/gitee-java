package part14语言包.class类;

import java.util.Scanner;

public class Class类 {
	
	public static void main(String[] args) throws Exception {
		// Class类  类对象 Class<T> T泛型 后续在集合中广泛应用
		
		// Class类构造方法被私有化，就不能使用new关键字创建Class类的类对象，由jvm构建。
		System.out.println("---得到Class类对象的方式1：-----");
		// 方式1：使用Class类中的静态方法forname(String classname) 获取类的类对象
		// 参数className - 所需类的完全限定名称。 
		Class strClass = Class.forName("java.lang.String");// 获取String这个类的类对象
		Class personClass = Class.forName("part14语言包.class类.Person");// 获取Person这个类的类对象
		 
		// 通过类对象创建Person类的对象
		Person p1 = (Person) personClass.newInstance();
		p1.setPname("小白");
		p1.setAge(18);
		
		System.out.println("---得到Class类对象的方式2：-----");
		//方式2：直接使用某个类的.class属性 就可以返回 该类的类对象
		Class personClass2 =Person.class;
		Person p2 = (Person) personClass2.newInstance();
		p2.setAge(18);
		
		System.out.println("---得到Class类对象的方式3：-----");
		//方式3： 通过类的对象.getClass()方法 返回类的类对象
		// 创建Person类的对象
		Person person = new Person();
		Class personClass3 = person.getClass();
		Person p3 = (Person) personClass3.newInstance();
		p3.setAge(18);
		
		// 疑问： 什么时候会使用以上这些方式来创建一个类的对象?
		// 有些时候，我们的代码在编译之前，并不确定到底是要创建哪一个类的对象?
	    // 只能在程序运行之后才能确定到底是创建哪一个类的对象?
		//例如：在控制台输入一个classname字符串(所需类的完全限定名称).根据这个类的完全限定名称，帮我们创建出对应的类的对象。可能String，Person,Integer...
		System.out.println("请输入类的完全限定名称：");
		Scanner scanner = new Scanner(System.in);
		String classname = scanner.next();
		// 构建类的类对象
		Class<?> class2 = Class.forName(classname);
		// 通过类的类对象创建类的对象
		System.out.println("匿名对象：" + class2.newInstance() );
	}
	
}

class Person {
	private String pname;
	private int age;
	public Person() {
		
	}
	public Person(String pname) {
		this.pname = pname;
	}
	private Person(String pname,int num) {
		this.pname = pname;
	}
	
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void t() {
		System.out.println("t~~~");
	}
	private void t2() {
		System.out.println("t2~~~");
	}
	public void t3(int num,double d) {
		System.out.println("num: " + num + ",d： " + d);
	}
	public void t4(Integer num,Double d) {
		System.out.println("num: " + num + ",d： " + d);
	}
	
}