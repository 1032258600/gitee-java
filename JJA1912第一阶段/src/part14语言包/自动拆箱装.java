package part14语言包;

public class 自动拆箱装 {

	public static void main(String[] args) {
		
		// 自动装箱：自动将 基本数据类型  变成 包装类
		Integer integer = 3; // 等价于 Integer integer2 = Integer.valueOf(3);
		
		// 自动拆箱：自动将 包装类 变成 基本数据类型
		int i = integer; // 等价于Integer.parseInt(3);
	}
}
