package part14语言包;

public class 包装类常用方法 {
	
	public static void main(String[] args) {
		// 创建Integer类
		Integer integer = new Integer(1);
		Integer integer2 = new Integer("123");
		System.out.println(integer +"," + integer2);
		
		// 常用方法 valueOf() 将原始数据类型或字符串转换成包装类对象
		Integer integer3 =Integer.valueOf(1);
		Integer integer4 =Integer.valueOf("1");

		// 常用方法 parseInt() 将字符串转换成基本数据类型
		int i = Integer.parseInt("1");
		
		// 未来数据都是从前端界面上接收过来的，都是字符串类型，后台需要进行类型转换，比如年龄 String-->int	
		
	}

}
