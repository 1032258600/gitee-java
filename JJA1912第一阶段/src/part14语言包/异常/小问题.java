package part14语言包.异常;

public class 小问题 {

	public static void main(String[] args) {
		
		System.out.println(小问题.name());
		System.out.println(小问题.method02());
	}
	
	/**
	 * 考点：  该方法最终返回值0 ，因为先执行try在执行finally中的代码
	 * @return
	 */
	public static int name() { // 该方法的返回值是 0 
		
		int i = 1;
		
		try {
			i++;
			return i; // 2
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
		   return 0; // 0 
		}
		
	}
	
	 public static int method02() {// 该方法的返回值是 2
		int i = 1;
		
		try {
			i++;
			return i; // 2
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			i++;
		}
		return 0;
	 }
}
