package part14语言包.异常;

public class TryFinally {

	public static void main(String[] args) {
	
		try {
			System.out.println(1/0);
		} finally {
			// 不管有没有异常，都会执行finally里面的代码
			System.out.println("关闭资源");
		}
		
		System.out.println("结束");
		
	}
}
