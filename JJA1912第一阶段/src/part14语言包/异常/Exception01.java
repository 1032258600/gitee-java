package part14语言包.异常;

public class Exception01 {

	public static void main(String[] args) throws ClassNotFoundException {
		
		// 运行时异常：
//		System.out.println(1/0); // java.lang.ArithmeticException: / by zero 算术异常
		String str = null;
		System.out.println(str.length()); //  java.lang.NullPointerException 空指针异常
		System.out.println("测试");
		
		// 编译时异常
		Class.forName("");
	}
}
