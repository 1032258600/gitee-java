package part14语言包.异常;

public class TryCatchFinally {

	public static void main(String[] args) {
	
		try {
			// 监视可能出现异常的代码块
			System.out.println(1/0);
			
		} catch (Exception e){
			// 捕获异常,进行异常处理
			System.out.println("出现异常");
			e.printStackTrace();
			
		} finally {
			// 不管有没有异常，都会执行finally里面的代码
			System.out.println("关闭资源");
		}
		
		System.out.println("结束");
		
	}
}
