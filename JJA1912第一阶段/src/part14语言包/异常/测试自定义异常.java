package part14语言包.异常;

public class 测试自定义异常 {

	public static void main(String[] args) throws AgeException {
		
		int age = -100;
		if (age<=0 || age>=150) {
			// 无效年龄
			// 手工抛出异常
			throw(new AgeException());
		}
		System.out.println(age);
	}
}
