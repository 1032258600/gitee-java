package part14语言包.异常;
/**
 * 多重catch：层次越高的异常类越往下写。
 * @author Administrator
 *
 */
public class Try多重Catch {

	public static void main(String[] args) {
		System.out.println("异常处理方式1");
		
		
		try {
			// 监视作用
			System.out.println(1/1);
			
			String string = null;
			System.out.println(string.length());
			
			int[] array = new int[2];
			System.out.println(array[2]);
			
		} catch (ArithmeticException e) { // 算术异常
			// 开发环境：程序开发期间记得打印轨迹
			e.printStackTrace();
			System.out.println("算术异常");
		} catch (NullPointerException e) { // 空指针异常
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("空指针异常");
		} catch (ArrayIndexOutOfBoundsException e) { // 数组越界异常
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("数组越界异常");
		} catch (Exception e) {  // 多重catch中，层次越高的异常类越往下写。
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("其他异常");
		}
		
		System.out.println("结束");
	}
}
