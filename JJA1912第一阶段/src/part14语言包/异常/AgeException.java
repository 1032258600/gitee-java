package part14语言包.异常;
/**
 * 自定义异常类，该类必须继承Exception类。
 * @author Administrator
 *
 */
public class AgeException extends Exception{

	public AgeException(){
		super("年龄无效"); // 打印的内容
	}
}
