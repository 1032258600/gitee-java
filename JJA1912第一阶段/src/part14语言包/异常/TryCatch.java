package part14语言包.异常;
/**
 * try..catch
 * try: 监视可能出现异常的代码块
 * catch: 捕获异常
 * @author Administrator
 *
 */
public class TryCatch {

	public static void main(String[] args) {
		System.out.println("异常处理方式1");
		
		try {
			// 监视作用
			System.out.println(1/0);
			
		} catch (Exception e) { // catch块 捕获异常
			
			// 开发环境：程序开发期间记得打印轨迹
			e.printStackTrace();
			
			// 生产环境：用户提示信息
			System.out.println("你的操作有误，请联系管理员");
		}
		
		System.out.println("结束");
	}
}
