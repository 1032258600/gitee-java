package part14语言包.字符串类;

public class String类常用方法 {
	
	public static void main(String[] args) {
		
	    // 构造方法 创建一个String类的对象
		String str1 = new String("abc");
		String str2 = "ABC";
		// 字符数组转换成字符串
		char[] cs = {'A','B','C'}; 
		String str3 = new String(cs);
		// 字符串变成字符数组
		char[] charArray = str3.toCharArray();
		
		// 常用方法  引用.方法名()
		String str4 = " ABCJ FJKLDJG ";
		
		// 求字符串长度length()
		System.out.println("长度：" + str4.length()); // 14 
		
		// indexOf() 返回指定子字符串第一次出现的字符串内的索引。 
		System.out.println("indexOf():" + str4.indexOf("F")); // 索引值6
		System.out.println("indexOf():" + str4.indexOf("AB")); // 索引值1
		System.out.println("indexOf():" + str4.indexOf("AC")); // 索引值-1 表示找不到
		
		// charAt(int index) 返回 char指定索引处的值。 
		System.out.println("charAt:" +  str4.charAt(1));
		System.out.println("charAt:" +  str4.charAt(6));
//		System.out.println("charAt:" +  str4.charAt(14));// 运行错误，下标越界
		
		// 字符串比较 compareTo(String anotherString) 按字典顺序比较两个字符串
		// 字符串比较 compareToIgnoreCase(String anotherString) 按字典顺序比较两个字符串，不区分大小写
		String s1 = new String("abc");
		String s2 = new String("aBc");
		System.out.println(s1==s2); // 不等
		System.out.println(s1.compareToIgnoreCase(s2)); // 0 相等
		
		// 字符串比较  equals(Object anObject)将此字符串与指定对象进行比较
		System.out.println(s1.equals(s2));//false
		System.out.println(s1.equalsIgnoreCase(s2));//true
		
		// 字符串切割 split()
		String s3 = "aBcDEFG";
		String[] split = s3.split("");
		// foreach循环
		for (String str : split) {
			System.out.println(str);
		}
		
		// 考试系统 abcd四个选项 多选题abcd ac
		String s4 = "a,b,c,d";
		split = s4.split(",");
		// foreach循环
		for (String str : split) {
			System.out.println(str);
		}
		
		// 截取字符串 substring(int beginIndex)  返回一个字符串，该字符串是此字符串的子字符串。  
		String s5 = "ABCDEGF";
		System.out.println("substring： " + s5.substring(2)); //CDEGF
		// 截取字符串 String substring(int beginIndex, int endIndex) 返回一个字符串，该字符串是此字符串的子字符串。 不包含结束索引  
		System.out.println("substring： " + s5.substring(2,5)); // CDE
		
		// 假设现在有一个文件名 12.345.jpg --->获取文件名格式
		// 首先第一步1
		String s6 = "12.345.jpg";
		int index = s6.lastIndexOf("."); // 最后一次出现的位置
		String subS = s6.substring(index);		
		System.out.println(subS);
		
		// replace(char oldChar, char newChar) 返回从替换所有出现的导致一个字符串 oldChar在此字符串 newChar 。 
		String s7 = "ABCD.AAAAjpg";
		System.out.println("替换：" + s7.replace('A', '*')); //*BCD.****jpg
		System.out.println("替换后：" + s7); //ABCD.AAAAjpg 字符串String不可变
		
		// trim() 去前后空格
		String s8 = " ABC DEF";
		System.out.println("去空格：" + s8.trim());
		
		s8 = "      ";
		if (s8.trim().length() >= 6) {
			System.out.println("长度符合条件");
		}	
		
		String st1 = null; // null
		String st2 = ""; // 空串
		System.out.println(st1.length()); // 运行错误，空指针异常
		System.out.println(st2.length()); // 长度0
	}

}
