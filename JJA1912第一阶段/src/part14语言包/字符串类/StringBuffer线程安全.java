package part14语言包.字符串类;

public class StringBuffer线程安全 {

	public static void main(String[] args) {
		// 创建一个StringBuffer类对象
		StringBuffer stringBuffer = new StringBuffer("abc");
		
		// StringBuffer是线程安全的，源码中各种方法都是加同步锁synchronized
	    // 其他方法的使用和StringBuilder一样。
	}
}
