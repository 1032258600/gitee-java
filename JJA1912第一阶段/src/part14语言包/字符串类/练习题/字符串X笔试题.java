package part14语言包.字符串类.练习题;

import org.junit.jupiter.api.Test;

public class 字符串X笔试题 {
	
//	3.定义一个字符串，判断字符串中包含的数字、纯字母、中文，其他的个数。
	@Test
	public void hw1() {
		String str = "ABC$abc1122344 !@叶鸿";
		int digitCount = 0;
		int letterCount = 0;// 字母
		int other = 0;
		int cnCount = 0; // 中文
		// 将字符串变成字符数组
		char[] charArray = str.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			// 利用Character类
			if (Character.isDigit(charArray[i])) { // 数字
				digitCount++;
			}else if (Character.isLetter(charArray[i])) { // isLetter包含字母和中文
				if (charArray[i]>='a' && charArray[i]<='z' || charArray[i]>='A' && charArray[i]<='Z') {
					letterCount++;
				} else {
					cnCount++;
				}
			}else {
				other++;
			}
		}
		System.out.println("数字：" +digitCount + ",字母：" + letterCount +",中文:"+ cnCount + ",其他字符：" + other  );
	}
	
//	4.定义一个整数字符串，判断是否是“回文数”。  123321 正序和反序是相同。
	@Test
	public void hw2() {
		String str= "123321";
		
		// 构建一个StringBuilder或者StringBuffer对象
		StringBuilder sb = new StringBuilder(str); // 123456
		sb.reverse(); //654321
		
		if (str.equals(sb.toString())) {
			System.out.println(str + "该整数是回文数");
		}else {
			System.out.println(str + "不是回文数");
		}
	}
	
	/**
	 * 考查字符串类的可变性和方法的参数传递
	 * ①String不可变，StringBuilder和StringBuffer是可变的
	 * ②String是值传递，StringBuilder和StringBuffer是引用传递
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = "abc";
		String s2 = "xyz";
		System.out.println(s1 + "---" + s2);//abc --- xyz
		changeStr(s1, s2); 
		System.out.println(s1 + "---" + s2);//abc --- xyz

		StringBuffer sb1 = new StringBuffer("abc");
		StringBuffer sb2 = new StringBuffer("xyz");
		System.out.println(sb1 + "---" + sb2); // abc -- xyz
		changeStr(sb1, sb2);
		System.out.println(sb1 + "---" + sb2); // abc --- xyzxyz
	}
	
	public static void changeStr(StringBuffer sb1, StringBuffer sb2) {
		sb1 = sb2; //sb1引用指向的堆中实例和sb2指向的实例是一致，sb1和sb2都指向了"xyz"实例
		sb2.append(sb1); //sb2=xyz sb1=xyz 经过append方法，sb2的实例xyz上追加了sb1的实例xyz，所以最终变成xyzxyz
		System.out.println(sb1 + "---" + sb2);//xyzxyz --- xyzxyz
	}

	public static void changeStr(String s11, String s22) {
		s11 = s22; // 表示s11引用和s22引用名指向的地址一致，
		s22 = s11 + s22;// 拼接，不可以变。xyzxyz
	}
}
class 测试字符串2{
	/**
	 * 考查字符串类的可变性和方法的参数传递
	 * ①String不可变，StringBuilder和StringBuffer是可变的
	 * ②String是值传递，StringBuilder和StringBuffer是引用传递
	 * @param args
	 */
	public static void main(String[] args) {
		
		StringBuffer sb1 = new StringBuffer("abc");
		StringBuffer sb2 = new StringBuffer("xyz");
		System.out.println(sb1 + "---" + sb2);// abc --- xyz
		changeStr(sb1, sb2);
		System.out.println(sb1 + "---" + sb2);// abcxyz --- xyzabcxyz
	}
	public static void changeStr(StringBuffer sb1, StringBuffer sb2) {
		sb1.append(sb2); //
		sb2.append(sb1); //
		System.out.println(sb1 + "---" + sb2);// abcxyz --- xyzabcxyz
	}

}
