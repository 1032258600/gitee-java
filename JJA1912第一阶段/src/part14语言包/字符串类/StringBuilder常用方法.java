package part14语言包.字符串类;

public class StringBuilder常用方法 {
	
	public static void main(String[] args) {
		// 创建一个StringBuilder类的对象
		StringBuilder sBuilder = new StringBuilder("abc");
		System.out.println(sBuilder.hashCode());
		
		// 1.添加
		/**
		 * append(追加的内容) 将内容追加到字符串最后
		 * insert(位置,插入的内容) 在指定的位置上插入指定的内容
		 */
		sBuilder.append("ABC");
		System.out.println(sBuilder); // abcABC  
		System.out.println(sBuilder.hashCode());
		
		sBuilder.insert(1, 'E'); 
		System.out.println(sBuilder); // aEbcABC  
		
		// 2.修改
		/**
		 * replace(int start, int end, String str) 用指定的String中的字符替换此序列的子字符串中的 String 。 
		 * reverse() 导致该字符序列被序列的相反代替。  
		  void setCharAt(int index, char ch) 指定索引处的字符设置为 ch 。  
		 */
		StringBuilder sBuilder2 = new StringBuilder("abcdefghijk");
		sBuilder2.replace(1, 5, "AAA");
		System.out.println(sBuilder2); // aAAAfghijk 包括开始位置，不包括结束位置
		sBuilder2.reverse();
		System.out.println(sBuilder2); //kjihgfAAAa
		sBuilder2.setCharAt(0, 'H');
		System.out.println(sBuilder2); //HjihgfAAAa
		
		//3 删除
		/**
		 * delete(int start, int end) 删除此序列的子字符串中的字符。  
	       deleteCharAt(int index)  删除 char在这个序列中的指定位置。  
		 */
		StringBuilder sBuilder3 = new StringBuilder("abcd");
		sBuilder3.delete(0, 2);
		System.out.println(sBuilder3); //cd
		sBuilder3.deleteCharAt(0);
		System.out.println(sBuilder3); //d
		
		
		//4 查询方法
		/**
		 * charAt(int index) 返回 char在指定索引在这个序列值。
		 * indexOf(String str) 返回指定子字符串第一次出现的字符串内的索引。 
		 */
		StringBuilder sBuilder4 = new StringBuilder("abcd");
		System.out.println(sBuilder4.charAt(0)); //a
		System.out.println(sBuilder4.indexOf("cd")); //2
		
		// 字符串拼接选择可变字符串
		StringBuilder sBuilder5 = new StringBuilder("A");
		for (int i = 0; i < 9; i++) {
			// 拼接
			sBuilder5.append(i);// 可变
		}
	}

}
