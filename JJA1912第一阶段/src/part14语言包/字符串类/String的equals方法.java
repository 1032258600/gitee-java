package part14语言包.字符串类;

public class String的equals方法 {

	public static void main(String[] args) {
		// 经典题目： == 和 equals
		String s1 = new String("ABC");
		String s2 = new String("ABC");
		
		// == 判断是地址
		System.out.println(s1==s2); // false
		
		// equals判断是内容
		System.out.println(s1.equals(s2)); // true
		
		// 例子：从前端获取一个用户名，我们想判断是否用户名 admin
		
		// String username ="admin";
		 String username = null;
	/*	if (username.equals("admin")) { username可能会是null，就可能会出现空引用
			System.out.println("admin");
		}else {
			System.out.println("用户名不存在");
		}*/
		 
		 // 在代码规范中，非空的对象.equals
		 if ("admin".equals(username)) { 
			System.out.println("admin");
		}else {
			 System.out.println("用户名不存在");
		}
		
	}
}
