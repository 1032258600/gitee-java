package part14语言包.字符串类;

public class 字符串类的equals方法 {

	public static void main(String[] args) {
		
		String s1 = new String("ABC");
		String s2 = new String("ABC");
		System.out.println(s1.equals(s2));// true
		
		StringBuilder sb1 = new StringBuilder("ABC");
		StringBuilder sb2 = new StringBuilder("ABC");
		System.out.println(sb1.equals(sb2));// false
		
		StringBuffer sb11 = new StringBuffer("ABC");
		StringBuffer sb22 = new StringBuffer("ABC");
		System.out.println(sb11.equals(sb22));//false
		
		// String  StringBuffer StringBuilder三个字符类都是继承Object类。
		// Object类中的equals比较是 地址  依然是使用==
		// String因为重写父类的equals()，判断的是内容
		// 而StringBuffer和StringBuilder没有重写父类的equals()，所以依然判断地址。
	}
}
