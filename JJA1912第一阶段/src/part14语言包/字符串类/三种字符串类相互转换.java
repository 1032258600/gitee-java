package part14语言包.字符串类;

public class 三种字符串类相互转换 {
	
	public static void main(String[] args) {
		// 将String-->StringBuffer和StringBuilder
		String str = "ABC";
		StringBuffer stringBuffer =  new StringBuffer(str);
		stringBuffer.append("AB");
		
		StringBuilder  stringBuilder =  new StringBuilder(str);
		stringBuilder.append("ab");
		
		// StringBuffer和StringBuilder-->String
		String str2 = stringBuilder.toString();
		String str3 = stringBuffer.toString();
		
	}

}
