package part14语言包.字符串类;

public class String不可变性 {
	public static void main(String[] args) {
		String s1 = "ABC";
		System.out.println(s1.hashCode()); // 64578
		
		// 重新赋值
		s1 = "ABCDEF";
		System.out.println(s1.hashCode());// 1923910755
		
		s1 = new String("ABCDDDD");
		System.out.println(s1.hashCode());// -488309694
		
		s1 = "ABC";
		System.out.println(s1.hashCode()); // 64578 因为String常量池的概念，池中已经有了“ABC”
		
		s1 = "ABCDEF";
		System.out.println(s1.hashCode()); // 1923910755  因为String常量池的概念,池中已经有了“ABCDEF”
		
		// 开发规范
		String s2 = "A";
		for (int i = 0; i < 9; i++) {
			s2 += i; // 不建议使用，因为每一次拼接，都是创建新的实例，造成内存资源浪费
		}
		System.out.println(s2);
		
	}

}
