package part14语言包.字符串类;

public class String常量池 {
	
	public static void main(String[] args) {
		// 包装类的常量池范围 -128到127
		// 现在来说说 String里面的常量池
		
		// 常量池
		String str1 = "ABC";
		String str2 = "ABC";
		// str1创建了常量对象abc，存放在常量池中
		// str2也创建了常量对象abc，常量池已经有abc，直接将str2的引用指向常量池中的abc
		System.out.println(str1 == str2); // true
		System.out.println(str1.equals(str2)); // true
		
		String str3 = new String("ABC");
		String str4 =  new String("ABC");
		System.out.println(str3 == str4); // false
		System.out.println(str3.equals(str4)); // true
		
	}

}
