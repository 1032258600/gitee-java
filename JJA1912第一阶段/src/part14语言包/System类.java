package part14语言包;

import java.util.Scanner;

public class System类 {

	public static void main(String[] args) {
		long s = System.currentTimeMillis();
		// 标准输入
		Scanner scanner = new Scanner(System.in);
		
		// 标准输出
		System.out.println("输出到控制台");
		
		// 错误输出
		System.err.println("错误输出");
		
		long end = System.currentTimeMillis();
		System.out.println(end-s);
		
		// 退出当前程序 终止jvm运行
		System.exit(0);
		
		while(1==1) {
			System.out.println("11");
		}
	}
}
