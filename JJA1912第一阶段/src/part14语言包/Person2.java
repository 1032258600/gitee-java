package part14语言包;

public class Person2 {

	private GenderEnum gender;

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}
	
	public static void main(String[] args) {
		Person2 person = new Person2();
		person.setGender(GenderEnum.男);
		System.out.println(person.getGender());
		Person2 person2 = new Person2();
		person2.setGender(GenderEnum.未知);
		System.out.println(person2.getGender());

	}
}
