package part14语言包;

import org.junit.jupiter.api.Test;

public class 单元测试 {

	public static void main(String[] args) {
		System.out.println("H");
//		单元测试 t = new 单元测试();
//		t.t1();
	}
	/**
	 * 单元测试：单元测试相当于main方法,一个类中只有一个main方法，但是可以有多个单元测试方法。
	        注意：单元测试的方法不能有参数，不能有返回值，访问修饰符不能是private。
	 */
	@Test
	public void t1() {
		System.out.println("T1");
	}
	
	@Test
	public void t2() {
		System.out.println("T2");	
	}
}
