package part14语言包.object类;

public class 克隆应用 {
	public static void main(String[] args) throws CloneNotSupportedException  {
		
		Apple apple = new Apple();
		apple.setSize("16");
		System.out.println(apple.getSize()); //16
		changeApple(apple); // 引用传递
		System.out.println(apple.getSize()); //47
		
		System.out.println("----利用克隆----");
		Apple apple2 = new Apple();
		apple2.setSize("16");
		changeApple((Apple)apple2.clone()); //克隆
		System.out.println(apple2.getSize()); //16 因为传递给方法的是apple引用的克隆，克隆出一个新的引用
	}
	public static void changeApple(Apple apple) {
		apple.setSize("47");
	}
}

class Apple implements Cloneable{
	private String size;

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
