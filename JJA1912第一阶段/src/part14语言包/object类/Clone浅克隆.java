package part14语言包.object类;

public class Clone浅克隆 {
	public static void main(String[] args) throws CloneNotSupportedException {
		// 创建一个Bod类的对象
		Boy boy = new Boy();
		boy.setName("晨宇");
	
		// 创建一个Girl类的对象
		Girl girl = new Girl();
		girl.setName("baby");
		// 设置女朋友属性
		boy.setGirl(girl);
		
		// 输出boy的信息
		System.out.println(boy.toString());
		System.out.println("原生：" +  boy.hashCode() + ",gril对象：" +  boy.getGirl().hashCode());
		
		// 克隆(复制)
		Boy boy2 = (Boy) boy.clone();
		boy2.setName("YY");
		boy2.getGirl().setName("冰冰");
		// 输出克隆boy2的信息
		System.out.println(boy2.toString());
		System.out.println("克隆：" +  boy2.hashCode() + ",gril对象：" +  boy2.getGirl().hashCode());
		
		System.out.println("原生：" +  boy.toString());
		System.out.println("原生：" + boy.hashCode() + ",gril对象：" +  boy.getGirl().hashCode());
		
	}

}

class Boy implements Cloneable{ // 支持克隆的类必须实现Cloneable接口，重写clone()方法
	private String name;
	// 女朋友
	private Girl girl; // 对象属性

	public Girl getGirl() {
		return girl;
	}

	public void setGirl(Girl girl) {
		this.girl = girl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Boy [name=" + name + ", girl=" + girl + "]";
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
	
}
class Girl {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Girl [name=" + name + "]";
	}
	
}