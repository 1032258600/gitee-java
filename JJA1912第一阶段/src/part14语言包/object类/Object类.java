package part14语言包.object类;

public class Object类 {

	public static void main(String[] args) {
		
		// 1 Object中的equals()判断两个对象是否相等 比较是 地址
		String str1 = new String("A");
		String str2 = new String("A");
		// String重写了Object类中的equals，变成判断字符串的内容
		// 此时的equals调用的子类String重写后的equals的方法
		System.out.println(str1.equals(str2)); // true
		
		StringBuffer sb1 = new StringBuffer("A");
		StringBuffer sb2= new StringBuffer("A");
		// 此時的equals()调用时父类Object中方法
		System.out.println(sb1.equals(sb2)); // false 
		
		Dog dog= new Dog("哈士奇");
		// 2.toString()方法
		// 如果没有重写toString方法 输出的是对象的信息： 包名.类名@哈希码  part14语言包.object类.Dog@7852e922
		System.out.println(dog); 
		
		Dog dog2= new Dog("哈士奇");
		
		// 3.hashCode() 相当于对象身份证标识符
		System.out.println(dog.hashCode());// 2018699554
		System.out.println(dog2.hashCode());// 1311053135
		
		
		for (int i = 0; i < 10; i++) {
			Dog dog3 = new Dog("大白");
			System.out.println(dog3.hashCode());
		}
		
		// 显式启动垃圾回收器
		System.gc();
		
	}
}

class Dog extends Object{
	
	private String name;
	public Dog(String name){
		this.name = name;
	}
	@Override
	public String toString() {
		return "Dog [name=" + name + "]";
	}
	@Override
	public boolean equals(Object obj) {
		// 以后我们可以重写equals改变判断规则
		return super.equals(obj);
	}
	@Override
	public int hashCode() {
		// 以后我们可以重写hashCode改变判断规则
		return super.hashCode();
	}
	
	@Override
	protected void finalize() throws Throwable { // 该方法是被java的垃圾回收器调用
		// TODO Auto-generated method stub
		System.out.println("Dog被垃圾回收器回收");
		super.finalize();
	}
}
