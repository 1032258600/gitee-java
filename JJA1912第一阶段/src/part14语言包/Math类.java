package part14语言包;

public class Math类 {

	public static void main(String[] args) {
		// 使用Math类中的属性和方法
		System.out.println(Math.PI);
		// 计算圆面积
		int r = 5;
		System.out.println(Math.PI * r * r);
		System.out.println(Math.PI * Math.pow(r, 2)); // 计算pow(r, 2)平方
		
		// 1 获取随机数  random() 大于等于0.0 ，小于1.0 
		double i = Math.random();
		System.out.println(i);
		
		int i2 = (int) (Math.random() * 3); // 【0,3)
		System.out.println(i2);
		
		i2 = (int) (Math.random() * 3 + 1); // 【0,3】
		System.out.println(i2);
		 
		// 2 Math.floor() 返回小于或等于参数的最大（最接近正无穷大） double值，等于数学整数。
		System.out.println("----Math.floor() 向下取整----");
		System.out.println(Math.floor(11.5));//11.0
		System.out.println(Math.floor(11.8));//11.0
		System.out.println(Math.floor(12.9));//12
		System.out.println(Math.floor(12.1));//12
		System.out.println(Math.floor(-12.1));//-13
		System.out.println(Math.floor(-12.9));//-13
		
		System.out.println("----Math.ceil() 向上取整----");
		System.out.println(Math.ceil(11.5));//12
		System.out.println(Math.ceil(11.8));//12
		System.out.println(Math.ceil(12.9));//13
		System.out.println(Math.ceil(12.1));//13
		System.out.println(Math.ceil(-12.1));//-12
		System.out.println(Math.ceil(-12.9));//-12
		
		System.out.println("----Math.round() 四舍五入----");
		System.out.println(Math.round(11.5));//12
		System.out.println(Math.round(11.8));//12
		System.out.println(Math.round(12.1));//12
		System.out.println(Math.round(-12.1));//-12
		System.out.println(Math.round(-12.9));//-13
	}
}
