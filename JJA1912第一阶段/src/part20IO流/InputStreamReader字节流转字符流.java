package part20IO流;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputStreamReader字节流转字符流 {

	public static void main(String[] args) throws IOException {
		// 接受控制台的输入
		// Scanner类的构造方法内部将字节流传递给了InputStreamReader类的
//		Scanner  sc = new Scanner(System.in);
//		System.out.println(sc.next());
		
		System.out.println("请输入：");
		// 字节流转字符流类
		InputStreamReader isr = new InputStreamReader(System.in);
		// 缓存读取流
		BufferedReader bReader = new BufferedReader(isr);
		
		String content = bReader.readLine();
		System.out.println(content);
	}
}
