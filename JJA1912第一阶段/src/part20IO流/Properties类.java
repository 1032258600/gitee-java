package part20IO流;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class Properties类 {
	public static void main(String[] args) throws Exception {
		Properties类 p = new Properties类();
//		p.写();
		p.读();
	}

	// 一般用作 配置文件使用
	// 文件类型 XXXX.properties
	public void 写() throws Exception {
		// 配合字节输出流
		FileOutputStream out = new FileOutputStream("jdbc.properties");
		// 构建Properties类的空属性的引用对象
		Properties properties = new Properties();
		// 键值对的形式进行存储
		properties.setProperty("url", "127.0.0.1");
		properties.setProperty("username", "root");
		properties.setProperty("password", "root");
		// 将properties对象写入流中
		properties.store(out, null);
		System.out.println("--配置信息写入完毕---");
	}

	public void 读() throws Exception {

		// 配合字节输入流
		FileInputStream in = new FileInputStream("jdbc.properties");
		// 构建Properties类的空属性的引用对象
		Properties properties = new Properties();
		// 读取
		properties.load(in); // 将流中读取信息到properties对象中
		// get
		System.out.println("连接地址：" + properties.getProperty("url"));
		System.out.println("用户名：" + properties.getProperty("username"));
		System.out.println("密码：" + properties.getProperty("password"));
		System.out.println("--配置信息读取完毕---");
	}
}
