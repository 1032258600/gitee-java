package part20IO流.hw;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class 字符流练习 {

	// 从控制台输入字符串，每次输入的字符串加上当前系统时间追加到日志文件中”log.txt”。每次输入后换行。直到用户输入exit才退出。
	// 要求：不能使用Scanner类，使用InputStreamReader类。

	public static void main(String[] args) {

		// 从控制台接收字符串
		BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
		FileWriter fWriter = null;
		BufferedWriter bfWriter = null;
	
		try {
			// 关联文件对象
			File file = new File("log.txt");
			if (!file.exists()) {
				// 创建
				file.createNewFile();
				System.out.println("创建文件成功");
			}
			
			// 写文件 
			fWriter = new FileWriter(file,true);
			bfWriter = new BufferedWriter(fWriter);
			
			String content = null;
			while (true) {
				
				System.out.println("请输入字符串");
				
				if ( (content = bReader.readLine()) != null) {
					
					if ("exit".equals(content)) {
						// 结束循环
						break;
					}
					
					bfWriter.write(content + "[" + new Date().toLocaleString() + "]");
					bfWriter.newLine();
					// 刷新流
					bfWriter.flush();
				}
			}
			
			System.out.println("----写入完毕---");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			try {
				if (bfWriter != null) {
					bfWriter.close();
				}
				if (fWriter != null) {
					fWriter.close();
				}
				if (bReader != null) {
					bReader.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	
	}
}
