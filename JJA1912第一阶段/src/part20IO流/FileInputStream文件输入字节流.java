package part20IO流;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.Test;

/**
 * FileInputStream文件输入字节流 读操作 read()读取方法
 * 
 * @author Administrator
 *
 */
public class FileInputStream文件输入字节流 {

	/**
	 * 文件读操作 read() 一个字节一个字节的读，但是会出现中文乱码。
	 */
	@Test
	public void 读操作() {
		// FileInputStream应用：读取文件内容

		File file = new File("test.txt");
		FileInputStream fis = null;
		
		if (file.exists()) {
			try {
				// 创建一个文件输入字节流
				fis = new FileInputStream(file);

				// 读文件
				for (int i = 0; i < file.length(); i++) {
					// read() 从该输入流中读取一个字节的数据
					int read = fis.read();
					// 将整型转成字符型
					char c = (char) read;
					System.out.print(c);
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					
					if (fis!=null) {
						fis.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}else {
			System.out.println("源文件不存在，无法读取");
		}
	}

	/**
	 * read(byte[] b) 从该输入流读取最多 b.length个字节的数据为字节数组。
	 * 一次性读取多个字节，但是也不是推荐做法。
	 * 虽然可以暂时解决中文乱码的问题，但是读取有时候会出问题
	 */
	@Test
	public void 读操作2() {
		// FileInputStream应用：读取文件内容

		File file = new File("test.txt");
		FileInputStream fis = null;
		if (file.exists()) {
			try {
				// 创建一个文件输入字节流
				fis = new FileInputStream(file);
				// 将读取的内容存放到字节数组中
				byte[] b = new byte[2048];
				fis.read(b);
				// 将字节数组 转 字符串
				String content = new String(b);
				System.out.println(content);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {

					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
		}else {
			System.out.println("源文件不存在，无法读取");
		}
	}
}
