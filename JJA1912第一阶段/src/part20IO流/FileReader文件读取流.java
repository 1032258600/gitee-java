package part20IO流;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.jupiter.api.Test;
/**
 * FileReader文件读取流
 * 字符流
 * @author Administrator
 *
 */
public class FileReader文件读取流 {

	@Test
	public void 读操作() {
		
		// 构建文件关联对象
		File file = new File("test.txt");
		
		// 使用文件读取流，读文件
		FileReader fReader = null;
		// 缓冲读取流
		BufferedReader bReader = null;
		try {
			
			fReader = new FileReader(file);
			
			// 字符读取流本身是一个一个字符的读，效率太低，一般配合缓冲流使用
			// 将 文件读取流对象包装到缓冲读取流中
			bReader = new BufferedReader(fReader);
			
			// 从缓冲流中读取内容   readLine()每次读取一行， 读到流的末尾,数据则为null
			String content = null;
			while (   (content = bReader.readLine()) != null  ) { //  每次读取一行数据，都要判断是否为null
				System.out.println(content);
			}
				
			System.out.println("读取完毕");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (bReader!=null) {
					bReader.close();
				}
				if (fReader!=null) {
					fReader.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
