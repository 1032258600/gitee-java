package part20IO流;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class 文件复制2 {

	public static void main(String[] args) {
		文件复制2.copy("a.txt", "b.txt");
	}
	public static void copy(String source, String dest) {
		
		// 构建文件关联对象
		File sourceFile = new File(source);
		File destFile = new File(dest);
		
		if (!sourceFile.exists()) {
			System.out.println("源文件不存在");
			return;
		}
		if (!destFile.exists()) {
			System.out.println("目标文件不存在");
			try {
				destFile.createNewFile();
				System.out.println("创建目标文件");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			return;
		}
		// 复制
		// 文件输入和输出流 字节流
		FileInputStream fis = null;
		FileOutputStream fos = null;
		
		try {
			// 输入流 读取源文件的内容
			fis = new FileInputStream(sourceFile);
			// 输出流 写内容到指定文件中
			fos = new FileOutputStream(destFile,true);
			
			// 一次性读取
			byte[] b = new byte[(int) sourceFile.length()];
			fis.read(b);
			// 一次性写入
			fos.write(b);
			
			System.out.println("一次性读取写入，复制完成");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if (fos!=null) {
					fos.close();
				}
				if (fis!=null) {
					fis.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
