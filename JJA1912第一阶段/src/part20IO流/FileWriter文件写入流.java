package part20IO流;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.jupiter.api.Test;

/**
 * FileWriter文件写入流 字符流
 * 
 * @author Administrator
 *
 */
public class FileWriter文件写入流 {

	@Test
	public void 写操作() {

		// 构建文件关联对象
		File file = new File("test.txt");

		// 使用文件写入流，写文件
		FileWriter fWriter = null;
		// 缓冲写入流
		BufferedWriter bWriter = null;
		
		try {
			fWriter = new FileWriter(file,true); // 追加
			bWriter = new BufferedWriter(fWriter);
			
			// 写入
			bWriter.newLine(); // 换行
			bWriter.write("A");
			bWriter.write("BCD");
			bWriter.newLine();
			bWriter.write(97);
			// 刷新流
			bWriter.flush();
			System.out.println("----写入完毕---");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (bWriter!=null) {
					bWriter.close(); // 关闭流，先刷新
				}
				if (fWriter!=null) {
					fWriter.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
