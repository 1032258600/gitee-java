package part20IO流;

import java.io.Serializable;

public class Person implements Serializable{ // 对象必须实现Serializable接口,才可以将对象写入流中

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String pname;
	private String age;
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Person(String pname, String age) {
		super();
		this.pname = pname;
		this.age = age;
	}
	public Person() {
		super();
		
	}
	@Override
	public String toString() {
		return "Person [pname=" + pname + ", age=" + age + "]";
	}
	
}
