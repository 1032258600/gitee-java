package part20IO流;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.junit.jupiter.api.Test;

/**
 * FileOutputStream文件输出字节流 写操作
 * write()
 * @author Administrator
 *
 */
public class FileOutputStream文件输出字节流 {

	@Test
	public void 写操作() {
		// FileOutputStream应用：写内容到文件中
		
		// 构建一个文件关联对象
		File file = new File("test.txt");
		// 构建文件输出字节流
		FileOutputStream fos = null;
		if (file.exists()) {
			
			try {
				// 输出流
//				fos = new FileOutputStream(file); // 写入操作是覆盖源文件的内容
				fos = new FileOutputStream(file, true);// 第二个参数设置true 表示 写入的内容是追加在文件的末尾
				// 写入
				String content = "今天是9月16日";
				// write(byte[] b) 
				fos.write(content.getBytes());  // 将字符串 转成 字节数组
				
				System.out.println("写入完成");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("源文件不存在，无法写入");
			
		}
		
	}
}
