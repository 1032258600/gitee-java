package part12单例设计模式;

public class SingletonTest {
	
	public static void main(String[] args) {
		// 创建类的实例
		/*Singleton singleton1 = new Singleton(); //编译错误，构造方法私有化了。
		Singleton singleton2 = new Singleton();
		Singleton singleton3 = new Singleton();
		System.out.println(singleton1.hashCode());
		System.out.println(singleton2.hashCode());
		System.out.println(singleton3.hashCode());*/
	    // 目前以上3个实例是不同的3个实例。
		
		// 创建单例类的实例
		
		Singleton instance1 = Singleton.getInstance();
		Singleton instance2 = Singleton.getInstance();
		Singleton instance3 = Singleton.getInstance();
		System.out.println(instance1.hashCode());
		System.out.println(instance2.hashCode());
		System.out.println(instance3.hashCode());
		
		Singleton2 instance4 = Singleton2.getInstance();
		Singleton2 instance5 = Singleton2.getInstance();
		Singleton2 instance6 = Singleton2.getInstance();
		System.out.println(instance4.hashCode());
		System.out.println(instance5.hashCode());
		System.out.println(instance6.hashCode());
		
		Singleton3 instance7 = Singleton3.getInstance();
		Singleton3 instance8 = Singleton3.getInstance();
		Singleton3 instance9 = Singleton3.getInstance();
		System.out.println(instance7.hashCode());
		System.out.println(instance8.hashCode());
		System.out.println(instance9.hashCode());
	}

}
