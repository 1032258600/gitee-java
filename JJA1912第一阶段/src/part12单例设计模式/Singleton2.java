package part12单例设计模式;
/**
 * 单例--懒汉模式
 * 延迟加载，线程不安全（以后会加入双锁进行检查）
 * @author Administrator
 *
 */
public class Singleton2 {
	
	// 拥有一个类的实例的静态成员
	private static Singleton2 instance = null; // 懒汉模式，延迟加载（懒加载），在多线程下不安全。

	// 私有化构造方法
	private Singleton2(){
		
	}
	
	// 提供一个公开的静态方法，返回类的唯一实例
	public static Singleton2 getInstance() {
		
		if (instance == null) {
			instance = new Singleton2();
		}
		return instance;
	}
}
