package part12单例设计模式;
/**
 * 静态内部类【最推荐的做法】
 * 延迟加载，线程安全。
 * @author Administrator
 *
 */
public class Singleton3 {
	
	// 构造方法私有化
	private Singleton3() {
		
	}
	
	// 静态内部类
	private static class SingletonHolder {
		// 拥有一个类实例的静态成员
		private static Singleton3 instanceHolder = new Singleton3();
	}
	
	// 提供一个公开的静态方法，返回类的唯一实例
	public static Singleton3 getInstance() {
			
		return SingletonHolder.instanceHolder;
	}

}
