package part12单例设计模式;
/**
 * 单例-饿汉模式
 * 立即加载，保证线程安全，但是可能造成内存资源浪费
 * @author Administrator
 *
 */
public class Singleton {
	
	// 拥有一个类的实例的静态成员变量
	private static Singleton instance = new Singleton(); // 饿汉模式，立即加载，保证线程安全。

	// 私有化构造方法
	private Singleton(){
		
	}
	
	// 提供一个公开的静态方法，返回类的唯一实例
	public static Singleton getInstance() {
		return instance;
	}
}
