package part16集合框架;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Arrays类 {

	public static void main(String[] args) {
		// 1 asList(T... a) 将数组转换成集合
		List<Object> asList = Arrays.asList();
		List<Object> asList2 = Arrays.asList(1,2,3);
		System.out.println(asList2);
		
		int array[] = new int[10];
		// 赋值
		array[0] = 1;
		
		List<int[]> asList3 = Arrays.asList(array);
		for (int[] is : asList3) {
			for (int i : is) {
				System.out.println(i);
			}
		}
		
		// 2 对数组进行直接排序
		int array2[] = new int[4];
		// 赋值
		array2[0] = 10;
		array2[1] = 1;
		array2[2] = 17;
		array2[3] = -9;
		
		for (int i : array2) {
			System.out.println(i);// 10 1 17 -9
		}
		
		// 排序：从小到大输出
		// 方式1： 冒泡排序
		// 方式2：利用Array类中sort方法 ： 表示自然顺序 数值类从小到大  字符串类按照字典排序
		Arrays.sort(array2);
		for (int i : array2) {
			System.out.println(i);// -9 1 10 17
		}
	
		// 字符串
		String strs[] = {"A","B","E","D","AB"};
		Arrays.sort(strs);
		for (String i : strs) {
			System.out.print(i + ","); //A,AB,B,D,E,
		}
		System.out.println("---------自定义比较器-----------");
		// 将字符串按照长度 从小到大
		Arrays.sort(strs, new Comparator<String>() { // 匿名内部类

			@Override
			public int compare(String o1, String o2) {
				if (o1.length() < o2.length()) {
					return -1;
				}
				if (o1.length() > o2.length()) {
					return 1;
				}
				return 0;
			}
		});
		
		// 输出
		for (String i : strs) {
			System.out.print(i + ","); //A,B,D,E,AB
		}
		
	}
}
