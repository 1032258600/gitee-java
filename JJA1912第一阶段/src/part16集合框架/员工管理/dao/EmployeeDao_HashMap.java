package part16集合框架.员工管理.dao;
/**
 * 数据库操作dao层
 * 临时：先使用集合来进行存储,以后会使用数据库进行数据的存储
 * @author Administrator
 *
 */

import java.util.HashMap;

import part16集合框架.员工管理.pojo.Employee;

public class EmployeeDao_HashMap {

	// 存储员工列表
	// Key是唯一的，可以将对象的唯一标识作为Key(员工id)
	// value是员工对象
	HashMap<Integer, Employee> hashMap = new HashMap<>();

	/**
	 * 思考： 方法名？ 参数？ 返回值？实现？
	 * 
	 * @param employee
	 *            添加的对象
	 * @return int 影响行数
	 */
	public int insert(Employee employee) {
		// 添加
		hashMap.put(employee.getEmpNo(), employee);
		return 1;
	}

	/**
	 * 查询所有员工列表
	 */
	public HashMap<Integer, Employee> query() {
		return hashMap;
	}

	/**
	 * 根据员工编号查询单个
	 * 
	 * @param empNo
	 *            员工编号
	 * @return 员工对象
	 */
	public Employee queryById(Integer empNo) {
		Employee employee = null;
		if (hashMap.containsKey(empNo)) { // 判断是否包含某一个key
			employee = hashMap.get(empNo);
		}
		return employee; // null / 对象
	}

	/**
	 * 根据用户名模糊查询
	 * 
	 * @param empName
	 *            员工姓名的关键字
	 * @return 员工列表
	 */
	public HashMap<Integer, Employee> queryByEmployeeName(String empNameKey) {
		// 存储符合条件的员工列表
		HashMap<Integer, Employee> result = new HashMap<>();
		// 遍历key集
		for (Integer key : hashMap.keySet()) {

			Employee employee = hashMap.get(key);
			if (employee.getEmpName().contains(empNameKey)) {
				// 符合条件
				result.put(employee.getEmpNo(), employee);
			}
		}
		return result; // 1种集合空列表  2有数据
	}

	/**
	 * 根据姓氏进行模糊查询
	 * 
	 * @param empName
	 *            员工姓氏的关键字
	 * @return 员工列表 以empNameKey开头的姓名，也就是例如 传入陈，查姓氏为"陈"的所有员工????
	 */
	public HashMap<Integer, Employee> queryByEmployeeName2(String empNameKey) {
		// 存储符合条件的员工列表
		HashMap<Integer, Employee> result = new HashMap<>();
		// 遍历key集
		for (Integer key : hashMap.keySet()) {

			Employee employee = hashMap.get(key);
			if (employee.getEmpName().startsWith(empNameKey)) {
				// 符合条件
				result.put(employee.getEmpNo(), employee);
			}
		}
		return result;
	}

	/**
	 * 根据员工编号进行删除
	 * 
	 * @param empNo
	 *            员工编号
	 * @return 返回影响行数
	 */
	public int deleteById(Integer empNo) {
		
		hashMap.remove(empNo);

		return 1;
	}

	/**
	 * 根据员工编号修改员工信息
	 * 
	 * @param employee
	 *            修改的对象
	 * @return int 影响行数
	 */
	public int updateById(Employee employee) {
		// 方式1: 直接覆盖
		hashMap.put(employee.getEmpNo(), employee);
		// 方式2: 先遍历，然后将新的值赋值给对象
		// 遍历key集
	/*	for (Integer key : hashMap.keySet()) {

			if (key == employee.getEmpNo()) {
				Employee emp = hashMap.get(key);
				emp.setEmpName(employee.getEmpName());
				emp.setSalary(employee.getSalary());
			}
			
			
		}*/
		return 1;
	}

}
