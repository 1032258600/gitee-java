package part16集合框架.员工管理.dao;
/**
 * 数据库操作dao层
 * 临时：先使用集合来进行存储,以后会使用数据库进行数据的存储
 * @author Administrator
 *
 */

import java.util.ArrayList;
import java.util.Iterator;

import part16集合框架.员工管理.pojo.Employee;

public class EmployeeDao {
	
	// 设置全局遍历 集合 存储员工列表
	ArrayList<Employee> list = new ArrayList<>(); // 临时充当数据库
	
	/**
	 * 思考： 方法名？ 参数？ 返回值？实现？
	 * @param employee 添加的对象
	 * @return int 影响行数 
	 */
	public int insert(Employee employee) {
		// 添加
		list.add(employee);
		return 1;
	}
	
	/**
	 * 查询所有员工列表
	 */
	public ArrayList<Employee> query() {
		return list;
	}
	
	/**
	 * 根据员工编号查询单个
	 * @param empNo 员工编号
	 * @return 员工对象
	 */
	public Employee queryById(Integer empNo) {
		Employee employee = null;
		for (Employee emp : list) {
			if (emp.getEmpNo() == empNo) {
				employee = emp;
			}
		}
		return employee;
	}
	
	/**
	 *  根据用户名模糊查询
	 * @param empName 员工姓名的关键字
	 * @return 员工列表
	 */
	public ArrayList<Employee> queryByEmployeeName(String empNameKey) {
		// 创建一个集合来存储符合条件的对象
		ArrayList<Employee> resultList = new ArrayList<>();
		for (Employee emp : list) { //从全部列表的集合进行一一遍历
//			if (emp.getEmpName().contains(empNameKey)==true) {
//				// 将符合条件的对象添加到指定的集合中。
//				resultList.add(emp);
//			}
			if (emp.getEmpName().indexOf(empNameKey) != -1) {
				// 将符合条件的对象添加到指定的集合中。
				resultList.add(emp);
			}
		
			
		}
		return resultList;
	}
	

	/**
	 * 根据员工编号进行删除
	 * @param empNo 员工编号
	 * @return 返回影响行数
	 */
	public int deleteById(Integer empNo) {
		// 遍历集合
		/*for (Employee employee : list) {
			if (employee.getEmpNo() == empNo) {
				list.remove(employee);
				return 1;
			}
		}
		return 0;*/
		
		Iterator<Employee> iterator = list.iterator();
		while (iterator.hasNext()) {
			Employee employee = (Employee) iterator.next();
			if (employee.getEmpNo() == empNo) {
				iterator.remove();
				return 1;
			}
		}
		return 0;
	}
	/**
	 * 根据员工编号修改员工信息
	 * @param employee 修改的对象
	 * @return int 影响行数 
	 */
	public int updateById(Employee employee) {
		// 遍历集合
		for (Employee e : list) {
			// employee是外部传递进来的员工对象   e是从集合中遍历出来的员工对象
			if (employee.getEmpNo() == e.getEmpNo()) {
				// 修改属性
				e.setEmpName(employee.getEmpName());
				e.setSalary(employee.getSalary());
				return 1;
			}
		}
		return 0;
	}
	
	/**
	 *  根据姓氏进行模糊查询
	 * @param empName 员工姓氏的关键字
	 * @return 员工列表
	 * 以empNameKey开头的姓名，也就是例如 传入陈，查姓氏为"陈"的所有员工????
	 */
	public ArrayList<Employee> queryByEmployeeName2(String empNameKey) {
		// 创建一个集合来存储符合条件的对象
		ArrayList<Employee> resultList = new ArrayList<>();
		for (Employee emp : list) { //从全部列表的集合进行一一遍历
				/*if (emp.getEmpName().indexOf(empNameKey)==0) {
					// 将符合条件的对象添加到指定的集合中。
					resultList.add(emp);
				}		*/
			if (emp.getEmpName().startsWith(empNameKey)) { // true
				// 将符合条件的对象添加到指定的集合中。
				resultList.add(emp);
			}
		}
		return resultList;
	}
	

}
