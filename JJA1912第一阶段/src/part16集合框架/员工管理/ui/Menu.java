package part16集合框架.员工管理.ui;

import java.util.ArrayList;
import java.util.Scanner;

import part16集合框架.员工管理.dao.EmployeeDao;
import part16集合框架.员工管理.pojo.Employee;

public class Menu {
	Scanner scanner = new Scanner(System.in);
	EmployeeDao empDao = new EmployeeDao();
	
	public static void main(String[] args) {
		new Menu().menu();
	}
	/**
	 * 添加的子菜单
	 */
	private  void addMenu() {
		System.out.println("请输入员工编号");
		int empNo = scanner.nextInt();
		System.out.println("请输入员工姓名");
		String empName = scanner.next();
		System.out.println("请输入员工工资");
		float salary = scanner.nextFloat();
		Employee employee = new Employee(empNo, empName, salary);
		int n = empDao.insert(employee);
		System.out.println(n>0?"添加成功":"添加失败");
	}
	/**
	 * 删除子菜单
	 */
	private void deleteMenu() {
		System.out.println("请输入删除的员工编号");
		int empNo = scanner.nextInt();
		int n = empDao.deleteById(empNo);
		System.out.println(n>0?"删除成功":"删除失败");
	}
	
	/**
	 * 修改子菜单
	 */
	private void updateMenu() {
		System.out.println("请输入修改员工编号");
		int empNo = scanner.nextInt();
		System.out.println("请输入修改的员工姓名");
		String empName = scanner.next();
		System.out.println("请输入修改的员工工资");
		float salary = scanner.nextFloat();
		// 构建一个员工对象
		Employee employee = new Employee(empNo, empName, salary);
		// 调用修改的方法，将构建的员工对象传递进去
		int n = empDao.updateById(employee);
		System.out.println(n>0?"修改成功":"修改失败");
	}
	/**
	 * 查询所有的方法
	 */
	private void queryAllMenu() {
		ArrayList<Employee> list = empDao.query();
		System.out.println("员工列表" + list);
	}
	
	/**
	 * 根据编号查询
	 */
	private void queryByIdMenu() {
		System.out.println("请输入要查询的员工编号：");
		int empNo = scanner.nextInt();
		Employee emp = empDao.queryById(empNo);
		if (emp==null) {
			System.out.println("查无此人");
		}else {
			System.out.println(emp);
		}
	}
	/**
	 * 根据名字
	 */
	private void queryByNameMenu() {
		System.out.println("请输入要查询的员工姓名关键字：");
		String empName = scanner.next();
		ArrayList<Employee> result = empDao.queryByEmployeeName(empName);
		for (Employee employee2 : result) {
			System.out.println(employee2);
		}
	}
	/**
	 * 根据姓氏
	 */
	private void queryByName2Menu() {
		System.out.println("请输入要查询的员工姓氏关键字：");
		String empName = scanner.next();
		ArrayList<Employee> result = empDao.queryByEmployeeName2(empName);
		for (Employee employee2 : result) {
			System.out.println(employee2);
		}
	}
	/**
	 * 主菜单
	 */
	public  void menu() {
		
		while (true) {
			System.out.println("请选择功能 1增加员工(员工入职)、2删除员工(员工离职)、3修改员工(信息变动)、4查询所有员工、5根据员工编号 6根据员工模糊姓名  7根据姓氏查询 8 退出系统");
			int key = scanner.nextInt();
			switch (key) {
			case 1:
				// 调用添加的方法
				addMenu();
				break;
			case 2:
				// 调用删除的方法
				deleteMenu();
				break;
			case 3:
				// 调用修改的方法
				updateMenu();
				break;
			case 4:
				// 调用查询员工的方法
				 queryAllMenu();
				break;
			case 5:
				// 调用根据员工编号的方法
				queryByIdMenu();
				break;
			case 6:
				// 调用根据员工姓名模糊查询的方法
				queryByNameMenu();
				break;
			case 7:
				// 根据姓氏进行查询 
				queryByName2Menu();
				break;
			case 8:
				// 退出系统
				System.exit(0);
			default:
				System.out.println("功能选择错误，请重新选择1-8");
				break;
			}
		}
	}
	
}
