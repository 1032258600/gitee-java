package part16集合框架;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Collections类 {

	public static void main(String[] args) {
		
		List<String> dest = new ArrayList<>();
		dest.add("a");
		dest.add("b");
		dest.add("c");
		dest.add("d");
		dest.add("e");
		List<String> src= new ArrayList<>();
		src.add("A");
		src.add("B");
		src.add("C");
		src.add("D");
		System.out.println("源列表：" + src);
		System.out.println("目的地列表：" +dest);
		//public static <T> void copy(List<? super T> dest,List<? extends T> src)
		//将所有元素从一个列表复制到另一个列表中。 操作完成后，目标列表中每个复制元素的索引将与源列表中的其索引相同。 
        // 目的地列表必须至少与源列表一样长。 如果它更长，则目的地列表中的剩余元素不受影响。 
		Collections.copy(dest, src);
		System.out.println("---copy之后---");
		System.out.println("源列表：" + src);
		System.out.println("目的地列表：" +dest);
		
		// 添加元素到集合中
//		boolean addAll(Collection<? super T> c, T... elements)将所有指定的元素添加到指定的集合。
		Collections.addAll(src, "Peaches 'n Plutonium", "Rocky Racoon"); 
		System.out.println("添加后的源列表：" + src);
		
		
		// sort(List<T> list) 按照自然顺序排序  数值类从小到大
		List<Integer> list = new ArrayList<>();
		list.add(100);
		list.add(10);
		list.add(1110);
		list.add(55);
		System.out.println("sort之前：" + list);
		Collections.sort(list);
		System.out.println("sort之后：" + list);
		
		// 现在我们希望，是按照从大到小，就需要自定义比较器
		// sort(List<T> list,Comparator<? super T> c)根据指定的比较器引起的顺序对指定的列表进行排序。
		System.out.println("-----sort自定义比较器----");
		// 方式1： 外部自定义一个类，传递进来
		Collections.sort(list, new MyIntegerComp());
		System.out.println("sort自定义排序之后：" + list);
		
		// 方式2：匿名内部类  直接在当前参数位置上创建接口的实现类，然后这个类没有名字，所以叫匿名内部类。
		// 匿名内部类的好处：减少类的创建
		Collections.sort(list, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return 0;
			}
		});
		
	}
}
// 外部自定义一个类
class MyIntegerComp implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		if (o1 < o2) {
			return 1;
		}
		if (o1 > o2) {
			return -1;
		}
		return 0;
	}
	
}
