package part16集合框架;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Iterator迭代器遍历 {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("C");
		list.add("B");
		// 遍历集合
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		// foreach
		for (String string : list) {
			System.out.println(string);
		}
		
		// 迭代器遍历:  可以将迭代器理解成一个容器
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) { // hasNext()判断是否有需要迭代的元素，有则返回true
			String str = (String) iterator.next(); // next获取迭代器中的下一个元素
			System.out.println(str);
			// 删除元素
			iterator.remove();
		}
		System.out.println("迭代器遍历删除之后：" + list);
		
		
	}
}
