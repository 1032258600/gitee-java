package part16集合框架.list接口;

import java.util.LinkedList;

public class LinkedList类_堆栈和队列 {
	public static void main(String[] args) {
//		LinkedList类_堆栈和队列.堆栈();
		LinkedList类_堆栈和队列.队列();
	}

	public static void 堆栈() {
		
		System.out.println("---模拟堆栈结构： 先进后出(后进先出)---");
		// 创建一个LinkedList类
		LinkedList<String> list = new LinkedList<>();
//		public void push(E e)将元素推送到由此列表表示的堆栈上。 换句话说，在该列表的前面插入元素。 此方法相当于addFirst(E) 。 
		list.push("A");
		list.push("B");
		list.push("C");
		
		// 输出
		System.out.println(list);
		
		// 获取堆栈中的第一个
		System.out.println("堆栈中的第一个元素：" + list.get(0));
//		peek() 检索但不删除此列表的头（第一个元素）。 
		System.out.println("堆栈中的第一个元素：" + list.peek());
		
//		poll() 检索并删除此列表的头（第一个元素）。 
		System.out.println("堆栈中的第一个元素并删除：" + list.poll());
//		public E pop()从此列表表示的堆栈中弹出一个元素。 换句话说，删除并返回此列表的第一个元素。 此方法相当于removeFirst() 。 
		System.out.println("堆栈中的第一个元素并删除：" + list.pop());
		
		// 输出
		System.out.println(list);
	}
	
	public static void 队列() {
		
		System.out.println("---模拟队列结构： 先进先出---");
		// 创建一个LinkedList类
		LinkedList<String> list = new LinkedList<>();
		
		// 添加元素
		list.add("001");
		list.add("002");
		list.add("003");
		
		// 获取元素 叫号
		System.out.println("队列 叫号：" + list.get(0) ); // 只获取，但是不删除
		System.out.println("列表元素：" + list);
//		public E pop()从此列表表示的堆栈中弹出一个元素。 换句话说，删除并返回此列表的第一个元素。 此方法相当于removeFirst() 。 
		System.out.println("队列 叫号 并且从列表中删除：" + list.pop() );
		System.out.println("列表元素：" + list);
	}
}
