package part16集合框架.list接口;

import java.util.ArrayList;


public class ArrayList类 {

	public static void main(String[] args) {
		// 创建一个ArrayList类的实例  有序可重复的集合，不同步(线程不安全)
//		List list = new ArrayList(); 
		ArrayList list = new ArrayList<>();// 构造一个初始容量为十的空列表。 
		
		// 1 添加  boolean add(Object e) 将指定的元素追加到此列表的末尾。  

		list.add("ABC");
		list.add(1);
		list.add(15.5);
		list.add(new B("小白"));
		
//		插入 void add(int index, Object element) 在此列表中的指定位置插入指定的元素。  
		list.add(0, "插入");
		
		
		// 是否可以添加重复的元素?
		list.add(1);
		
		// 2 输出集合大小 int size() 获取集合的长度
		System.out.println("集合长度：" + list.size());
		
		// 3 查询 Object get(int index) 返回此列表中指定位置的元素。 
		System.out.println("查询集合元素get:" + list.get(0));
		
		//  boolean contains(Object o) 如果此列表包含指定的元素，则返回 true 。 
		System.out.println("查询集合元素是否存在contains：" + list.contains(1));
		System.out.println("查询集合元素是否存在contains：" + list.contains(100));
		
		// indexOf(Object o) 返回此列表中指定元素的第一次出现的索引，如果此列表不包含元素，则返回-1。 
		System.out.println("查询集合元素的索引值indexOf：" + list.indexOf(1));
		
		// 4 删除
		/*E remove(int index) 
		删除该列表中指定位置的元素。  
		boolean remove(Object o) 
		从列表中删除指定元素的第一个出现（如果存在）。  */
		System.out.println("删除之前index=0：" + list.get(0));
		list.remove(0);
		System.out.println("删除之后index=0：" + list.get(0));
		
		list.remove("ABC");
		System.out.println("删除Object之后：" + list.get(0));
		
		// 想删除集合元素中的 1 这个对象
		list.remove(new Integer(1));
		System.out.println("删除Object之后：" + list.get(0));
		
		// 5 修改 E set(int index, E element) 用指定的元素替换此列表中指定位置的元素。 
		list.set(0, "HHHH");
		System.out.println("修改之后：" + list.get(0));
		
		// 6 判断集合是否为空
		System.out.println("判断集合是否为空：" + list.isEmpty());
		
		// 7 如何遍历集合?
		System.out.println("--普通for循环遍历--");
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + "  ");
		}
		System.out.println("--增强for循环遍历");
		for (Object obj : list) {
			System.out.print(obj + " ");
		}
		System.out.println();
		// 输出集合
		System.out.println(list.toString());
	}
}
class B{
	private String name;

	public B(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "B [name=" + name + "]";
	}
	
}
