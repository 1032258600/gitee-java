package part16集合框架.list接口;

import java.util.ArrayList;
import java.util.List;

public class 泛型 {

	public static void main(String[] args) {
		// 泛型： 规定存储的数据类型，在编译期对数据类型进行检查
		// 集合中如何应用泛型?
		List<Integer> list = new ArrayList<Integer>();
		list.add(1); // 自动装箱 
		list.add(new Integer(2));
		list.add(2);
//		list.add("AA"); // 编译错误 因为该集合规定了泛型是Integer，就只能存放Integer类型
		
		// 泛型擦除：泛型在运行期之后就不存在，被擦除。泛型只在编译器。
		
		
	}
}
