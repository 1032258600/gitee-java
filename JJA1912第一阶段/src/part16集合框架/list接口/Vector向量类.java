package part16集合框架.list接口;

import java.util.Vector;

public class Vector向量类 {

	public static void main(String[] args) {
		// Vector是一个可变数组，同步，线程安全。
		// 如何构建Vector类
		Vector<Integer> vector = new Vector<>();
		
		// 添加
		vector.add(1);
		vector.add(2);
		vector.add(3);
		// 输出
		System.out.println(vector.get(0));
		// 输出
		System.out.println(vector);
		
		for (Integer integer : vector) {
			System.out.println(integer);
		}
		// 其他方法和ArrayList几乎类似....
	}
}
