package part16集合框架.list接口;

import java.util.LinkedList;

public class LinkedList类 {
	
	public static void main(String[] args) {
		// 创建一个LinkedList类
		LinkedList<String> list = new LinkedList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add(null);
		list.add(null);
		list.add("C");
		// 新增的方法 添加
//		 addFirst(E e) 在该列表开头插入指定的元素。  
//		 void addLast(E e)  将指定的元素追加到此列表的末尾。  
		list.addFirst("GG");
		list.addLast("MM");
		
		// 输出
		System.out.println(list);

		// 新增方法 删除
//		E removeFirst() 从此列表中删除并返回第一个元素。  
//		E removeLast() 从此列表中删除并返回最后一个元素。  
		list.removeFirst();
		list.removeLast();
		System.out.println(list);
	
		
		// 其他方法和ArrayList是几乎类似,.....
	}

}
