package part16集合框架.map接口;

import java.util.Hashtable;

public class Hashtable类 {

	public static void main(String[] args) {
		Hashtable<Object,Object> hashtable = new Hashtable();
		// 添加：
		hashtable.put(100, "VBB");
		hashtable.put(1, "AAA");
		// 不允许将null设置key
		hashtable.put(null, "1"); // 运行时会出现空指针异常
		System.out.println(hashtable);
	}
}
