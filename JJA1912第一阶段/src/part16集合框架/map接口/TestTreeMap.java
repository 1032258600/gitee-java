package part16集合框架.map接口;

import java.util.Comparator;
import java.util.TreeMap;

public class TestTreeMap {

	public static void main(String[] args) {
		// TreeMap 有序映射集 对key加入自然顺序
		TreeMap<Integer, String> treeMap = new TreeMap<>();
		// 添加
		treeMap.put(1, "英雄");
		treeMap.put(11, "YY");
		treeMap.put(2, "友澍");
		// 不允许将null设置key
//		treeMap.put(null, "友澍"); // 编译时候不会出错，调用的时候会报空指针异常。
		// 输出集合 默认自然顺序是从小到大
		for (Integer key : treeMap.keySet()) {
			String value = treeMap.get(key);
			System.out.println("Key" + key + ",value：" +value);
		}

		// 自定义比较器 传入按照自己的规则
		TreeMap<Integer, String> treeMap2 = new TreeMap<>(new MyIntegerComparator2());
		// 添加
		treeMap2.put(100, "英雄");
		treeMap2.put(11, "YY");
		treeMap2.put(2000, "友澍");

		// 输出集合
		System.out.println(treeMap2);

	}
}
/**
 * 自定义比较器
 * 比较数字，从大到小输出
 * @author Administrator
 *
 */
class MyIntegerComparator2 implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		//原先的规则：从小到大
		//返回负整数，零或正整数，因为第一个参数小于，等于或大于第二个参数。 -1，0，或1。 
		//当第一个参数<第二个参数 返回值-1 
		//当第一个参数=第二个参数 返回值0
		//当第一个参数>第二个参数 返回值1
		//改变规则：从大到小输出
		if (o1<o2) {
			return 1;
		}
		if (o1>o2) {
			return -1;
		}

		return 0;
	}	
}
