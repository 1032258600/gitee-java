package part16集合框架.map接口;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class HashMap类 {

	public static void main(String[] args) {
		// 创建一个HashMap  无序映射集
		HashMap<Object, Object> hashMap = new HashMap<>();
		
		// 1 添加元素 put(key, value);
		hashMap.put(1, "A");
		hashMap.put("A", "HELLO");
		hashMap.put("10", "B");
		
		// 添加重复的key,后面的value会覆盖前面的value---》key不能重复的
		hashMap.put("10", "Bbbbb");
		
		// 添加key为null值，value为null
		hashMap.put(null, "AAAA");
		
		
		// 2 获取元素 get(Object key)
		System.out.println(hashMap.get(1)); // A
		System.out.println(hashMap.get("10"));//Bbbbb
		System.out.println(hashMap.get(null));//AAAA
		
		// 3清空集合
//		hashMap.clear();
		// 4集合大小
		System.out.println(hashMap.size());
		
		
		// 5删除 remove(Object key) 从该集合中删除指定键的映射（如果存在）。返回值是被删除的Value值 
		System.out.println( hashMap.remove(1));// A
		
		
		System.out.println("--遍历map集合---");
		// 方式1： 使用键集  keySet() 获取所有key。 
		Set<Object> keySet = hashMap.keySet();
		for (Object key : keySet) {
			System.out.println("key:" + key + ",value： " + hashMap.get(key));
		} 
		
		System.out.println("--遍历map集合---");
		// 方式2: 使用键值集 entrySet() 获取key-value映射集。 
		Set<Entry<Object, Object>> entrySet = hashMap.entrySet();
		for (Entry<Object, Object> entry : entrySet) {
			Object key = entry.getKey();
			Object value = entry.getValue();
			System.out.println("key:" + key + ",value： " + value);
		}
		
		System.out.println("--遍历map集合所有的value值---");
		// values() 获取所有的value值。 
		Collection<Object> values = hashMap.values();
		for (Object v : values) {
			System.out.println(v);
		}
		
		
		// 规定k和v的类型 K只能是整型 V只能是字符串类型
		HashMap<Integer, String> hashMap2 = new HashMap<>();
		hashMap2.put(1, "A");

	}
}
