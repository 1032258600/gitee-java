package part16集合框架.set接口;

import java.util.Comparator;
import java.util.TreeSet;

public class SortBook implements Comparator<SortBook>{

	private String bookName;
	private Float price;
	private Integer page;
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public SortBook(String bookName, Float price, Integer page) {
		super();
		this.bookName = bookName;
		this.price = price;
		this.page = page;
	}
	public SortBook() {
		super();
	}
	
	@Override
	public String toString() {
		return "SortBook [bookName=" + bookName + ", price=" + price + ", page=" + page + "]";
	}
	@Override
	public int compare(SortBook o1, SortBook o2) {
		
		if (o1.getPrice() > o2.getPrice()) {
			return 1;
		}
		if (o1.getPrice() < o2.getPrice()) {
			return -1;
		}
		return 0;
	}
	public static void main(String[] args) {
		// 创建5个图书对象
		SortBook sortBook1 = new SortBook("西游记", 5.5f, 100);
		SortBook sortBook2 = new SortBook("西游记", 10.5f, 200);
		SortBook sortBook3 = new SortBook("西游记", 1.56f, 1300);
		SortBook sortBook4 = new SortBook("西游记", 99.5f, 1100);
		SortBook sortBook5 = new SortBook("西游记", 0.5f, 100);
		// 存储到TreeSet
		TreeSet<SortBook> treeSet = new TreeSet<>(new SortBook()); // 将自定义的比较器类通过构造方法传递进来
		treeSet.add(sortBook1);
		treeSet.add(sortBook2);
		treeSet.add(sortBook3);
		treeSet.add(sortBook4);
		treeSet.add(sortBook5);
		
		// 遍历
		for (SortBook book : treeSet) {
			System.out.println(book);
		}
	}
	
	
}








