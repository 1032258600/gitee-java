package part16集合框架.set接口;

import java.util.HashSet;
import java.util.Iterator;
/**
 * HashSet是无序，不重复的集合
 * 不同步
 * 不支持普通for循环遍历。
 * @author Administrator
 *
 */
public class HashSet类 {

	public static void main(String[] args) {
		// 创建一个HashSet类
		HashSet<String> hashSet = new HashSet<>();
		
		// 添加
		hashSet.add("AA");
		hashSet.add("你好");
		hashSet.add("C");
		hashSet.add(null);
		
		System.out.println(hashSet); // 无序输出
		System.out.println( hashSet.add("E") ); // true 添加成功
		// 能否添加重复的元素？不能，添加执行返回false
		// 不会重复那到底是 出现编译错误？出现异常信息？覆盖？执行添加方法是失败？
	    System.out.println( hashSet.add("C") ); // false 添加方法没有执行成功
		System.out.println(hashSet);

		// 求大小
		System.out.println(hashSet.size());
		
		// 1 Hast不支持普通for循环进行遍历  不支持get(index) 因为它没有索引
	/*	for (int i = 0; i < hashSet.size(); i++) {
			System.out.println(hashSet.);
		}*/
		
		// 1 foreach
		for (String string : hashSet) {
			System.out.println("foreach：" + string);
		}
		
		// 2 迭代器
		Iterator<String> iterator = hashSet.iterator();
		while (iterator.hasNext()) { // hasNext()判断是否有需要迭代的元素，有则返回true
			String str = (String) iterator.next(); // next获取迭代器中的下一个元素
			System.out.println("iterator：" + str);
		}
	}
}
