package part16集合框架.set接口;

import java.util.HashSet;

public class HashSet类2 {
	public static void main(String[] args) {
		
		// 创建一个HashSet类
//		HashSet是Set接口的实现类，底层是HashMap
//		HashSet特点：1迭代元素的时候是无序 2元素不重复
		HashSet<Person> hashSet = new HashSet<>();
		hashSet.add(new Person(1,"耀钦", "男"));

		Person person1 = new Person(2,"CN", "男");
		System.out.println("person1的hashCode：" +  person1.hashCode());
		hashSet.add(person1);
		System.out.println(hashSet);
		
		// 再添加一个"重复"的对象，因为此时new的动作，两个Person其实haseCode值不同，所以能够被添加成功。
		Person person2 = new Person(2,"CN", "男");
		System.out.println("person2的hashCode：" +  person2.hashCode());
		hashSet.add(person2);
		System.out.println(hashSet);
		
		// 没有重写equals的时候，返回false，因为person1和person2都是new出来，两个引用指向的地址是不一样的。
		// 重写equals之后，返回true，虽然person1和person2都是new出来，两个引用指向的地址也是不一样的，但是我们在equals方法中，判断如果两个对象的pid相同就返回true
		System.out.println(person1.equals(person2));  
	
		// 现在需求：id一样，就认为是同一个对象，就不该被添加成功。
		// 必须重写Person类中equals和hashCode方法，改变比较对象的规则
		
	}

}
