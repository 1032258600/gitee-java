package part16集合框架.set接口;

public class Person {
	private Integer pid; // 编号是唯一
	private String pname;
	private String gender;
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Person() {
		
	}

	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public Person(Integer pid, String pname, String gender) {
		super();
		this.pid = pid;
		this.pname = pname;
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Person [pid=" + pid + ", pname=" + pname + ", gender=" + gender + "]";
	}
	/**
	 * 重写equals方法：判断对象的指定属性是否相同
	 * 如果pid属性相同，equals返回true
	 * 目前我们只判断id这个属性.
	 */
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) { // 空对象
			return false; 
		}
		
		if (this == obj) { //两个引用指向的地址相同
			return true; 
		}
		
		if (obj instanceof Person) { // 判断传递进来的obj是不是Person类的实例(对象)，这个判断可以不做。
			Person person = (Person)obj;
			if (this.pid == person.getPid()) {
				return true;
			}
		}
		
		return false; // 不是同一个对象
	}
	/**
	 * 重写hashCode： 将对象 pid属性作为hashCode的返回值
	 */
	@Override
	public int hashCode() {
		return this.pid;
	}
	
	
}
