package part16集合框架.set接口;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSet类_自定义比较器2 {

	public static void main(String[] args) {

		// 创建一个TreeSet树集： 元素不能重复，有自然顺序
		TreeSet<String> treeSet3 = new TreeSet<>();
		treeSet3.add("ACD");
		treeSet3.add("acd");
		treeSet3.add("GAB");
		treeSet3.add("ABBBB");
		treeSet3.add("CCD");
		treeSet3.add("CAAA");
		treeSet3.add("EEEE");
		System.out.println(treeSet3);
		
		/**
		 * TreeSet(Comparator<? super E> comparator) 
		        构造一个新的，空的树集，根据指定的比较器进行排序。
		 */
		// 自定义一个比较器，实现字符串类，按照长度从小到大排序输出
		TreeSet<String> treeSet4 = new TreeSet<>(new MyStringComparator());
		treeSet4.add("ACD");
		treeSet4.add("acd");
		treeSet4.add("GAB");
		treeSet4.add("ABBBB");
		treeSet4.add("CCD");
		treeSet4.add("CAAA");
		treeSet4.add("EEEE");
		System.out.println(treeSet4);
	}
}
/**
 * 自定义比较器类 必须实现比较接口
 * @author Administrator
 *
 */
class MyStringComparator implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		// 原先的比较规则是从小到大
//		比较其两个参数的顺序。 返回负整数，零或正整数，因为第一个参数小于，等于或大于第二个参数。 
//		在前面的描述中，符号sgn( ) 表达式表示数学符号函数，其定义根据表达式的值是否为负，零或正返回的-1一个，0，或1。 
//		当o1 < 02 返回 -1
//		当o1 = 02 返回 0
//		当o1 > 02 返回 1
		// 改变规则： 按照长度从小到大
		if (o1.length() < o2.length()) {
			return -1;
		}
		if (o1.length() > o2.length()) {
			return 1;
		}
		return 0;
	}	
}










