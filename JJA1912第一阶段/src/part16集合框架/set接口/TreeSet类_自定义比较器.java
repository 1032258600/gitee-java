package part16集合框架.set接口;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSet类_自定义比较器 {

	public static void main(String[] args) {

		// 创建一个TreeSet树集： 元素不能重复，有自然顺序
		TreeSet<Integer> treeSet = new TreeSet<>();
		treeSet.add(1);
		treeSet.add(0);
		treeSet.add(90);
		treeSet.add(55);
		treeSet.add(-55);
		// 数值类，输出是按照从小到大
		System.out.println(treeSet);
		
		/**
		 * TreeSet(Comparator<? super E> comparator) 
		        构造一个新的，空的树集，根据指定的比较器进行排序。
		 */
		// 自定义一个比较器，实现数值类，从大到小进行排序输出
		TreeSet<Integer> treeSet2 = new TreeSet<>(new MyIntegerComparator());
		treeSet2.add(1);
		treeSet2.add(0);
		treeSet2.add(90);
		treeSet2.add(55);
		treeSet2.add(-55);
		// 数值类，输出是按照从小到大
		System.out.println(treeSet2);
	}
}
/**
 * 自定义比较器类 必须实现比较接口
 */
class MyIntegerComparator implements Comparator<Integer>{
	@Override
	public int compare(Integer o1, Integer o2) {
		// 原先的比较规则是从小到大
//		比较其两个参数的顺序。 返回负整数，零或正整数，因为第一个参数小于，等于或大于第二个参数。 
//		在前面的描述中，符号sgn( ) 表达式表示数学符号函数，其定义根据表达式的值是否为负，零或正返回的-1一个，0，或1。 
//		当o1 < 02 返回 -1
//		当o1 = 02 返回 0
//		当o1 > 02 返回 1
		// 改变规则：从大到小
		if (o1 < o2) {
			return 1;
		}
		if (o1 > o2) {
			return -1;
		}
		return 0;
	}
	
}










