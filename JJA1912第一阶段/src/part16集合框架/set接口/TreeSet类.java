package part16集合框架.set接口;

import java.util.TreeSet;

public class TreeSet类 {

	public static void main(String[] args) {
		// 创建一个TreeSet树集： 元素不能重复，有自然顺序
		TreeSet<Integer> treeSet = new TreeSet<>();
		treeSet.add(1);
		treeSet.add(0);
		treeSet.add(90);
		treeSet.add(55);
		treeSet.add(-55);
		// 数值类，输出是按照从小到大
		System.out.println(treeSet);
		
		TreeSet<String> treeSet2 = new TreeSet<>();
		treeSet2.add("01");
		treeSet2.add("00001");
		treeSet2.add("10");
		treeSet2.add("19");
		treeSet2.add("5555");
		treeSet2.add("25555");
		// 字符串类，按照字典的顺序 ，第一位比较完，比较第二位
		System.out.println(treeSet2);
		
		TreeSet<String> treeSet3 = new TreeSet<>();
		treeSet3.add("ACD");
		treeSet3.add("acd");
		treeSet3.add("GAB");
		treeSet3.add("ABBBB");
		treeSet3.add("CCD");
		treeSet3.add("CAAA");
		treeSet3.add("EEEE");
		treeSet3.add("a");
		
		// 字符串类，按照字典的顺序 ，第一位比较完，比较第二位
		System.out.println(treeSet3);
		
	}
}
