package part16集合框架;

public class Array数组 {

	public static void main(String[] args) {
		// 如何定义一个数组
		int[] array = new int[10];
		
		// 如何对数组元素赋值
		array[0] = 1;
		array[2] = 10;
		array[9] = 90;
		
		// 如何对数组进行访问 
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] +" ");
		}
		System.out.println();
		// 增加for循环
		for (int i : array) {
			System.out.print(i +" ");
		}
	}
}
