package part19File类;

import java.io.File;
import java.io.IOException;

public class File类 {

	public static void main(String[] args) throws IOException {
		// 关联上一个文件对象
		File file = new File("F:\\Hello.java"); ////指明详细的路径以及文件名，请注意双斜线
		File file2 = new File("F:/Hello.java");
		
		System.out.println("--文件属性：---");
//		System.out.println(file.getName()); // 获取文件名
//		System.out.println(file.getAbsolutePath()); // 获取文件绝对路径
//		System.out.println(file.length()); // 获取文件长度
//		System.out.println(file.exists()); //判断文件是否存在
	
		// 判断文件是否存在
		if (file.exists()) {
			System.out.println("文件对象存在");
			// 判断文件是文件还是目录
			if (file.isFile()) {
				System.out.println(file.getName()+"这是一个文件");
			}
			if (file.isDirectory()) {
				System.out.println(file.getName()+"这是一个目录");
			}
		}else {
			System.out.println("文件不存在");
		}
		
		System.out.println("---------创建文件-----------");
		File file3 = new File("F:\\t.java"); // 关联上一个文件
		if (file3.exists()) {
			System.out.println("文件对象存在");
		}else {
			System.out.println("文件不存在");
			// 创建文件
			boolean flag = file3.createNewFile(); // 创建文件，不能创建目录
			System.out.println(flag ? "创建新文件成功" : " 创建失败");
		}
		
		System.out.println("---------创建目录(文件夹)---------");
		File file4 = new File("F:\\t"); // 关联上一个目录
		if (file4.exists()) {
			System.out.println("目录存在");
		}else {
			System.out.println("目录不存在");
			// 创建目录
			boolean flag = file4.mkdir(); // 创建目录,文件夹
			System.out.println(flag ? "创建目录成功" : " 创建失败");
		}
		
		System.out.println("---------删除文件---------");
		File file5 = new File("F:\\t.java"); // 关联上一个目录
		if (file5.exists()) {
			System.out.println("文件存在");
			// 删除文件
			boolean flag =file5.delete();
			System.out.println(flag ? "删除文件成功" : " 删除失败");
		}
	}
}
