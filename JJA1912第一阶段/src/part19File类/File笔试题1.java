package part19File类;

import java.io.File;
import java.util.Scanner;

public class File笔试题1 {

	// 从命令行输入一个路径，如果路径存在，则将该目录下的所有子文件和子文件夹列举出来。
	// 如果路径不存在，则提示不存在。
	
	public static void main(String[] args) {
		// File[] listFiles()	返回文件夹内的子文件与子文件夹的数组
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入一个路径：");
		String pathname = scanner.nextLine();
		
		// 关联该路径的文件对象
		File file = new File(pathname);
		
		// 判断是否存在
		if (file.exists()) {
			
			if (file.isFile()) {
				System.out.println("文件："+ file.getAbsolutePath());
				
			}else if (file.isDirectory()) {
				
				// 列举目录下的所有子文件和子文件夹
				File[] listFiles = file.listFiles();
				// 遍历数组
				for (File f : listFiles) {
					if (f.isFile()) {
						System.out.println("文件："+ f.getAbsolutePath());
					}else if (f.isDirectory()) {
						System.out.println("目录："+ f.getAbsolutePath());
					}
				}
			}
		}else {
			System.out.println("路径不存在");
		}
	}
}
