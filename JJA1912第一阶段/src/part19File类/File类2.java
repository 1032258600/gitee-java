package part19File类;

import java.io.File;
import java.io.IOException;

public class File类2 {

	public static void main(String[] args) throws IOException {
		// 关联当前电脑D盘中的t.text文件
		File file = new File("D:\\t.text");

		// 关联当前项目中的一个t.text的文件
		File file2 = new File("t.text");
		if (file2.exists()) {
			// 读取属性
			System.out.println(file2.getName() + " 存在");
		} else {
			System.out.println(file2.getName() + " 不存在");
			// 创建文件
			boolean flag = file2.createNewFile();
			System.out.println(flag ? "创建新文件成功" : " 创建失败");
		}
	}
}
