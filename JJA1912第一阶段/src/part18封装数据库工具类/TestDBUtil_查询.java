package part18封装数据库工具类;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.jupiter.api.Test;

public class TestDBUtil_查询 {

	@Test
	public void 查所有() {
		
		ResultSet resultSet = DBUtil.doQuery("select * from employee");
		// 遍历
		// 遍历结果集
		try {
			while (resultSet.next()) {
				int empNo = resultSet.getInt("emp_no");
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				System.out.println(empNo + "  "  + empName +"  " + salary);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void 查单个() {
		int empNo = 1;
		ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_no = ?",empNo);
		// 遍历
		// 遍历结果集
		try {
			while (resultSet.next()) {
//				int empNo = resultSet.getInt("emp_no");
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				System.out.println(empNo + "  "  + empName +"  " + salary);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void 查编号和员工名() {
		int empNo = 5;
		String empName = "一鸣";
		ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_no = ? and emp_name = ?",empNo,empName);
		// 遍历
		// 遍历结果集
		try {
			while (resultSet.next()) {
//				int empNo = resultSet.getInt("emp_no");
//				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				System.out.println(empNo + "  "  + empName +"  " + salary);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void 查询单个字段() {

		ResultSet resultSet = DBUtil.doQuery("select emp_name as ename from employee");
		// 遍历
		// 遍历结果集
		try {
			while (resultSet.next()) {

				String empName = resultSet.getString("ename");

				System.out.println( empName);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
