package part18封装数据库工具类;

import org.junit.jupiter.api.Test;

public class TestDBUtil_更新 {

	@Test
	public void 添加() {
		
		int n = DBUtil.doUpdate("insert into employee values(null,?,?)", "QH",155.5f);
		System.out.println( n>0 ? "添加成功" : "添加失败");
	
	}
	
	@Test
	public void 删除() {
		int n = DBUtil.doUpdate("delete from employee where emp_no = ?", 7);
		System.out.println( n>0 ? "删除成功" : "删除失败");
	}
	
	@Test
	public void 更新() {
		int n = DBUtil.doUpdate("update employee set salary = ?", 2000);
		System.out.println(n);
		System.out.println( n>0 ? "更新成功" : "更新失败");
	}
	
	
}
