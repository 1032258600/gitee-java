package part18封装数据库工具类.员工管理.ui;

import java.util.ArrayList;
import java.util.Scanner;

import part18封装数据库工具类.员工管理.pojo.Employee;
import part18封装数据库工具类.员工管理.service.EmployeeService;

public class Menu {
	Scanner scanner = new Scanner(System.in);
	// 声明Service业务层
	EmployeeService employeeService = new EmployeeService();
	
	public static void main(String[] args) {
		new Menu().menu();
	}
	/**
	 * 添加的子菜单
	 */
	private  void addMenu() {
		
		System.out.println("请输入员工姓名");
		String empName = scanner.next();
		System.out.println("请输入员工工资");
		float salary = scanner.nextFloat();
//		Employee employee = new Employee(null, empName, salary);
		Employee employee = new Employee(empName, salary);
		boolean flag = employeeService.addEmployee(employee);
		System.out.println( flag ?"添加成功":"添加失败");
	}
	/**
	 * 删除子菜单
	 */
	private void deleteMenu() {
		System.out.println("请输入删除的员工编号");
		int empNo = scanner.nextInt();
		boolean flag = employeeService.deleteById(empNo);
		System.out.println(flag ? "删除成功":"删除失败");
	}
	
	/**
	 * 查询所有的方法
	 */
	private void queryAllMenu() {
		ArrayList<Employee> list = employeeService.getAllEmployee();
		System.out.println("员工列表" + list);
	}
	
	/**
	 * 根据编号查询
	 */
	private void queryByIdMenu() {
		System.out.println("请输入要查询的员工编号：");
		int empNo = scanner.nextInt();
		Employee emp = employeeService.getEmployeeById(empNo);
		if (emp==null) {
			System.out.println("查无此人");
		}else {
			System.out.println(emp);
		}
	}
	/**
	 * 修改子菜单
	 */
	private void updateMenu() {
		System.out.println("请输入修改编号：");
		int empNo = scanner.nextInt();
		System.out.println("请输入员工姓名");
		String empName = scanner.next();
		System.out.println("请输入员工工资");
		float salary = scanner.nextFloat();
		
		Employee employee = new Employee(empNo, empName, salary);
		boolean flag = employeeService.updateById(employee);
		System.out.println( flag ?"添加成功":"添加失败");
	}
	/**
	 * 根据名字
	 */
	private void queryByNameMenu() {
		System.out.println("请输入员工姓名的关键字：");
		String empNameKey = scanner.next();
		ArrayList<Employee> list = employeeService.getEmployeeByName(empNameKey);
		System.out.println("员工列表" + list);
		
	}
	/**
	 * 根据姓氏
	 */
	private void queryByName2Menu() {
		System.out.println("请输入员工姓的关键字：");
		String empNameKey = scanner.next();
		ArrayList<Employee> list = employeeService.getEmployeeByName2(empNameKey);
		System.out.println("员工列表" + list);
	}
	/**
	 * 主菜单
	 */
	public  void menu() {
		
		while (true) {
			System.out.println("请选择功能 1增加员工(员工入职)、2删除员工(员工离职)、3修改员工(信息变动)、4查询所有员工、5根据员工编号 6根据员工模糊姓名  7根据姓氏查询 8 退出系统");
			int key = scanner.nextInt();
			switch (key) {
			case 1:
				// 调用添加的方法
				addMenu();
				break;
			case 2:
				// 调用删除的方法
				deleteMenu();
				break;
			case 3:
				// 调用修改的方法
				updateMenu();
				break;
			case 4:
				// 调用查询员工的方法
				 queryAllMenu();
				break;
			case 5:
				// 调用根据员工编号的方法
				queryByIdMenu();
				break;
			case 6:
				// 调用根据员工姓名模糊查询的方法
				queryByNameMenu();
				break;
			case 7:
				// 根据姓氏进行查询 
				queryByName2Menu();
				break;
			case 8:
				// 退出系统
				System.exit(0);
			default:
				System.out.println("功能选择错误，请重新选择1-8");
				break;
			}
		}
	}
	
}
