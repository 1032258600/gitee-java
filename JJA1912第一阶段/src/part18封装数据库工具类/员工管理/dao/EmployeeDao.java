package part18封装数据库工具类.员工管理.dao;

/**
 * 数据库操作dao层
 * @author Administrator
 *
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import part18封装数据库工具类.员工管理.pojo.Employee;
import part18封装数据库工具类.员工管理.utils.DBUtil;

public class EmployeeDao {

	/**
	 * @param employee
	 *            添加的对象
	 * @return int 影响行数
	 */
	public int insert(Employee employee) {

		return DBUtil.doUpdate("insert into employee values(null,?,?)", employee.getEmpName(), employee.getSalary());

	}

	/**
	 * 根据员工编号进行删除
	 * 
	 * @param empNo
	 *            员工编号
	 * @return 返回影响行数
	 */
	public int deleteById(Integer empNo) {

		return DBUtil.doUpdate("delete from employee where emp_no = ?", empNo);
	}
	/**
	 * 根据员工编号修改员工信息
	 * 
	 * @param employee
	 *            修改的对象
	 * @return int 影响行数
	 */
	public int updateById(Employee employee) {

		return DBUtil.doUpdate("update employee set emp_name = ?, salary = ? where emp_no = ?", 
				employee.getEmpName(), employee.getSalary(), employee.getEmpNo());
	}

	/**
	 * 查询所有员工列表
	 */
	public ArrayList<Employee> query() {
		// 定义一个集合存储所有的列表
		ArrayList<Employee> list = new ArrayList<>();
		// 使用JDBC封装类
		ResultSet resultSet = DBUtil.doQuery("select * from employee");
		// 遍历结果集
		try {
			while (resultSet.next()) {
				int empNo = resultSet.getInt("emp_no");
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				list.add(new Employee(empNo, empName, salary));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 根据员工编号查询单个
	 * 
	 * @param empNo
	 *            员工编号
	 * @return 员工对象
	 */
	public Employee queryById(Integer empNo) {
		Employee employee = null;
		// 使用JDBC封装类
		ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_no = ?", empNo);
		// 遍历结果集
		try {
			while (resultSet.next()) {
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				employee = new Employee(empNo, empName, salary);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return employee;
	}

	/**
	 * 根据用户名模糊查询
	 * 
	 * @param empName
	 *            员工姓名的关键字
	 * @return 员工列表
	 */
	public ArrayList<Employee> queryByEmployeeName(String empNameKey) {
		// 定义一个集合存储所有的列表
		ArrayList<Employee> list = new ArrayList<>();
		// 使用JDBC封装类  select * from employee where emp_name like '%张三%'
		ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_name like ?","%" + empNameKey + "%");
		//ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_name like concat('%',?,'%')",empNameKey);
		// 遍历结果集
		try {
			while (resultSet.next()) {
				int empNo = resultSet.getInt("emp_no");
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				list.add(new Employee(empNo, empName, salary));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	

	/**
	 * 根据姓氏进行模糊查询
	 * 
	 * @param empName
	 *            员工姓氏的关键字
	 * @return 员工列表 以empNameKey开头的姓名，也就是例如 传入陈，查姓氏为"陈"的所有员工????
	 */
	public ArrayList<Employee> queryByEmployeeName2(String empNameKey) {
		// 定义一个集合存储所有的列表
		ArrayList<Employee> list = new ArrayList<>();
		// 使用JDBC封装类  select * from employee where emp_name like '%张三%'
		ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_name like ?" , empNameKey + "%");
		//ResultSet resultSet = DBUtil.doQuery("select * from employee where emp_name like concat(?,'%')",empNameKey);
		// 遍历结果集
		try {
			while (resultSet.next()) {
				int empNo = resultSet.getInt("emp_no");
				String empName = resultSet.getString("emp_name");
				float salary = resultSet.getFloat("salary");
				list.add(new Employee(empNo, empName, salary));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
