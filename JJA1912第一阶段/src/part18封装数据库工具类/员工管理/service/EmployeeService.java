package part18封装数据库工具类.员工管理.service;

import java.util.ArrayList;

import part18封装数据库工具类.员工管理.dao.EmployeeDao;
import part18封装数据库工具类.员工管理.pojo.Employee;
/**
 * Employee相关业务层
 * @author Administrator
 *
 */
public class EmployeeService {

	EmployeeDao empDao = new EmployeeDao();
	
	/**
	 * 添加业务
	 */
	public boolean addEmployee(Employee employee) {
		// 业务层调用dao层
		int n = empDao.insert(employee);
		return n > 0 ? true : false;
	}
	
	/**
	 * 删除业务
	 */
	public boolean deleteById(Integer empNo) {
		// 业务层调用dao层
		int n = empDao.deleteById(empNo);
		return n > 0 ? true : false;
	}
	/**
	 * 更新业务
	 */
	public boolean updateById(Employee employee) {
		// 业务层调用dao层
		int n = empDao.updateById(employee);
		return n > 0 ? true : false;
	}
	
	/**
	 * 查询所有业务
	 */
	public ArrayList<Employee> getAllEmployee() {
		return empDao.query();
	}
	/**
	 * 查询单个业务
	 */
	public Employee getEmployeeById(Integer empNo) {
		return empDao.queryById(empNo);
	}
	/**
	 * 模糊查询业务
	 */
	public ArrayList<Employee> getEmployeeByName(String empNameKey) {
		return empDao.queryByEmployeeName(empNameKey);
	}
	
	/**
	 * 模糊查询业务
	 */
	public ArrayList<Employee> getEmployeeByName2(String empNameKey) {
		return empDao.queryByEmployeeName2(empNameKey);
	}
}
