package part03类和对象;

public class PersonTest {

	public static void main(String[] args) {
		 
		// 创建Person类的对象，创建Person类的实例
		Person person = new Person(); // 通过new关键字在堆中开辟内存空间  Person()是Person类的无参构造方法 
		// 对成员属性赋值   成员运算符 .
		person.name = "伟滨";
		person.age = 18;
		person.gender = "男";
		// 调用方法
		person.showInfo();
		person.eat();
		
		// 创建第2个对象
		Person person2 = new Person();
		person2.name = "小白";
		person2.age = 16;
		person2.gender = "男";
		// 调用方法
		person2.showInfo();
		person2.eat();
		
		person.showInfo();
		person.eat();
		
		// 创建第3个对象
		Person person3 = new Person();
		person3.showInfo();
		person3.showInfo();
		
		
		
	
	}
}
