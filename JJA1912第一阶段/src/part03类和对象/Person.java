package part03类和对象;
/**
 * 类
 * @author Administrator
 *
 */
public class Person {
	
	// 成员属性 
	public String name; // 姓名
	public int age;     // 年龄
	public String gender; // 性别
	
	// 成员方法
	/*访问修饰符 返回值类型  方法名(参数列表){
		   // 方法体
		}*/
	
	// 无参数无返回值方法
	public void eat() {
		System.out.println(name + " 在吃汉堡!");
	}
	
	// 显示个人信息的方法
	public void showInfo() {
		System.out.println("姓名：" + name +", 年龄： " + age + ", 性别： " + gender);
	}

}
