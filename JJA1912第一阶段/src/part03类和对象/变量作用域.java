package part03类和对象;

public class 变量作用域 {

	public int num; // 全局变量：

	public void t1() {
		int num2 = 150; // 局部变量
		num = 10;
		System.out.println("t1：" + num); // 10
		System.out.println("t1：" + num2);
	}

	public void t2() {
		num = 20;
		System.out.println("t2：" + num);
//		System.out.println("t2：" + num2);// 编译错误，num2是t1方法中的局部变量
	}
	
	public static void main(String[] args) {
		变量作用域  var = new 变量作用域();
		var.num = 100;
		System.out.println(var.num); //100
		
		// 调用方法
		var.t1();
		System.out.println(var.num); //10
		
		int condition= 1;
		int c = 100;  // 方法内的局部变量
		if (condition==1) {
			int b = 10; // if条件内的局部变量
			c = 200; // 对方法内的局部变量赋值
			System.out.println(b);
			
		}
//		System.out.println(b); // 编译错误，因为b是if条件语句中的局部变量
		System.out.println(c); // 编译通过

	}
}
