package part03类和对象;
/**
 * 自定义方法
 * @author Administrator
 *
 */
public class Method {

	/**
	 * 无参数无返回值方法
	 * void 表示该方法没有返回值
	 */
	public void show() {
		System.out.println("无参数无返回值方法");
	}
	
	/**
	 * 有1个参数无返回值方法
	 * void 表示该方法没有返回值
	 */
	public void showWithParam(int num) { // 形参 形式参数
		System.out.println("有参数无返回值方法：  入参值" + num  );
	}
	
	/**
	 * 有2个参数无返回值方法
	 * @param num  第一数
	 * @param num2  第二数
	 */
	public void showWithParam2(int num, int num2) { // 形参 形式参数
		System.out.println("有参数无返回值方法：  入参值的和" + (num + num2));
	}
	
	/**
	 * 功能：专门用来计算2个数的累加和
	 * 有2个参数 有返回值方法
	 * @param num  第一数
	 * @param num2  第二数
	 * @return 返回累加和
	 */
	public int showWithReturn(int num, int num2) { // 形参 形式参数
		int sum = num + num2;
		return sum;
	}
	
	/**
	 * 无参数 有返回值方法
	 * @param num  第一数
	 * @param num2  第二数
	 * @return 返回累加和
	 */
	public double showWithReturn2() { // 形参 形式参数

		return 1.15;
	}
	
	public static void main(String[] args) {
		// 测试方法的调用? 
		// 如何Method类中的方法?
		Method method = new Method();
		// 对象名.方法名()
		method.show();
		
		// 调用有参数的方法  
		method.showWithParam(30); // 实参 实际参数
		
		// 调用有参数的方法
		int a = 150;
		method.showWithParam(a); // 实参 实际参数
		
		// 调用2个参数的方法
		int b = 100,c = 200;
		method.showWithParam2(b, c);
		
		// 调用有返回值的方法
		int result = method.showWithReturn(150, 350);  // 定义一个变量来接收方法的返回值
		System.out.println("有参数有返回值：" +  result);
		
		int result2 = method.showWithReturn(250, 350);
		System.out.println("有参数有返回值：" +  (result2 + 1000));
		
		double result3 = method.showWithReturn2();
		System.out.println("无参数有返回值： " + result3);
		
		
		
	}
}
