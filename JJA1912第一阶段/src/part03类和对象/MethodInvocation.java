package part03类和对象;

/**
 * 方法的调用
 *  ①通过对象名.方法名(实际参数)进行调用
	②同一个类中方法之间相互调用  通过方法名(实际参数)调用
	③方法自己调用自己，递归调用，必须杜绝死循环情况。
	④用static修饰的方法称为静态方法，直接使用 类名.静态方法名(实际参数)调用
 * @author Administrator
 *
 */
public class MethodInvocation {
	
	public void t() {
		
		System.out.println("t");
	
	}
	
	public void t2() {
		
		System.out.println("t2");
		t();// 方法之间可以相互调用  直接 方法名()
	}

	/**
	 * 静态方法  static关键字修饰的方法
	 * @param args
	 */
	public static void t3() {
		
		System.out.println("t3是静态方法");
		
	}
	
	public static void main(String[] args) {
		// 方法调用方式1： 对象名.方法名()
		MethodInvocation mi = new MethodInvocation();
		mi.t();	
		mi.t2();
		
		// 类名.静态方法名()
		MethodInvocation.t3();
		
	}
}
