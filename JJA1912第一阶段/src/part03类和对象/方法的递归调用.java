package part03类和对象;

public class 方法的递归调用 {

	/*public void t() {
		
		t(); // 会死循环
	}*/
	
    // 计算 n的阶乘    例如5! = 5*4*3*2*1
	public int cal(int n) {
		if (n==1) {
			return 1; // 递归的出口
		}
		return n * cal(n-1);	
	}
	
	
	public static void main(String[] args) {
		方法的递归调用 f = new 方法的递归调用();
//		f.t();
		int result = f.cal(5);
		System.out.println("5的阶乘：" +  result);
		
		result = f.cal(10);
		System.out.println("10的阶乘：" +  result);
	}
}
