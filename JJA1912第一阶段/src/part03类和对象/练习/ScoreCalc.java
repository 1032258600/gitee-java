package part03类和对象.练习;

public class ScoreCalc {
	
	// 成员属性
	public float java;
	public float c;
	public float db;
	
	// 计算平均分
	public String avg() {
		float avg = (java+c+db)/3;
		return String.format("%.2f", avg); // 保留2位小数  String.format(匹配的格式，转换的数据) 返回值String
	}
	
	// 计算总分
	public String sum() {
		float sum = java+c+db;
		return String.format("%.2f", sum);
	}

	public static void main(String[] args) {
		ScoreCalc  scoreCalc = new ScoreCalc();
		scoreCalc.java = 100.58f;
		scoreCalc.c = 60.58f;
		scoreCalc.db = 70.88f;
		String sum = scoreCalc.sum();
		String avg = scoreCalc.avg();
		System.out.println("总分：" +sum +",平均分：" + avg );
		
	}
}
