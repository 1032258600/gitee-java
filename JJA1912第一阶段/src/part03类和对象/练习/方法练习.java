package part03类和对象.练习;

public class 方法练习 {

	// 输出2个数的最大值。
	public void compare(int num1, int num2) {
		System.out.println(num1>num2 ? num1 : num2);
	}
	
	// 输出2个数的最大值。
	public int compare2(int num1, int num2) {
		  return num1>num2 ? num1 : num2;
	}
	
	
	public static void main(String[] args) {
		// 对象名.方法名
		方法练习 f = new 方法练习();
		f.compare(150, 10);
		
		int max = f.compare2(120, 250);// 快速定义返回值 快捷键 shilt + alt + L
		System.out.println("最大值：" +  max);
	}
}
