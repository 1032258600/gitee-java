package part03类和对象.练习;

public class AnimalTest {

	public static void main(String[] args) {
		
		// 创建对象
		Animal animal = new Animal();
		// 赋值
		animal.name = "哈士奇";
		animal.color = "黑白";
		animal.gender = "雄性";
		animal.showInfo();
		
		Animal animal2 = new Animal();
		// 赋值
		animal2.name = "泰迪";
		animal2.color = "棕色";
		animal2.gender = "雌性";
		animal2.showInfo();
	}
}
