package part03类和对象.练习;

public class Animal {

	// 共同的属性
	public String name;
	public String color;
	public String gender;
	
	// 共同方法-- 显示基本信息
	public void showInfo() {
		System.out.println("动物基本信息： 名称"  + name + ", 颜色： " + color + ", 性别 " + gender);
	}
}
