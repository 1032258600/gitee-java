package part01基础语法;

public class 循环_dowhile {
	
	public static void main(String[] args) {
		
		// while 先判断再执行
		boolean flag = false;
		while (flag == true) {
			System.out.println("while循环");
		}
		
		// dowhile 先执行一次，再判断
		flag = false;
		do {
			// 重复的操作
			System.out.println("do--while循环");
			// 循环条件改变
		}while(flag == true);
		
		
		// 输出5次hello
		int i = 1;
		do {
			System.out.println("do--while循环  HELLO");
			// 循环条件改变
			i++;
		} while (i<=5);
		
	}

}
