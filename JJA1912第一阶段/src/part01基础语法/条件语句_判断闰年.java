package part01基础语法;

import java.util.Scanner;

public class 条件语句_判断闰年 {

	public static void main(String[] args) {
		/*从控制台接收一个年份，判断该年份是否是闰年。如果是闰年则输出 xxx年是闰年。如果不是闰年，反之。。

		闰年： 年份能被4整除但不能被100整除；年份能被400整除*/
		
		Scanner scanner = new Scanner(System.in);
		
		int year = scanner.nextInt();
		
		if ( (year % 4 == 0 && year % 100 != 0 ) ||  (year % 400 == 0) ) {
			
			System.out.println( year  +" 年是闰年");
		} else {
			System.out.println( year  +" 年不是闰年");
		}
		
		scanner.close();
	}
}
