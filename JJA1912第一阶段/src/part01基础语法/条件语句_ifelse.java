package part01基础语法;

import java.util.Scanner;

public class 条件语句_ifelse {

	public static void main(String[] args) {
		System.out.println("---if----");
		
//		方式1：判断分数如果大于等于90,则输出优秀
//		if(逻辑表达式 / boolean的值){
//			   // 条件成立执行的代码块
//		}
		Scanner input = new Scanner(System.in);
		System.out.println("请输入分数：");
		int score = input.nextInt();
		System.out.println("分数： " +  score);

		if (score >= 90) {
			// 条件成立执行的语句
			System.out.println("优秀");
		}
		
		System.out.println("结束条件判断");
		
//		方式2：判断分数如果大于等于90,则输出优秀，否则输出一般
//		if(逻辑表达式 / boolean的值){
//			//当条件成立执行
//		}else{
//			//当条件不成立执行
//		}
		
		System.out.println("---if else-----");
		System.out.println("请输入分数：");
		score = input.nextInt();
		if (score >= 90) {
			System.out.println("优秀");
			System.out.println("很强");
		} else {
			System.out.println("一般");
			System.out.println("很弱");
		}
		
		// 关于if..else 写法
		System.out.println("---if和else中有且执行一句语句的简写方式-----");
		
		int num = 100;
		if (num==100) 
			System.out.println("num为100"); // 如果if中执行的语句有且只有一句，就可以省略掉花括号{}
		else 
			System.out.println("num不是100");//如果else中执行的语句有且只有一句，就可以省略掉花括号{}
		
		System.out.println("结束条件判断");
		
		
		System.out.println("---if....else if...else-----");
//		方式3：判断分数如果大于等于90,则输出优秀，如果大于80，输出良好，如果大于60，输出及格，其他为不及格。
//		if(条件1){
//			//当条件1成立执行
//
//		}else if(条件2){
//			//当条件2成立执行
//
//		}else if(条件3){
//			//当条件3成立执行
//
//	    }else{
//			//当条件1-3不成立执行
//	    } 
//		
		score = 85; 
		if (score >= 90) {
			System.out.println("优秀");
		} else if (score >= 80) {
			System.out.println("良好");
		} else if (score >= 60) {
			System.out.println("及格");
		} else {
			System.out.println("不及格");
		}
		
		
	}
}








