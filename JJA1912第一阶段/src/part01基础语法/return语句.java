package part01基础语法;

public class return语句 {

	public static void main(String[] args) {
		
		
		for (int i = 1; i <= 5; i++) {
			
			if (i==3) {
				//break; // 结束当前for循环
				//continue; // 结束当前这一次循环，进入下一次循环
				return; // 结束当前main方法的程序运行
			}
			System.out.println(i);
			
		}
		
		System.out.println("结束");
		
		int i = 3;
		if (i==3) {
			System.out.println("I==3");
			return;
		}
		
		System.out.println("结束");
	}
}
