package part01基础语法;

public class 循环_while {

	public static void main(String[] args) {
		
		// 输出5句helloworld
		System.out.println("helloworld");
		System.out.println("helloworld");
		System.out.println("helloworld");
		System.out.println("helloworld");
		System.out.println("helloworld");
		
		// 循环 目的：解决重复性操作
		
		// while   先判断后执行
		/*while ( 循环条件 ) {
			  //重复的操作
		      //必须设置循环条件的变化
		}*/
		
		int i = 1; // 控制循环次数
		while ( i <= 5 ) {
			System.out.println("循环");
			// 必须设置循环条件的改变,否则会导致死循环
			i++;
		}
		
		
		// 计算1+2+3+..100累加和 5050
		int sum = 0;
		int x = 1;
		while (x <= 100) {
			sum = sum + x;
			x++;
		}
        System.out.println("1-100累加和："  + sum);
        
        
        // 计算1+2+3+..100 奇数累加和 
        
        sum = 0;
        x = 1;
        while (x <= 100) {
        	if (x % 2 != 0) { //奇数
        		sum = sum + x;
			}
        	x++;
		}
        
        System.out.println("1-100 奇数累加和："  + sum);
        sum = 0;
        x = 1;
        while (x <= 100) {
        
        	sum = sum + x;
			
        	x += 2; //等价于 x = x + 2
		}
        System.out.println("1-100 奇数累加和："  + sum);
        
		
		
	}
}
