package part01基础语法;

public class debug调式技巧 {

	public static void main(String[] args) {

		// debug

		// System.out.println(1/0);

		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		}

			
		// 自己使用debug模式感受双重for循环中的运行过程
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= 5; j++) {
				if (j == 3) {
					break;
				}
				System.out.println("i： " + i + ",j " + j); // 11 12 21 22 31 32 41 42 51 52
			}
		}
	}
}
