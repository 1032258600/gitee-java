package part01基础语法;

import java.util.Scanner;

public class 循环_练习 {
	public static void main(String[] args) {
		
		/*1使用while循环或者dowhile循环实现买单
		实现不间断的输入商品编号(int)、价格、数量，直到用户选择结束买单为止，显示出购买的总金额。
		提示：设置一个标杆作为循环的条件*/
	
		/*Scanner scanner = new Scanner(System.in);
		// 计算总金额
		float sum = 0;
		boolean flag = true; //开始买单
		while (flag==true) {
			
			System.out.println("请输入商品编码:");
			int productNo = scanner.nextInt();
			System.out.println("请输入商品单价:");
			float price = scanner.nextFloat();
			System.out.println("请输入商品数量:");
			int count = scanner.nextInt();
			
			sum = sum + (price * count);
			
			// 设置循环条件的改变
			System.out.println("是否继续买单 true继续 false结束");
			flag =  scanner.nextBoolean();	// flag==false，不会继续循环	
		}
		// 循环外输出总金额
		System.out.println("总金额：" + sum);
		
		scanner.close();*/
		

		/*2 使用for循环打印直角三角形-->双重for循环
		 *
		 **
		 ***
		 ****
		*/
		for (int i = 1; i <= 4; i++) { // 控制行数
			
			for (int j = 1; j <= i; j++) { // 控制每一行打印的次数
				System.out.print("*"); // 不换行
			}
			// 换行
			System.out.println();
		}
	}

}










