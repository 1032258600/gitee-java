package part01基础语法;

import java.util.Scanner;

public class 条件语句_switch练习 {

	public static void main(String[] args) {
		// 通过switch..case语句实现菜单

		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();

		switch (n) {
		case 1:
			System.out.println("执行登录业务");
			// 调用一个自定义方法 登录业务
			break;
		case 2:
			System.out.println("执行注册业务");
			break;
		case 3:
			System.out.println("执行退出业务");
			System.exit(0); // 结束整个程序的运行   不会继续执行代码，也就是30行不会被执行
			break;
		default:
			System.out.println("输入有误");
			break;
		}

		System.out.println("练习");

		// 通过switch..case语句实现：从控制台接收年份和月份，输出该月有多少天？ 大月 小月 2月
		System.out.println("请输入年份:");
		int year = scanner.nextInt();
		System.out.println("请输入月份:");
		int month = scanner.nextInt();

		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:	
		case 10:
		case 12:
			System.out.println("31天"); //特殊情况可以合并case语句，省略到部分break
			break;
		case 4:	
		case 6:
		case 9:
		case 11:
			System.out.println("30天");
			break;
		case 2:
			// 判断闰年或者平年
			if ( (year % 4 == 0 && year % 100 != 0 ) ||  (year % 400 == 0) ) {
				System.out.println("29天");
			} else {
				System.out.println("28天");
			}
			break;
		default:
			System.out.println("输入错误");
			break;
		}
	}
}
