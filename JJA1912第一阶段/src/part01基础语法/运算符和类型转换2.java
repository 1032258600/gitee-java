package part01基础语法;

public class 运算符和类型转换2 {

	public static void main(String[] args) {
		short s = 1;
		s = (short) (s + 1); // 先计算右侧的 s+1，然后赋值左侧s，因为1是int类型，所以s+1依然是int类型
		s += 1; // +=是一个运算符，解析成 s = (short) (s+1)
	}
}
