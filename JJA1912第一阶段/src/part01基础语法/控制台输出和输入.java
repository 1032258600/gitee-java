package part01基础语法;

import java.util.Scanner;

public class 控制台输出和输入 {
	
	public static void main(String[] args) {
		// 控制台输出
		// 换行输出
		System.out.println("将内容输出到控制台");
		System.out.println("将内容输出到控制台");
		// 不换行输出
		System.out.print("将内容输出到控制台");
		System.out.print("将内容输出到控制台");
		System.out.println("");
		
		
		// 控制台输入 Scanner
		// 创建一个Scanner类的对象 对象名sc
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个整数：");
		// 调用对象中的方法  对象名.方法名()
		int a = sc.nextInt();
		System.out.println(a);
		
		System.out.println("请输入一个小数：");
		float f = sc.nextFloat();
		
		System.out.println(f);
		
		System.out.println("请输入一个字符串：");
	
		String str = sc.next();   // abc 123
		System.out.println(str); // abc
		// next方法接收字符串，以 enter，空格作为结束符
		
		// 如果输入的字符串含有空格 
		System.out.println("请输入一个字符串2：");
		
		sc.nextLine();
		
		// nextLine方法接收整行字符串，以 enter作为结束符
		String str2 = sc.nextLine();   // abc 123
		System.out.println(str2); // abc 123
	}
}
