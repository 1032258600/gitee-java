package part01基础语法;

/**
 * 
1 类是通过class关键字修饰，类是java的基础结构。
2 class类名命名规则： 首字母大写的驼峰原则
3 一个类可以有多个方法,启动主程序的是main方法(主函数)。
4 每一个语句结束必须加分号
5 java程序创建类的规则：
①一个java文件中可以有多个类
②但是被public修饰的类只能有一个
③被public修饰的类的类名必须和该java文件的文件名一样。
 * @author Administrator
 *
 */
public class Hello {

	
	public static void main(String[] args) {
		
		// 书写语句
		System.out.println("HHH");
		System.out.println("WORD");

	}
	
	public  void test() {
		System.out.println("TEST");
	}

}

// 定义一个类
class Hello2{
	
}
