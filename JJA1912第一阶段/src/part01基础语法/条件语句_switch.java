package part01基础语法;

public class 条件语句_switch {

	public static void main(String[] args) {
		// 条件语句switch..case
		int b = 10;
		int num = 2;
		switch (num) { // key 是要判断的变量 byte  char  short int String 类型
			case 1:    // value值 是key的可能值，常量值
				// 执行语句
				System.out.println("1");
				break; // break关键词：结束掉switch判断
			case 2:
				System.out.println("2");
				break;
			case 3:
				System.out.println("3");
				break;
			case 4:
				System.out.println("4");
				break;
			case 5:
				System.out.println("5");
				break;
	
			default: //默认
				System.out.println("不属于1-5");
				break;
			}
		
		// 练习： 输入一个等级，根据等级输出对应的语句  A A B B
		String grade = "A";
		if ("A".equals(grade)) {
			System.out.println("A");
		}else if ("B".equals(grade)) {
			System.out.println("B");
		} else {
			System.out.println("其他");
		}
		
		switch (grade) {
		case "A":
			System.out.println("A");
			System.out.println("优秀");
			break;
		case "B":
			System.out.println("B");
			break;
		default:
			System.out.println("其他");
			break;
		}
		
		// 思考： 多重ifelse 和 switch 有什么区别??
//		相同点： 都可以用来多种情况的判断
//		不同点： 多重if既可以判断等值，还可以判断区间，而switch只能判断等级，switch一般用来做菜单栏。
		
		
		
	}
}
