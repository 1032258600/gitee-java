package part01基础语法;

public class 循环_for {

	public static void main(String[] args) {

		int i = 1;
		/*
		 * while ( 循环条件 ) { //重复的操作 //必须设置循环条件的变化 }
		 */

		/*
		 * for (初始化;循环条件;条件变化) { //循环操作 }
		 */

		// 输出5句hello -- 擅长处理已经知道会执行多少次的循环操作

		for (int j = 1; j <= 5; j++) {
			System.out.println("for循环 hello");

		}

		// 计算1..100累加和
		int sum = 0;
		for (int j = 1; j <= 100; j++) {
			sum = sum + j;
		}
		System.out.println("sum：" + sum);

		// 计算1..100 奇数累加和
		sum = 0;
		for (int j = 1; j <= 100; j++) {
			if (j % 2 != 0) {
				sum = sum + j;
			}

		}
		System.out.println("奇数sum：" + sum);

		// 计算1..100 奇数累加和
		sum = 0;
		for (int j = 1; j <= 100; j=j+2) {
				sum = sum + j;
		}
		System.out.println("奇数sum：" + sum);

	}
}



