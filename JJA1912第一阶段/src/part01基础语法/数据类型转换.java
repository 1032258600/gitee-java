package part01基础语法;
public class 数据类型转换 {

	public static void main(String[] args) {
		
//		自动转换: 低精度---> 高精度
		float f = 15;
		
//		强制转换: 高精度---> 低精度
		float f2 = (float) 15.5; // 将double数据类型强转成float类型
		
		// 不同精度之间做运算
		int a = 1;
	    float b = 1.5f;
	    System.out.println(a+b); //2.5 低精度和高精度做运算符，最终结果是高精度的数据类型
	    
	    int a2 = 1;
	    int b2 = 1;
	    System.out.println(a2+b2); // 2
	    System.out.println(15/10); // 1
	    System.out.println(15/10.0); // 1.5
	    
	    // char和int相互转换， 会涉及到一个ASCII码的概念
	    char c = 'A';
	    System.out.println(c); // A
	    int a3 = c;
	    System.out.println(a3); //65 ASCII码中 65对应大写字母A 97对应小写字母a 65-90A-Z 97-122a-z
	    
	    c = 'B';
	    System.out.println(c); // B
	    a3 = c;
	    System.out.println(a3); //66
	    
	    int i = 97;
	    char c2 = (char) i;
	    System.out.println(c2); //a
	        
//		非自动非强制
	    String str = "130";
	    System.out.println(str+100); // 130100
	    // 如何将字符串130 转成 整数130
	    int i2 = Integer.parseInt(str);
	    System.out.println(i2+100); //230
	}
}
