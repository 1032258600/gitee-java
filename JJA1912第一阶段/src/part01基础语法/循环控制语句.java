package part01基础语法;

public class 循环控制语句 {

	public static void main(String[] args) {

		for (int i = 1; i <= 5; i++) {
			System.out.print(i + " "); // 1 2 3 4 5
		}

		System.out.println("");
		System.out.println("--break--");
		for (int i = 1; i <= 5; i++) {
			if (i == 3) {
				break; // 结束当前整个for循环
			}
			System.out.print(i + " ");// 1 2
		}

		System.out.println("");
		System.out.println("--continue--");
		for (int i = 1; i <= 5; i++) {
			if (i == 3) {
				continue; // 结束当前这一次循环，进入下一次循环
			}
			System.out.print(i + " ");// 1 2 4 5
		}

		System.out.println("");
		System.out.println("循环外");

		// 双重for循环中
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= 5; j++) {
				if (j == 3) {
					break;
				}
				System.out.println("i： " + i + ",j " + j); // 11 12 21 22 31 32 41 42 51 52
			}
		}

		// 双重for循环中
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= 5; j++) {
				if (j == 3) {
					continue;
				}
				System.out.println("i： " + i + ",j " + j); // 11 12 14 15 21 22 24 25 31 32 34 35 41 42 44 45 51 52 54
															// 55
			}
		}
		
		System.out.println("--return----");
		for (int i = 1; i <= 5; i++) {

			for (int j = 1; j <= 5; j++) {
				if (j == 3) {
					return;
				}
				System.out.println("i： " + i + ",j " + j); //11  12 
			}
		}

		// 1+2+3..100奇数累加和
		// 计算1..100 奇数累加和
		int sum = 0;
		for (int j = 1; j <= 100; j++) {
			if (j % 2 != 0) {
				sum = sum + j;
			}

		}
		System.out.println("奇数sum：" + sum);

		// 计算1..100 奇数累加和
		sum = 0;
		for (int j = 1; j <= 100; j = j + 2) {
			sum = sum + j;
		}
		System.out.println("奇数sum：" + sum);
		
		
		sum = 0;
		for (int j = 1; j <= 100; j++) {
			if (j % 2 == 0) {
				continue; //偶数跳过
			}
			sum = sum + j;
		}
		System.out.println("奇数sum：" + sum);
	}
}
