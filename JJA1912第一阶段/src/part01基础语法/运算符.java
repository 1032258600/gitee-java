package part01基础语法;

public class 运算符 {
	
	public static void main(String[] args) {
		// 算术运算符 +  -  *  / %  ++ --
		int num1 = 10;
		int num2 = 15;
		System.out.println(num1+num2); // 25
		System.out.println(num1-num2); // -5
		System.out.println(num1*num2); // 150
		System.out.println(num1/num2); // 0
		System.out.println(num1%num2); // 求余数 10
		System.out.println(8%3); // 求余数 2
		System.out.println(8%2); // 余数0 以后会利用% 模运算符做奇偶数判断
		
		// ++ 自己+1
		int num3 = 10;
		System.out.println("num3:" + num3++);//10   num3++ 表示的num3自增1，因为++在后面，先运行，再+1
		System.out.println("num3:" + num3);//11  
		
		int num4 = 10;
		System.out.println("num4:" + ++num4);//11   ++num4 表示的num4自增1，因为++在前面，再+1，后运行
		System.out.println("num4:" + num4);//11
		
		// 玩一玩
		int num5 = 2;
		System.out.println("num5：" +  ( ++num5 + num5++ )); // 3+3=6 
		System.out.println("num5：" +  num5); // 4 
		
		num5 = 2;
		System.out.println("num5：" +  ( num5++ + ++num5)); // 2+4=6 
		System.out.println("num5：" +  num5); // 4
		
		// -- 自己减-1
		int num6 = 10;
		System.out.println("num6:" + num6--);//10   num6-- 表示的num6自减1，因为--在后面，先运行，再-1
		System.out.println("num6:" + num6);// 9
		
		int num7 = 10;
		System.out.println("num7:" + --num7);//9  ++num7 表示的num7自减1，因为--在前面，先-1，再运行
		System.out.println("num7:" + num7);//9
				
		
		// 赋值运算符  =  *= += -= /=
		int a = 100; // 将右边的值赋值给左边的变量

		a += 150; // 等价于a = a + 150;
		a *= 150; // a = a * 150;
		
		System.out.println("a ：" + a);
		
		
		// 比较运算符  >  < >=  <=  等于==  不等!=  
		System.out.println(1>2); // false
		System.out.println(2>=2);// true
		System.out.println(2==2);// true
		System.out.println(2!=2);// false
		
		System.out.println("----逻辑运算符-------");
		// 逻辑运算符  与&&  或||  非!   返回值是boolean类型 
		// 与&& ： 当所有的表达式都为真，结果才为真。
		System.out.println(1>2 && 2==2); //false
		// 或|| ： 当所有的表达式只要有一个为真，结果就为真。
		System.out.println(1>2 || 2==2); //true
		// 非！： 将表达式的结果取反
		System.out.println(!true); //false
		System.out.println(!(1>2)); //true
		
		System.out.println("----&和&&有什么区别-------");
		// 经典题目：  位运算符&也可以当做逻辑与判断
		// &和&&有什么区别??
		int b = 2;
		System.out.println(b>2 && ++b>10); // false
		System.out.println("输出b：" + b);  // 2
		
		b = 2;
		System.out.println(b>2 &  ++b>10); //false
		System.out.println("输出b：" + b);   //3
		
		
		// &和&&都可以用来逻辑与(and)的操作，当所有表达式都为真，结果才为真。
		// &&，不仅是逻辑与，还具有“短路与”的功能，即如果第一个表达式为false，则不会计算第二个表达式。
		// & 位运算符，也可以当做逻辑与运算符，即如果第一个表达式为false，依然会计算第二个表达式。
		
		// 三目运算符 （条件运算符）
		// 表达式 ? 表达式为真执行的语句 : 表达式为假执行的语句
		int c = 100;
		System.out.println( c>100 ? "c大于100" : "c小于100"); // c小于100
		
		int n = 1;
		System.out.println(n>0 ? "添加成功": "添加失败"); // 添加成功
	}

}
