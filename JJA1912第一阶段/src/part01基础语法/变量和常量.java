package part01基础语法;

public class 变量和常量 {
	
	// 声明变量
	
	// main函数是程序的入口函数
	public static void main(String[] args) {
		// 声明变量
		// 声明变量格式   数据类型 变量名;
		// 方式1：
		int age; // 声明了一个int整型的变量age，存储年龄
		 
		// 赋值
		age = 18;
		// 调用变量  直接通过变量名
//		System.out.println(age);  // 如果age没有赋值,会编译错误
		System.out.println("age: " + age);  // age: 18
		
		// 方式2： 声明同时赋值
		int num = 100;
		System.out.println("num：" + num);// num：100
		
		// 方式3： 相同数据类型的变量可以统一声明
		int a,b=10,c;
		a = 100;
		b = 200;
		c = 300;
		System.out.println("a：" + a +  ",b: " + b + ",c: " + c); // a：100,b: 200,c: 300
		b = 2000;
		System.out.println("b:" + b);// b:2000	
		
		System.out.println("-------------------------------------");// b:2000	
		
		// 声明常量
		final int z;
		
		// 对常量进行赋值
		z = 100;
		
		System.out.println("常量z： " + z); // 常量z： 100
		
		// 对常量再一次赋值,编译错误，因为常量一旦赋值，就不可以再改变
		//z = 300;
		
		
		// 声明常量同时赋值  常量一般是全大写
		final int Y = 300; 
		
	}

}
