package part01基础语法;

public class 复习 {
	
	public static void main(String[] args) {
		
		for (int i = 0; i < args.length; i++) {
			for (int j = 0; j < args.length; j++) {
				if (j==1) {
//					break;
//					continue;
					return;
				}
			}
		}
		
		if (1==1) {
			System.out.println();
		} else {
			System.out.println();
		}
		
		
		// 数组：静态数组、声明数组的同时需要定义长度，长度不能改变，是一组数据类型相同的，有序。
		int[] array = new int[2];
		int[] array2 = {1,2,3};
		
		// 访问 通过索引值 下标  从0开始 到 长度-1
		System.out.println(array.length);
//		System.out.println(array[2]);// 运行错误：ArrayIndexOutofBoundsException
		
		// 数组的遍历
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		int[] array3 = new int[2];
		array3[0] = 1;
		array3[1] = 1;
//		array3[2] = 1;
		
		array3 = new int[3];
	}

}
