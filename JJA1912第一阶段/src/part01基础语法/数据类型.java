package part01基础语法;

import java.math.BigDecimal;

public class 数据类型 {

	public static void main(String[] args) {
		// 声明变量： 数据类型 变量名 =  初始值;
		// 8种基本数据类型
		
		// 字节类型 IO流中使用
		byte b = 1;
		System.out.println(b);
		// byte范围 -128 到 127 
		byte b2 = -128;
		System.out.println(--b2); // 127    -- 自身减1
		
		// 字符类型: 表示一个字符，使用单引号，可以存储中文字符
		char gender = '男';
		System.out.println(gender);
		
		char w = 'w';
		System.out.println(w);
		
		// 整型 short int long
		short s = 32767; //范围 -32768到32767
		int num = 100;   
		// long类型的数据需要使用L标识。建议大写的L
		long phone = 156069580869L;
		
	
	    // 浮点型： 带小数点 float单精度 (6位) double双精度(12位)
		double money = 558.8888;
		System.out.println(money);
		
		// 在java中，默认的小数点都是双精度double类型 1.2也是double
		// float类型的数据需要使用f标识。
		float money2 = 558.88f;
		
		
		// boolean布尔类型  只有2个值 true真 false假
		// 一般用作标杆
		boolean flag = false;
		boolean flag2 = true;
		
		
		// 浮点型精度丢失问题?
		System.out.println(1.2-1); // 0.19999999999999996
		
		// 为什么出现这个问题？ 因为计算机的建模方式是二进制形式，对分数形式不能够精确。
		// 如何解决这个问题？ 使用
		BigDecimal num1 = new BigDecimal("1.2"); //记得需要以字符串的形式传入
		BigDecimal num2 = new BigDecimal("1");
		System.out.println( num1.subtract(num2));//0.2 精度正常
		
	
		System.out.println("------------------------------");
		
		int a2,b3,c2; // 此时3个变量都是存储在 栈 中
		
		// 引用数据类型  字符串
		String str = "abc"; // str引用名存在"栈"中，  abc存在"堆"中
		// new关键字表示创建一个引用数据类型(对象)
		String str2 = new String("123");//str2引用名存在"栈"中，  new String("123")存在"堆"中 
	}
}
