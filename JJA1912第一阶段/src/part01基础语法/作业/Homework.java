package part01基础语法.作业;

import java.util.Scanner;

public class Homework {
	
	public static void main(String[] args) {

	// 1、根据天数（46）计算周数和剩余的天数
		
		int days = 46;
		int weeks = days / 7;
		int day = days % 7;
		System.out.println("46天，周数：" + weeks  + "，余 " + day + "天");

	/*
	 * 2、 实现一个数字加密器，从控制台输入一个整数，加密规则是： 加密结果 = （整数*10+5）/2 + 3.14159 加密结果仍为一整数
	 */
		Scanner input = new Scanner(System.in);
		
		System.out.println("请输入要加密的整数:");
		// 接收一个整数
		int num = input.nextInt();
		// 加密
		num = (int) ((num*10+5)/2 + 3.14159);
		System.out.println("加密：" +  num);
		
	// 3、从控制台分别输入3个数,并求最大值;使用三目运算符
		System.out.println("请输入整数1:");
		// 接收一个整数
		int num1 = input.nextInt();
		System.out.println("请输入整数2:");
		// 接收一个整数
		int num2 = input.nextInt();
		System.out.println("请输入整数3:");
		// 接收一个整数
		int num3 = input.nextInt();
		
		// 先比较num1和num2，大的赋值变量max，max和num3进行比较
		int max = num1 > num2 ? num1 : num2;
		
		max = max > num3 ? max : num3;
		
		System.out.println("三个数：" + num1 + "," + num2 +"," + num3 + "  最大值：" +  max);
		
	// 4、从控制台输入两个数字,将这2个数字交换后输出
		System.out.println("请输入a:");
		// 接收一个整数
		int a = input.nextInt();
		System.out.println("请输入b:");
		// 接收一个整数
		int b = input.nextInt();
		
		// 使用临时变量，进行交换
		int temp = a;
		a = b;
		b = temp;
		
		System.out.println("交换后的:a: " +  a  + ",b：" + b);
		
		// 关闭输入对象
		input.close();
	}
}
