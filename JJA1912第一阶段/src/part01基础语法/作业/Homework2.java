package part01基础语法.作业;

public class Homework2 {

	public static void main(String[] args) {
		// 99乘法表
		for (int i = 1; i <= 9; i++) {  // 控制行数 9行
			
			for (int j = 1; j <= i; j++) { // 控制每行列数 
				System.out.print(j +"*" + i + "=" +(j*i) + "\t"); //  \t转义字符 表示一个tab键							
			}
			System.out.println();
		}
		
		
		// 打印三角形
		/* 
		    *     
		  *  *        
		 *  *  *       
	    *  *  *  *  
		第一行：空格3个 星星1
		第二行：空格2个 星星2
		第三行：空格1个 星星3
		第四行：空格0个 星星4*/
		
		for (int i = 1; i <= 4; i++) {
			
			// 控制空格
			for (int k = 0; k < 4-i; k++) {
				System.out.print(" ");
			}
			// 控制星星
			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			// 一行结束之后就要换行
			System.out.println();
		}
		
		
		// 3 百钱买百鸡”是我国古代的著名数学题。 
		/*题目这样描述：3文钱可以买1只公鸡， 2文钱可以买一只母鸡，1文钱可以买3只小鸡。
		用100文钱买100只鸡，那么各有公鸡、母鸡、小鸡多少只？
		
		设 x公鸡 y母鸡 z小鸡代表鸡的数量    满足   x+y+z=100只  &&  3x +  2y + 1/3z = 100文钱--->分别*3  9x+6y+z =300
		*/
		//使用3层for循环
		// x公鸡
		for (int x = 0; x <= 100; x++) {
			
			// y母鸡
			for (int y = 0; y <= 100; y++) {
						
				// z小鸡
				for (int z = 0; z <= 100; z++) {
					
					// 只有满足2个方程式才是最终组合答案
					if (x+y+z==100  &&  9*x+6*y+z == 300) {
						System.out.println("公鸡：" +x +  ",母鸡：" + y + ",小鸡：" + z + "只");
					}
				}
			}
		}
		System.out.println("---第2种做法：---");
		// 第二种做法
		for (int x = 0; x <= 33; x++) { // 公鸡
			
			// y母鸡
			for (int y = 0; y <= 50; y++) {
						
				// z小鸡
				for (int z = 0; z <= 100; z++) { // 因为最多只要买100只，
					
					// 只有满足2个方程式才是最终组合答案
					if (x+y+z==100  &&  9*x+6*y+z == 300) {
						System.out.println("公鸡：" +x +  ",母鸡：" + y + ",小鸡：" + z + "只");
					}
					
				}
			}
		}	
		
		// 第4题 水仙花数  所谓水仙花数，是指一个三位数abc,如果满足a*a*a+b*b*b+c*c*c=abc，则abc是水仙花数。
		
		for (int i = 100; i <= 999; i++) {
			
			// i的百位
			int a = i / 100;
			// i的十位
			int b = i / 10 % 10; // i % 100 / 10
			// i的个位
			int c= i % 10;
			
			if (a*a*a+b*b*b+c*c*c == i) {
				System.out.println("水仙花数： " + i);
			}
		}
		
		
		// 第二种做法
		for (int a = 1; a <= 9; a++) {
			for (int b = 0; b <= 9; b++) {
				for (int c = 0; c <= 9; c++) {
					
					if (a*a*a+b*b*b+c*c*c == a*100 + b*10 + c) {
						System.out.println("水仙花数： " + a+""+b+""+c);
					}
				}
			}
		}
		
	}
}
