package part01基础语法;

import java.util.Scanner;

public class 循环_练习2 {

	public static void main(String[] args) {
//		从键盘连续输入一个班5个学生的分数，求和并输出。
		Scanner scanner = new Scanner(System.in);
		int sum = 0;
		for (int i = 0; i < 5; i++) {
			System.out.println("请输入第" + (i+1) +"个学生的成绩：");
			int score = scanner.nextInt();
			sum += score;
		}
		System.out.println("总分： " + sum);
		
		
		
//		计算2008年1月1日到2019年1月1日相距多少天。（注意闰年）
		int days= 0;
		for (int i = 2008; i < 2019; i++) {
			if (i % 4 == 0 && i % 100 !=0  || i % 400 == 0) {
				days += 366;
			} else {
				days += 365;
			}
		}
		System.out.println("2008-2019.1.1相距： " + days +"天");
	}
}
