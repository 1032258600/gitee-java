package part04封装.练习;

public class Book3Test {
	public static void main(String[] args) {
		// 创建类的对象
		Book3 book = new Book3();
		// setter方法赋值
		book.setTitle("西游记");
		book.setPageNum(1000);
		book.setType("四大名著");
		book.detail();
		
		// 构造方法赋值==实例化对象同时赋值
		Book3 book2 =new Book3("红楼梦", 200);
		book2.detail();
		
		Book3 book3 =new Book3("三国演义", 150, "名著");
		book3.detail();
		
	}

}
