package part04封装.练习;

public class Book3 {

	private String title;
	private int pageNum;
	private String type;
	
	public Book3() {
	}
	
	public Book3(String title, int pageNum) {
		this.title = title;
		this.pageNum = pageNum;
		this.type = "计算机";
	}
	// 全参数的构造
	public Book3(String title, int pageNum, String type) {
		this.title = title;
		this.pageNum = pageNum;
		this.type = type;
	}
	
    public void detail() {
	  System.out.println("title: " + title + ",pageNum：" + pageNum + ",type：" + type);
    }
    
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
    
	
}
