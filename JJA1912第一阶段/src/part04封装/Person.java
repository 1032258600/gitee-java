package part04封装;
/**
 * 封装
 * @author Administrator
 *
 */
public class Person {

	// 私有化成员属性
	private String name;
	private int age;
	private String gender;

	// 对外提供公开的setter和getter方法
	public void setName(String name) {
		this.name = name; // 将右侧传进来的参数name赋值给左侧的name【类的成员属性】
	}

	public String getName() {
		return name;
	}

	public void setAge(int age) {
		// 在设置的方法中可以对参数进行拦截判断
		if (age > 100 || age < 0) {
			age = 0;
		}
		this.age = age; // 将右侧传进来的参数name赋值给左侧的name【类的成员属性】
	}

	public int getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}
}
