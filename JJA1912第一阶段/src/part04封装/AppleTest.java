package part04封装;

public class AppleTest {
	
	public static void main(String[] args) {
		// 创建Apple类的对象
		Apple apple = new Apple(); // 通过无参数构造方法创建类的对象
		// 通过setter方法对属性赋值
		apple.setColor("金色");
		apple.setSize(4);
		// 输出内容
		System.out.println(apple.toString());
		
		
		// 创建Apple的对象
		Apple apple2 = new Apple("银色", 5); // 通过有参构造方法创建类的对象同时对color和size属性赋值
		// 输出内容
		System.out.println(apple2.toString());
		
		
		Apple apple3 = new Apple("黑色"); // 通过1个参数的构造方法 创建类的对象 同时对color属性赋值
		apple3.setSize(6);
		System.out.println(apple3.toString());
		
		Apple apple4 = new Apple(7);// 通过1个参数的构造方法 创建类的对象 同时对size属性赋值
		apple4.setColor("白色");
		System.out.println(apple4.toString());
		
	
		
	}
}









