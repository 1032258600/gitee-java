package part04封装;
/**
 * 构造方法
 * @author Administrator
 *
 */
public class Apple {
	
	private String color; // 颜色
	private int size;  // 尺寸

	// 一个类中默认隐藏着一个 无参构造方法(方法体是空的)
	// 但是如果显示定义了有参数的构造方法，那么这个隐藏的无参数构造方法就会被覆盖。
	// 所以如果既要有，有参数构造也要有无参构造，就必须把 无参构造 显式声明 出来。
	// ①方法名和类名一致 ②无返回类型 
	public Apple() {
		
		System.out.println("无参构造");
	}
	
	public Apple(String color, int size) { // 全参构造方法
		super();
		this.color = color;
		this.size = size;
	}

	public Apple(int size) { // 1个参数的构造方法
		super();
		this.size = size;
	}

	public Apple(String color) { // 1个参数的构造方法
		super();
		this.color = color;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	@Override
	public String toString() {
		return "Apple [color=" + color + ", size=" + size + "]";
	}
	
	

	

}
