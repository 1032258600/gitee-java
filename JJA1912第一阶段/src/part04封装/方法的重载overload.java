package part04封装;

public class 方法的重载overload {

	public void show() {
		System.out.println("无参数的show方法");
	}
	
	public void show(int a) {
		System.out.println("1个int参数的show方法");
	}
	
	public void show(String a) {
		System.out.println("1个String参数的show方法");
	}
	
	public void show(String a,String b) {
		System.out.println("2个String参数的show方法");
	}
	
	public void show(String b,int c) {
		System.out.println("1个String 1个int 参数的show方法");
	}
	
	public static void main(String[] args) {
		方法的重载overload f = new 方法的重载overload();
		f.show();
		f.show(1);
		f.show("ABC");
		f.show("ABC","BVC");
		f.show("ABC",1);
		
	}
	
	
}
