package part04封装;

public class 关键字this {
	
	private String name;
	
	public 关键字this(String name) {
		// name = name; // 左侧和右侧的name都是参数name
		// this.属性名表示访问类中的成员属性，用来区分成员变量和局部变量（重名问题） 
		// 左侧this.name的name是成员属性name 
		// 右侧name是参数name
		this.name = name;
	}

	public 关键字this() {
		// this() 调用本类的构造方法，必须放在第一句
		this("伟斌");
		System.out.println("无参构造调用有参构造~");
	}
	
	public void t1() {
		
	}
	public void t2() {
		t1();
		this.t1();// this.成员方法名() 表示用来访问本类的成员方法
	}
	
	@Override
	public String toString() {
		return "关键字this [name=" + name + "]";
	}


	public static void main(String[] args) {
		关键字this t1 = new  关键字this("叶鸿");
		System.out.println(t1.toString());
		
		关键字this t2 = new  关键字this();
		System.out.println(t2.toString());
		
		
				 
	}
}








