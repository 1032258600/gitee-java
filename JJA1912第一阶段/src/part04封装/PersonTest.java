package part04封装;


public class PersonTest {

	public static void main(String[] args) {
		
		// 实例化
		Person person = new Person();
		
		// 属性的赋值
//		person.name = "小白"; // 编译错误，因为name属性被私有化
//		person.age = 76; //  编译错误，因为age属性被私有化
		
		// 通过setter方法对属性进行赋值
		person.setName("小白");
		person.setAge(180);
	
		// 属性的访问
//		System.out.println(person.name + person.age); // 编译错误，因为name属性被私有化
		
		// 通过getter方法对属性进行访问
		System.out.println(person.getName() +  ", " + person.getAge());
		
		System.out.println("---关于年龄属性值赋值问题--");
		Person person2 = new Person();
		int age = -15;
		if (age>100 || age<0) {
			age = 0;
		}
		person2.setAge(age);
		
		System.out.println(person2.getAge());
		
		Person person3 = new Person();
		int age2 = 150;
		if (age2>100 || age2<0) {
			age2 = 0;
		}
		person3.setAge(age2);
		
		System.out.println(person3.getAge());
		System.out.println("---setter解决年龄属性值赋值问题--");
		Person person4 = new Person();
		int age3 = -1500;
		person4.setAge(age3);
		
		System.out.println(person4.getAge());
		
		// 利用toString()方法输出Person类对象的属性信息
		System.out.println(person4);
		System.out.println(person4.toString());
	}
}
