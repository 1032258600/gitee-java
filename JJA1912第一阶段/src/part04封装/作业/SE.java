package part04封装.作业;

public class SE {

	private String id; // 员工编号
	private String name;// 姓名
	private String gender;// 性别
	private float salary;// 工资
	private int hot;// 关注度
	
	
	public SE() {
		super();
		this.id = "001";
		this.name = "张三";
		this.gender = "男";
		this.salary = 5000.00f;
		this.hot = 1;
	}
	
	public SE(String id, String name, String gender, float salary, int hot) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.hot = hot;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public int getHot() {
		return hot;
	}
	public void setHot(int hot) {
		this.hot = hot;
	}
	@Override
	public String toString() {
		return "SE [id=" + id + ", name=" + name + ", gender=" + gender + ", salary=" + salary + ", hot=" + hot + "]";
	}
	
	
}
