package part04封装.作业;

public class PM {

	private String id; // 员工编号
	private String name;// 姓名
	private String gender;// 性别
	private float salary;// 工资
	private String exp;// 项目经验
	private float bonus;// 项目分红

	public PM() {
		super();
		this.id = "001";
		this.name = "黄经理";
		this.gender = "男";
		this.salary = 10000.00f;
		this.exp = "五年工作经验";
		this.bonus = 0.56f;
	}
	public PM(String id, String name, String gender, float salary, String exp, float bonus) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.exp = exp;
		this.bonus = bonus;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}

	public String getExp() {
		return exp;
	}
	public void setExp(String exp) {
		this.exp = exp;
	}
	public float getBonus() {
		return bonus;
	}
	public void setBonus(float bonus) {
		this.bonus = bonus;
	}
	@Override
	public String toString() {
		return "PM [id=" + id + ", name=" + name + ", gender=" + gender + ", salary=" + salary + ", exp=" + exp
				+ ", bonus=" + bonus + "]";
	}
	
	
}
