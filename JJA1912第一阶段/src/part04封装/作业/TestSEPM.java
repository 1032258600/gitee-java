package part04封装.作业;

public class TestSEPM {

	public static void main(String[] args) {
		// 创建程序员 1
		SE se1 = new SE();
		System.out.println(se1);
		se1.setId("201901");
		se1.setName("叶鸿");
		System.out.println(se1);
		// 创建程序员2
		SE se2 = new SE("201902", "wb", "男", 564282, 56);
		System.out.println(se2);
		
		// 创建经理
		PM pm1 = new PM();
		System.out.println(pm1);
		
		PM pm2 = new PM("201902", "陈经理", "女", 564282.56f,"10年", 0.85f);
		System.out.println(pm2);
	}
}
