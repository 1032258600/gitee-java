package part04封装.作业;
/**
 * 计算工资类
 * @author Administrator
 *
 */
public class SalaryCalc {

	/**
	 * 计算程序员工资
	 */
	public float pay(SE se) { // SE se = new SE("001,5000)
		return se.getSalary()+50; // 每个人的工资上再加50元
	}
	
	/**
	 * 计算项目经理工资
	 */
	public float pay(PM pm) {
		return  pm.getSalary() * (1 + pm.getBonus()); // 每个人的工资乘以分红
	}
}
