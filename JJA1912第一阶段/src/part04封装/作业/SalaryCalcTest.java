package part04封装.作业;

public class SalaryCalcTest {

	public static void main(String[] args) {

		// 计算工资的类
		SalaryCalc sc = new SalaryCalc();
		
		// 创建一个程序员对象
		SE se = new SE("001", "叶鸿", "男", 10000, 10);
		float salary = sc.pay(se);
		System.out.println(se.getName() + "工资： " + salary);
		
		// 创建一个程序员对象
		SE se2 = new SE("001", "BABY", "女", 5500, 10);
		salary = sc.pay(se2);
		System.out.println(se2.getName() + "工资： " + salary);

		// 创建一个项目经理对象
		PM pm = new PM("201902", "陈经理", "女", 50000,"10年", 0.85f);
		salary = sc.pay(pm);
		System.out.println(pm.getName() + " 工资：" + salary);
	}
}
