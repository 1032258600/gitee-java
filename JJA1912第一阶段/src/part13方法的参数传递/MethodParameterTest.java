package part13方法的参数传递;

public class MethodParameterTest {

	public static void main(String[] args) {
		MethodParameter mp = new MethodParameter();
		System.out.println("---基本数据类型作为参数传递 值传递----");
		int a = 10;
		mp.changeInt(a); //将a的值拷贝一份给方法changeInt()的形参，值传递
		System.out.println("main:a:" + a); //10 
		System.out.println();
		
		System.out.println("---数组作为参数传递 引用传递----");
		int[] array = {1,2,3}; // array引用名在栈中  {1,2,3}真正的值在堆中。
		mp.changeArray(array);// 将array引用名[堆的地址]传递给方法changeArray的形参array，和当前array，两个引用名其实指向的是堆中的同一个对象。
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("main：数组元素：" + array[i]);// 2 4 6 
		}
		System.out.println();
		
		
		System.out.println("---类作为参数传递 引用传递----");
		Dog dog = new Dog();
		dog.setDogName("哈士奇");
		mp.changeObject(dog);// 将dog引用名[堆的地址]传递给方法changeObject的形参dog，和当前dog，两个引用名指向的是堆中的同一个对象。
		System.out.println("main： 狗信息：" + dog.getDogName());// 哈巴狗
		System.out.println();
		
		
		System.out.println("---String作为参数传递 值传递----");
		String str = "Hello";
		mp.changeString(str);
		System.out.println("main:str:" + str);// hello
		
		
		System.out.println("---可变参数(数组) String 作为参数传递---");
		int num = 1;
		String s0 = "A";
		String s1 ="a"; 
		String s2 ="b"; 
		String s3 ="c"; 
//		mp.changeParams(num, s0,s1);
//		mp.changeParams(num, s0,s1,s2);
		mp.changeParams(num, s0,s1,s2,s3);
		System.out.println("main:num:" + num);// 1
		System.out.println("main:s0:" + s0);// A
		System.out.println("main:s1:" + s1);// a
		System.out.println("main:s2:" + s2);// b
		System.out.println("main:s3:" + s3);// c
		
		System.out.println("---可变参数(数组) 类 作为参数传递 ---");
		Dog dog2 = new Dog();
		Dog dog3 = new Dog();
		dog2.setDogName("小白");
		dog3.setDogName("大白");
		mp.changeParams(dog2,dog3);
		System.out.println("main:dog2:" + dog2.getDogName());// 小黑
		System.out.println("main:dog2:" + dog3.getDogName());// 小黑
	}
}















