package part13方法的参数传递;

public class 可变参数 {

	public static  void calc1(int[] array) {
		// 传递进来的元素累加
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		System.out.println(sum);
	}
	
	public static  void calc2(int a,int b) {
		// 传递进来的元素
	    System.out.println(a + "," + b);
	}
	public static  void calc3(int a,int b,int c) {
		// 传递进来的元素
		   System.out.println(a + "," + b +"," + c);
	}
	/**
	 * int... params 
	 * ...表示可变参数  本质是一个数组  可以传递0到N个参数
	 * 一个方法只能有一个可变参数,只能定义在参数列表的最后
	 * @param params
	 */
	public static  void calc4(int num,int... params) {
		System.out.println("固定参数num：" + num);
		System.out.print("可变参数：");
		// 传递进来的元素
		for (int i = 0; i < params.length; i++) {
			System.out.print(params[i] +",");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		int[] array = {1,2,3};
		可变参数.calc1(array);
		可变参数.calc2(10,20);
		可变参数.calc3(10,20,30);
		// 可变参数，可以传递不同个数的参数
		可变参数.calc4(150);
		可变参数.calc4(100);
		可变参数.calc4(100,300);
		可变参数.calc4(100,300,500,700,600);
	}
}
