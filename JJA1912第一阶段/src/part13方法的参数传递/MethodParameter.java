package part13方法的参数传递;
/**
 * 方法的参数传递
 * @author Administrator
 *
 */
public class MethodParameter {

	/**
	 * @param a int类型 基本数据类型 【值传递】
	 */
	public void changeInt(int a) { // 形参
		// 改变
		a = a + 2;
		System.out.println("changeInt: a ："  + a );//12
	}
	/**
	 * @param array 数组 引用数据类型【引用传递】
	 */
	public void changeArray(int[] array) {
		// 改变数组的内容 将数组中的每个元素都*2
		for (int i = 0; i < array.length; i++) {
			array[i] = array[i] * 2;
		}
		// 再一次输出数组中的元素：
		for (int i = 0; i < array.length; i++) {
			System.out.println("changeArray：数组元素：" + array[i] + "  ");// 2 4 6
		}

	}
	/**
	 * @param dog 自定义Dog类 引用数据类型 【引用传递】
	 */
	public void changeObject(Dog dog) {
//		dog = new Dog(); // 如果重新new，就是新的一个对象。
		// 改变dog的属性
		dog.setDogName("哈巴狗");
		System.out.println("changeObject 狗信息：" + dog.getDogName());// 哈巴狗
	}
	
	/**
	 * @param str 字符串 引用数据类型 【值传递】
	 * String字符串不可变的原因，导致如果对字符串重新赋值，在内存中就会重新开辟一个对象
	 */
	public void changeString(String str) {
		str = "world"; // 对str重新赋值，会重新开辟一个对象
		System.out.println("changeString：str:" + str);// world
	}
	/**
	 * 
	 * @param num int基本数据类型【值传递】
	 * @param str String引用数据类型【值传递】
	 * @param params String可变参数(数组)【值传递】
	 */
	public void changeParams(int num, String str, String... params) {
		
		num = 10;
		System.out.println("changeParams：int的num：" + num); // 10
		str = "B";
		System.out.println("changeParams：String的str：" + str); //B
		
		// 改变可变参数(数组)内容
		for (int i = 0; i < params.length; i++) {
			params[i] = params[i] + "@";
		}
		// 输出
		for (int i = 0; i < params.length; i++) {
			System.out.println("changeParams:" +params[i] );// a@ b@ c@
		}
	}
	/**	 * 
	 * @param params  Dog... params 可变参数，引用类型类 
	 */
	public void changeParams(Dog... params) {
		
		for (int i = 0; i < params.length; i++) {
			params[i].setDogName("小黑");
		}
		// 输出
		for (int i = 0; i < params.length; i++) {
			System.out.println("changeParams:" +params[i].getDogName());// 小黑
		}
	}
}







