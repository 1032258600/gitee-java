package part22网络编程.群聊;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * 客户端 群聊 一旦连接上服务器，就应该接收服务器的消息
 * 
 * @author Administrator
 *
 */
public class Client3 {
	public static void main(String[] args) {
		Client3 client1 = new Client3();
		client1.testClient();
	}

	public void testClient() {
		Socket client = null;
		DataInputStream dis = null; // 数据输入流
		DataOutputStream dos = null; // 数据输出流
		Scanner scanner = new Scanner(System.in);

		try {
			// 1.调用Socket类创建一个套接字，并连接到服务器
			client = new Socket("127.0.0.1", 6666);
			System.out.println("连接上服务器");

			// 群聊提升1：一旦客户端连接上服务器，就需要读取服务器消息 启动读服务器消息的线程
			new ReadServerMessageThread(client).start();

			
			// 2.调用Socket类的getOutputStream()和getInputStream获取输出流和输入流，开始网络数据的发送和接收。
			dos = new DataOutputStream(client.getOutputStream());
			// 不停的写
			while (true) {
				System.out.println("客户端输入要发送的消息：");
				String sendMessage = scanner.nextLine();
				if ("exit".equals(sendMessage)) {
					// 退出聊天
					break;
				}
				// 发消息给服务器
				dos.writeUTF(sendMessage);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				// 最后关闭通信套接字。
				if (dos != null) {
					dos.close();
				}
				if (dis != null) {
					dis.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

/**
 * 客户端专门读取服务器消息的线程类
 * 
 * @author Administrator
 *
 */
class ReadServerMessageThread extends Thread {
	private Socket client;

	public ReadServerMessageThread(Socket client) {
		this.client = client;
	}

	@Override
	public void run() {
		DataInputStream dis = null;
		// 不断读取服务器
		while (true) {
			try {
				dis = new DataInputStream(client.getInputStream());
				// 接收服务器发来的消息
				String content = dis.readUTF();
				System.out.println("服务器：" + content); // 读取服务器信息
			} catch (IOException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				// 当客户端关闭之后，应该处理异常信息
				System.out.println("服务器关闭或者客户端下线，停止读取消息");
				break;// 退出循环
			}
		}
	}
}
