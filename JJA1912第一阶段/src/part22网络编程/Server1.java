package part22网络编程;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * 服务端
 * 
 * @author Administrator
 *
 */
public class Server1 {

	public static void main(String[] args) {
		Server1 server1 = new Server1();
		server1.testServer();
	}

	public void testServer() {

		ServerSocket server = null; // 服务端套接字
		Socket client = null;       // 客户端 通信套接字
		DataInputStream dis = null; // 数据输入流
		DataOutputStream dos = null; // 数据输出流
		Scanner scanner = new Scanner(System.in);
		try {
			// 调用ServerSocket(int port)创建一个服务器端套接字，并绑定到指定端口上；
			server = new ServerSocket(6666);
			System.out.println("服务器启动成功");

			// 调用accept()，监听连接请求，如果客户端请求连接，则接受连接，返回通信套接字。
			client = server.accept();
			System.out.println(client.getRemoteSocketAddress() + "已连接上~");

			// 调用Socket类的getOutputStream()和getInputStream获取输出流和输入流，开始网络数据的发送和接收。
			dis = new DataInputStream(client.getInputStream());
			dos = new DataOutputStream(client.getOutputStream());
			
			// 不停的读和写
			while (true) {
				// 读：读取客户端的消息
				String content = dis.readUTF();
				System.out.println(client.getRemoteSocketAddress() + " : " + content);
				
				// 写: 发送消息给客户端
				System.out.println("服务器输入要发送的消息：" );
				String sendMessage = scanner.nextLine();
				dos.writeUTF(sendMessage);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			// 异常处理：当客户端下线，提醒下线信息
			System.out.println(client.getRemoteSocketAddress()  +" 已下线!");
		} finally {
			try {
				// 最后关闭通信套接字。
				if (dos != null) {
					dos.close();
				}
				if (dis != null) {
					dis.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
