package part22网络编程.一对多;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * 客户端
 * 
 * @author Administrator
 *
 */
public class Client2 {
	public static void main(String[] args) {
		Client2 client1 = new Client2();
		client1.testClient();
	}

	public void testClient() {
		Socket client = null;
		DataInputStream dis = null; // 数据输入流
		DataOutputStream dos = null; // 数据输出流
		Scanner scanner = new Scanner(System.in);

		try {
			// 1.调用Socket类创建一个套接字，并连接到服务器
			client = new Socket("127.0.0.1", 6666);
			// 2.调用Socket类的getOutputStream()和getInputStream获取输出流和输入流，开始网络数据的发送和接收。
			dos = new DataOutputStream(client.getOutputStream());
			dis = new DataInputStream(client.getInputStream());
			
			// 不停的读和不停的写
			while (true) {
				System.out.println("客户端输入要发送的消息：" );
				String sendMessage = scanner.nextLine();
				if ("exit".equals(sendMessage)) {
					// 退出聊天
					break;
				}
				// 发消息给服务器
				dos.writeUTF(sendMessage);
				
				// 接收服务器发来的消息
				String content = dis.readUTF();
				System.out.println("服务器：" + content);
			}
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				// 最后关闭通信套接字。
				if (dos != null) {
					dos.close();
				}
				if (dis != null) {
					dis.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
