package part10访问修饰符;

import part09访问修饰符.Test1;

public class Test3 {
	
	public static void main(String[] args) {
		
		// 不同包下的访问修饰符测试
		Test1 test1 = new Test1();
		test1.a = 10; // 可以调用public修饰的属性
		test1.publicMethod();// 可以调用public修饰的方法
		
		// 不同包下，不能访问 protected、缺省、private修饰的属性和方法
	}

}


