package part10访问修饰符;

import part09访问修饰符.Test1;

public class Test4 extends Test1{

	public static void main(String[] args) {
		// 不同包下，不能访问 protected、缺省、private修饰的属性和方法
		Test1 test1 = new Test1();
		test1.a = 10; // 可以调用public修饰的属性
		test1.publicMethod();// 可以调用public修饰的方法
		
		// 不同包下，子类可以访问父类的public和protect修饰的属性和方法
		Test4 test4 = new Test4();
		test4.a = 100; // 可以调用public修饰的属性
		test4.publicMethod();// 可以调用public修饰的方法
		test4.d=100;// 可以调用父类protect修饰的方法
		test4.protectMethod();// 可以调用父类protect修饰的方法
	}
}
