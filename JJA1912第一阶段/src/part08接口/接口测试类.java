package part08接口;

public class 接口测试类 {

	public static void main(String[] args) {
		
//		InterfaceA iA = new InterfaceA();// 编译错误  接口不能被实例化
			
		// 接口不能被实例化，能不能被继承??? 可以被接口继承
		// 接口不能被实例化, 只能被实现
			
	    // 接口的引用 指向 实现类
		InterfaceA interfaceA  = new InterfaceAImpl();
		interfaceA.t1();
		
		// 接口的引用 指向 实现类
		InterfaceA interfaceA2 = new InterfaceABImpl();
		interfaceA2.t1();
		interfaceA2.t5();
		
		// 访问接口中的静态方法
		InterfaceA.t4();
	}
}
