package part08接口;
/**
 * 接口
 * 使用interface定义的类形成了接口
①接口使用interface关键字修饰。
②接口中只能有静态常量，默认使用static final。
③接口、接口的常量、接口中的方法只能使用public、缺省访问修饰符
④接口中可以有抽象方法、static静态方法、default默认方法。在jdk1.7之前接口中只能有抽象方法，jdk1.8之后可以有静态和默认方法。
⑤接口不能实例化，只能被实现。通过implements 关键字
⑥一个类可以实现多个接口，必须重写接口中的抽象方法，解决Java单继承的问题。如果不重写，该类就需要定义为一个抽象类。
⑦接口可以有多个实现类。
⑧接口中没有构造方法。
⑨接口可以被接口继承。
 * @author Administrator
 */
public interface InterfaceA {
	
	// 静态常量  默认使用static final修饰，只能使用public或者缺省作为访问修饰符
	int num = 10;
	static final int num2 = 10;
	public int num4 = 100;
	
//	public InterfaceA() {// 接口中没有构造方法
//		
//	}
	/**
	 * jdk1.7之前的接口中只能有抽象方法。
	 * 接口中可以有抽象方法，默认使用abstract关键字修饰的方法
	 * 只能使用public或者缺省作为访问修饰符
	 */ 
	void t3();
	public void t1();
	public abstract void t2();
	
	/**
	 * jdk1.8+新增的特性
	 * 接口中可以有静态方法，使用static关键字修饰的方法
	 * 只能使用public或者缺省作为访问修饰符
	 * 注意：实现接口的类或者子接口不会继承接口中的静态方法
	 */
	public static void t4() {
		System.out.println("InterfaceA的静态方法t4");
	}

	/**
	 * jdk1.8+新增的特性
	 * 接口中可以有default方法，使用default关键字修饰的方法
	 * 只能使用public或者缺省作为访问修饰符
	 */
	public default void t5() {
		System.out.println("InterfaceA的default方法t5");
	}
}
