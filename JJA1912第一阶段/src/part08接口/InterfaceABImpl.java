package part08接口;
/**
 * 一个类可以实现多个接口，解决Java单继承的问题。
 *  必须重写所有接口中的所有抽象方法，如果不重写，该类就需要定义为一个抽象类。
 *  
 *  实现接口的类如果显式重写接口中的default方法，不需要再使用default修饰
 *  注意：实现接口的类或者子接口不会继承接口中的静态方法
 * @author Administrator
 *
 */
public class InterfaceABImpl implements InterfaceA,InterfaceB{

	@Override
	public void show() {
		// TODO Auto-generated method stub
		System.out.println("实现类中重写show");
	}

	@Override
	public void t3() {
		// TODO Auto-generated method stub
		System.out.println("实现类中重写t3");
	}

	@Override
	public void t1() {
		System.out.println("实现类中重写t1");
	}

	@Override
	public void t2() {
		// TODO Auto-generated method stub
		System.out.println("实现类中重写t2");
	}
	
    /**
     * 显式 重写接口中的default方法，
     * 不需要使用default修饰
     */
	@Override
	public void t5() {
		// 如果多个接口中含有相同的default方法，可以显式声明调用 接口名.super.默认方法()
		InterfaceA.super.t5();
		InterfaceB.super.t5();
		System.out.println("实现类中显示重写default方法");
	}

	
}
