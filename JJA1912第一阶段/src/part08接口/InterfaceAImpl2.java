package part08接口;
/**
 * 一个接口可以被多个类实现
 * @author Administrator
 *
 */
public class InterfaceAImpl2 implements InterfaceA{

	@Override
	public void t3() {
		// TODO Auto-generated method stub
		System.out.println("重写t3");
	}

	@Override
	public void t1() {
		// TODO Auto-generated method stub
		System.out.println("重写t1");
	}

	@Override
	public void t2() {
		// TODO Auto-generated method stub
		System.out.println("重写t2");
	}

}
