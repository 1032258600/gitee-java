package part08接口;
/**
 * 一个类实现单一接口
 * 必须重写接口中的抽象方法，如果不重写，该类就需要定义为一个抽象类。
 * @author Administrator
 *
 */
public class InterfaceAImpl implements InterfaceA{

	@Override
	public void t3() {
		// TODO Auto-generated method stub
		System.out.println("重写t3");
	}

	@Override
	public void t1() {
		// TODO Auto-generated method stub
		System.out.println("重写t1");
	}

	@Override
	public void t2() {
		// TODO Auto-generated method stub
		System.out.println("重写t2");
	}

}
