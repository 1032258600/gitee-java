package part08接口;
/**
 * 接口B
 * @author Administrator
 *
 */
public interface InterfaceB {

	void show();

	/**
	 * jdk1.8+新增的特性
	 * 接口中可以有default方法，使用default关键字修饰的方法
	 * public或者缺省作为访问修饰符
	 */
	public default void t5() {
		System.out.println("InterfaceB的default方法t5");
	}
	
}
