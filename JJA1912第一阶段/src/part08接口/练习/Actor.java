package part08接口.练习;

public interface Actor {
	
	// 常量
	public int role = 1;
	
	// 抽象
	public String show(String op);
	
	// 静态方法
	
	// 默认方法
	
	// 此时我们想要在演员这个接口中，新增一个功能，如果使用抽象方法，则会导致所有的实现类都需要修改。
	// 使用默认方法，不仅可以新增功能，而且便于维护
	public default String show2(String op) {
		return "默认方法";
	}

}
