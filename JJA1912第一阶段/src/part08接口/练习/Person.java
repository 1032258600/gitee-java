package part08接口.练习;
/**
 * 接口的实现类
 * @author Administrator
 *
 */
public class Person implements Actor,Singer{
	private String pname;
	

	public Person(String pname) {
		super();// 调用父类object
		this.pname = pname;
	}
	public Person() {
		super();
	}
	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	@Override
	public String singsong(String song) {
		// TODO Auto-generated method stub
		return this.pname + "唱： " + song +  "，角色编号：" + Singer.role;
	}

	@Override
	public String show(String op) {
		// TODO Auto-generated method stub
		
		return this.pname + "表演：" + op+  "，角色编号：" + Actor.role;
	}
	
	public static void main(String[] args) {
		
		// 创建person
		System.out.println(new Person("彭于晏").show("喷火龙"));
		System.out.println(new Person("彭于晏").singsong("天路"));
		
		// 创建第3个person
		Person person = new Person("晨宇");
		System.out.println(person.show("上刀山"));
		System.out.println(person.singsong("双截棍"));
		System.out.println(person.show2("杂技"));
		
	}

}
