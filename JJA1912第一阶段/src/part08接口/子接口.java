package part08接口;
/**
 * 接口可以继承接口，
 * 但是普通类不能继承接口
 * 
 * 注意：实现接口的类或者子接口不会继承接口中的静态方法
 * @author Administrator
 *
 */
public interface 子接口 extends InterfaceA{

}

/*class 子类2 extends InterfaceA{ // 编译错误， 普通类不能继承接口

}*/
