package part02数组;

import java.util.Scanner;

public class 数组_练习 {
	
	public static void main(String[] args) {
//		① 从控制台中接收5个学生成绩，放在数组中，输出总分，平均分，最高分，最低分。
		Scanner scanner = new Scanner(System.in);
		int scores[] = new int[5];
		int sum = 0;
		// 使用循环为数组元素赋值
		for (int i = 0; i < scores.length; i++) {
			
			System.out.println("输入第 " + (i+1) +"个学生成绩：");
			scores[i] = scanner.nextInt(); // 数组元素赋值
		    sum += 	scores[i];// 分数累加
		}
		System.out.println("总分：" +sum );
		System.out.println("平均分：" + (float)sum / scores.length );
		
		// 最高分最低分
		// 假设：数组中第一个元素就是最大值，也是最小值
		int max = scores[0];
		int min = scores[0];
		// 遍历数组的所有元素，和最大值、最小值进行对比
		for (int i = 0; i < scores.length; i++) {
			
			if (scores[i] > max) {
				max = scores[i]; // 交换
			}
			if (scores[i] < min) {
				min = scores[i]; // 交换
			}
		}
		System.out.println("最高分：" + max );
		System.out.println("最低分：" + min);
		
//		② 查询特定某个成绩在数组中存储的位置【比如我输入分数50 60 78 79 88】，我要查询78出现在哪一个位置。获取元素对应的索引值。
		System.out.println("请输入要查询的分数：");
		int score = scanner.nextInt(); // 数组元素赋值
		boolean flag = false; // 表示无此分数
		for (int i = 0; i < scores.length; i++) {
			
			if (scores[i] == score) {
				flag = true;
				System.out.println(score + "分数的索引值：" + i);
			}
		}
		// 判断标杆
		if (flag==false) {
			System.out.println("无此分数");
		}
		
	}

}
