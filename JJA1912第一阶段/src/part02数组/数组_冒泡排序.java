package part02数组;

public class 数组_冒泡排序 {

	public static void main(String[] args) {
		
		int array[] = {-1,-10,2,29,87,1,78};
		// 通过冒泡排序，将数组的元素从大到小排序并输出
//		代码实现：通过两个for循环，外层控制轮数（5个数，需要4轮）。
//		内层循环控制比较次数（第一轮比较4次，第二轮比较3次，依次减1），然后两两进行比较，进行位置交换。
		System.out.println("从大到小输出");
		for (int i = 0; i < array.length-1; i++) { // 控制轮数 5个数4轮  7个数6轮 其实就是数组元素个数-1
			
			for (int j = 0; j < array.length-1-i; j++) { // 7个数6轮如果第一轮需要比较6次， 第二轮必须比较5次，第三轮必须比较4次
				
				// 两两比较，从大到小
				if (array[j] < array[j+1]) {
					
					 // 交换位置
					int temp =  array[j+1];
					array[j+1] = array[j];
					array[j] = temp;
				}
			}
		}
		
		// 输出
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		System.out.println("从小到大输出");
		// 从小到大
		for (int i = 0; i < array.length-1; i++) { // 控制轮数 5个数4轮  7个数6轮 其实就是数组元素个数-1
			
			for (int j = 0; j < array.length-1-i; j++) { // 7个数6轮如果第一轮需要比较6次， 第二轮必须比较5次，第三轮必须比较4次
				
				// 两两比较，从小到大
				if (array[j] > array[j+1]) {
					
					 // 交换位置
					int temp =  array[j+1];
					array[j+1] = array[j];
					array[j] = temp;
				}
			}
		}
		
		// 输出
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		
	}
}
