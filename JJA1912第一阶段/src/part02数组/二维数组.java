package part02数组;

public class 二维数组 {

	public static void main(String[] args) {
		
//		数据类型 数组引用名[][] = new 数据类型[行数][列数]
		
		// 定义一个二维数组
		int array[][] = new int[3][4]; // 内存中开辟一个连续的空间，这个二维数组由3行4列构成
		
		// 赋值?
		array[0][0] = 10;
		array[2][2] = 50;
		array[2][3] = 50;	
		
		// 访问
		System.out.println(array[0][0]);//10
		System.out.println(array[0][1]);// 没有赋值，默认为0
		
		System.out.println(array.length); // 行数
		// 遍历二维数组
		for (int i = 0; i < 3; i++) {
			
			for (int j = 0; j < 4; j++) {
				
				System.out.println(array[i][j]);
			}
		}
	}
}
