package part02数组;

public class 二维数组_杨辉三角 {

	public static void main(String[] args) {
		
		// 二维数组
		int[][] yh = new int[10][10];
		
		for (int i = 0; i < yh.length; i++) {  // 控制行数

			for (int j = 0; j <= i; j++) { // 控制列
				
				// 打印数字1
				if (j==0 || j==i) {
					yh[i][j] = 1;
				}else {
					// 当前位置的值 = 该位置的上一行的前2个数相加
					yh[i][j] = yh[i-1][j-1] + yh[i-1][j];
				}
				
				System.out.print(yh[i][j] +"\t");
			}
			// 换行
			System.out.println();
		}
	}
}
