package part02数组;

public class 数组 {
	
	public static void main(String[] args) {
		
		// 定义一个变量?? 数据类型 变量名 = 初始值
		int n = 10;
		
		// 1、定义一个数组 
		// 方式1 数据类型 数组引用名[] = new 数据类型[长度]
		
		int array[] = new int[10]; // 开辟一个连续的空间，长度为10
		
		// 方式2  数据类型[]  数组引用名 = new 数据类型[长度]
		int[] array2 = new int[10]; // 开辟一个连续的空间，长度为10
		
		// 方式3  声明同时赋值
		int array3[] = {0,1,10,80};
		
		
		// 2、 如何为数组赋值   数组引用名[下标] = 值  下标从0开始到长度-1
		array[0] = 15; // 将15赋值给数组中的第一个元素位置 
		array[1] = 20; //
		array[2] = 30; //
		array[3] = 40; //
		array[4] = 50; //
		array[9] = 90; // 数组下标（索引值）最大值是长度-1
//		array[10] = 1000; // 运行错误，会报java.lang.ArrayIndexOutOfBoundsException: 10 数组索引值越界
		
		// 3、如何访问数组
		System.out.println("访问数组：" + array[0]); // 15 
		System.out.println("访问数组：" + array[1]); // 20 
		System.out.println("访问数组：" + array[9]); // 90
//		System.out.println("访问数组第一个位置：" + array[10]); // 运行错误，同理 数组索引值越界
		
		// 4 求数组的长度
		System.out.println("访问数组长度：" + array.length); // 10
		
		// 5 如何遍历数组  使用for循环输出数组的内容
		for (int i = 0; i < array.length; i++) {
			// 输出
			System.out.println("遍历数组内容：" +array[i] );
		}
		
		
		
		
		
		
		
		
	}

}
