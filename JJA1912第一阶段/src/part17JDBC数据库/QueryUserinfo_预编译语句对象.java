package part17JDBC数据库;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class QueryUserinfo_预编译语句对象 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// ①导入数据库驱动jar包,加载驱动类
		Class.forName("com.mysql.jdbc.Driver");

		// ②创建连接对象，建立连接  url-->jdbc:mysql://数据库服务器ip地址:3306/数据库名
		Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jja1912_1", "root", "root");
		System.out.println("连接数据库成功");

		// ③创建预编译语句对象,执行sql
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入Id：");
		String id =  scanner.nextLine();

		// prepareStatement(String sql) 预编译语句对象  1 预编译的功能 2支持占位符
	    PreparedStatement pstat = conn.prepareStatement("select * from userinfo where user_id = ?");
	    // 对占位符赋值
	    pstat.setString(1, id); // 第1个参数是占位符的索引从1开始  第2个参数是参数值
		// 执行SQL
	    ResultSet resultSet = pstat.executeQuery();
		
		// ④遍历结果集(查询才有的)，如果是更新操作，就没有该步骤
		while (resultSet.next()) { // next方法判断结果集中是否有数据
			// 获取每一行的记录 resultSet.getXxx(数据库列名) Xxxx表示是数据类型 
			int userId = resultSet.getInt("user_id"); // user_id 参数是数据库表中的列名
			String username = resultSet.getString("username");
			String password = resultSet.getString("password");
			int age = resultSet.getInt("age");
			System.out.println("userId:" + userId + " username:" +username + "  password:" +password +  " age:" + age);
		}
        // ⑤关闭数据库：先创建的对象后关闭。
		if (resultSet != null) {
			resultSet.close();
		}
		if (pstat != null) {
			pstat.close();
		}
		if (conn != null) {
			conn.close();
		}
		
	}
}
