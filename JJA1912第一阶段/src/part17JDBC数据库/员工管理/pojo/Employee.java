package part17JDBC数据库.员工管理.pojo;
/**
 * 实体类
 * @author Administrator
 *
 */
public class Employee {
	
	private Integer empNo; // 员工编号
	private String empName;// 员工姓名
	private Float salary; // 薪资
	
	public Integer getEmpNo() {
		return empNo;
	}
	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Float getSalary() {
		return salary;
	}
	public void setSalary(Float salary) {
		this.salary = salary;
	}
	public Employee() {
		super();
	}
	public Employee(Integer empNo, String empName, Float salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
	}
	public Employee(String empName, Float salary) {
		super();
		this.empName = empName;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", empName=" + empName + ", salary=" + salary + "]";
	}
	
	
	
}
