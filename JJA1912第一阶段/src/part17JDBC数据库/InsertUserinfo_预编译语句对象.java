package part17JDBC数据库;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUserinfo_预编译语句对象 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// ①导入数据库驱动jar包,加载驱动类
		Class.forName("com.mysql.jdbc.Driver");

		// ②创建连接对象，建立连接  url-->jdbc:mysql://数据库服务器ip地址:3306/数据库名
		Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jja1912_1", "root", "root");
		System.out.println("连接数据库成功");

		// ③创建预编译语句对象,执行sql
		String username = "hhhhh";
		String password = "123";
		int age = 58;
		// 使用占位符
		PreparedStatement pstat = conn.prepareStatement("insert into userinfo values(null,?,?,?)");
		// 对占位符赋值
		pstat.setString(1, username);
		pstat.setString(2, password);
		pstat.setInt(3, age);
		// 执行SQL
		int n = pstat.executeUpdate();
	
		// 判断是否添加成功
		System.out.println(n>0 ? "添加成功" : "添加失败");
        // ⑤关闭数据库：先创建的对象后关闭。
		if (pstat != null) {
			pstat.close();
		}
		if (conn != null) {
			conn.close();
		}
		
	}
}
