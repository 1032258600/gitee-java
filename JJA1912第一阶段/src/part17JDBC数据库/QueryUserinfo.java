package part17JDBC数据库;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class QueryUserinfo {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// ①导入数据库驱动jar包,加载驱动类
		Class.forName("com.mysql.jdbc.Driver");

		// ②创建连接对象，建立连接  url-->jdbc:mysql://数据库服务器ip地址:3306/数据库名
		Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jja1912_1", "root", "root");
		System.out.println("连接数据库成功");

		// ③创建语句对象,执行sql
		Statement stat = conn.createStatement();
		// executeQuery(String sql) 返回值结果集对象
		Scanner scanner = new Scanner(System.in);
		String id =  scanner.nextLine();
		
		ResultSet resultSet = stat.executeQuery("select * from userinfo where user_id = " + id);

		// ④遍历结果集(查询才有的)，如果是更新操作，就没有该步骤
		while (resultSet.next()) { // next方法判断结果集中是否有数据
			// 获取每一行的记录 resultSet.getXxx(数据库列名) Xxxx表示是数据类型 
			int userId = resultSet.getInt("user_id"); // user_id 参数是数据库表中的列名
			String username = resultSet.getString("username");
			String password = resultSet.getString("password");
			int age = resultSet.getInt("age");
			System.out.println("userId:" + userId + " username:" +username + "  password:" +password +  " age:" + age);
		}
        // ⑤关闭数据库：先创建的对象后关闭。
		if (resultSet != null) {
			resultSet.close();
		}
		if (stat != null) {
			stat.close();
		}
		if (conn != null) {
			conn.close();
		}
		
	}
}
