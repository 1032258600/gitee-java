package part17JDBC数据库;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUserinfo {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// ①导入数据库驱动jar包,加载驱动类
		Class.forName("com.mysql.jdbc.Driver");

		// ②创建连接对象，建立连接  url-->jdbc:mysql://数据库服务器ip地址:3306/数据库名
		Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/jja1912_1", "root", "root");
		System.out.println("连接数据库成功");

		// ③创建语句对象,执行sql
		Statement stat = conn.createStatement();
		// 更新操作--添加  executeUpdate(String sql) 返回值是影响行数int类型
//		int n = stat.executeUpdate("insert into userinfo values(null,'毅杰','123456',25)");
		
		String username = "XX";
		String password = "113";
		int age = 18;
		int n = stat.executeUpdate("insert into userinfo values(null,'"+username+"','"+password+"',"+age+")");
		
		// 判断是否添加成功
		System.out.println(n>0 ? "添加成功" : "添加失败");
        // ⑤关闭数据库：先创建的对象后关闭。
		if (stat != null) {
			stat.close();
		}
		if (conn != null) {
			conn.close();
		}
		
	}
}
