package part23swingdemo.com.etc.service;

import java.util.List;

import part23swingdemo.com.etc.pojo.Userinfo;

public interface UserinfoService {

	/**
	 * 登录业务
	 * @param username 用户名 
	 * @param password 密码
	 * @return 用户对象信息
	 */
	public Userinfo login(String username, String password);
	
	/**
	 * 获取所有用户列表业务
	 * @return
	 */
	public List<Userinfo> getAllUsers();
}
