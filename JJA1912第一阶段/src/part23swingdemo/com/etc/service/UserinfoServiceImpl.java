package part23swingdemo.com.etc.service;

import java.util.List;

import part23swingdemo.com.etc.dao.UserinfoDao;
import part23swingdemo.com.etc.dao.UserinfoDaoImpl;
import part23swingdemo.com.etc.pojo.Userinfo;

public class UserinfoServiceImpl implements UserinfoService{

	private UserinfoDao dao = new UserinfoDaoImpl();
	
	@Override
	public Userinfo login(String username, String password) {
		
		return dao.queryByUsernameAndPassword(username, password);
	}

	@Override
	public List<Userinfo> getAllUsers() {
		return dao.query();
	}

}
