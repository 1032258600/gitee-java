package part23swingdemo.com.etc.dao;

import java.util.List;

import part23swingdemo.com.etc.pojo.Userinfo;

public interface UserinfoDao {

	/**
	 * 根据用户名和密码进行查询
	 * @param username 用户名
	 * @param password 密码
	 * @return 
	 */
	public Userinfo queryByUsernameAndPassword(String username, String password);
	
	/**
	 * 查询所有用户表
	 * @return
	 */
	public List<Userinfo> query();
}
