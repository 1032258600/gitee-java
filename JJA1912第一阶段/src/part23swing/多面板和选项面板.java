package part23swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;

public class 多面板和选项面板 {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					多面板和选项面板 window = new 多面板和选项面板();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public 多面板和选项面板() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(28, 10, 366, 50);
		frame.getContentPane().add(panel);
		
		JLabel lblpanel = new JLabel("第一个面板panel");
		panel.add(lblpanel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(28, 83, 366, 50);
		frame.getContentPane().add(panel_1);
		
		JLabel lblpanel_1 = new JLabel("第二个面板panel");
		panel_1.add(lblpanel_1);
		
		textField = new JTextField();
		panel_1.add(textField);
		textField.setColumns(10);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(28, 157, 366, 95);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("选项版1", null, panel_2, null);
		
		JLabel label = new JLabel("第一个选项版");
		panel_2.add(label);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("选项版2", null, panel_3, null);
		
		JLabel label_1 = new JLabel("这是第二个选项版");
		panel_3.add(label_1);
	}
}
