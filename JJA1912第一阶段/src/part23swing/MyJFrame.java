package part23swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyJFrame {

	private JFrame frame;
	private JTextField textField;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyJFrame window = new MyJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MyJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		//窗口不允许调整大小
		frame.setResizable(false);
		//设置窗口名称
		frame.setTitle("登录页面");
		
		
		JLabel label = new JLabel("用户名：");
		label.setBounds(28, 23, 54, 15);
		frame.getContentPane().add(label);
		
		textField = new JTextField();
		textField.setBounds(70, 20, 66, 21);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btn_login = new JButton("登录");
		
		btn_login.setBounds(28, 158, 93, 23);
		frame.getContentPane().add(btn_login);
		
		JRadioButton radio_man = new JRadioButton("男");
		radio_man.setSelected(true);
		buttonGroup.add(radio_man);
		radio_man.setBounds(67, 79, 54, 23);
		frame.getContentPane().add(radio_man);
		
		JRadioButton radio_woman = new JRadioButton("女");
		buttonGroup.add(radio_woman);
		radio_woman.setBounds(145, 79, 47, 23);
		frame.getContentPane().add(radio_woman);
		
		JLabel label_1 = new JLabel("性别：");
		label_1.setBounds(28, 83, 54, 15);
		frame.getContentPane().add(label_1);
		
		JButton btn_exit = new JButton("退出");
		btn_exit.setBounds(186, 158, 93, 23);
		frame.getContentPane().add(btn_exit);
		
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 真正事件处理
//				System.out.println("你点了登录按钮");
				
				// 弹出消息框
//				JOptionPane.showMessageDialog(null, "你点了退出");
				JOptionPane.showMessageDialog(null, "你点了登录按钮", "按钮提示", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		btn_exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 真正事件处理
//				System.out.println("你点了退出按钮");
				
				// 弹出确认框
//				JOptionPane.showConfirmDialog(null, "你确定退出吗?"); // 是 否 取消
				int n = JOptionPane.showConfirmDialog(null, "你确定退出吗?", "退出提示", JOptionPane.YES_NO_OPTION);// 是 否
				if (n == 0) { // 用户选择是返回值0 确定
					// 做退出
					System.exit(0);
				}
				if (n == 1) { // 用户选择否返回值1
					
				}
			}
		});
		
	}
}
