package part23swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class 表格 {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					表格 window = new 表格();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public 表格() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("表格：");
		label.setBounds(38, 27, 54, 15);
		frame.getContentPane().add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 62, 348, 127);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		
		// 表格模型中第一个参数：数据(二维数组)
		Object[][] data = new Object[10][4];
		// 表格模型中第一个参数：列信息(一维数组)
		String[] cols = {"编号", "用户名", "年龄", "操作"};
		
		
		table.setModel(new DefaultTableModel(data,cols));
		scrollPane.setViewportView(table);
	}
}
