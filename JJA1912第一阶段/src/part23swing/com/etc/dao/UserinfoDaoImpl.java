package part23swing.com.etc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import part23swing.com.etc.pojo.Userinfo;
import part23swing.com.etc.utils.DBUtil;
/**
 * 操作数据库
 * @author Administrator
 *
 */
public class UserinfoDaoImpl implements UserinfoDao{

	@Override
	public Userinfo queryByUsernameAndPassword(String username, String password) {
		Userinfo userinfo = null;
		ResultSet rs = DBUtil.doQuery("select user_id,age from userinfo where username = ? and password = ?", username, password);
		try {
			while (rs.next()) {
				int userId = rs.getInt("user_id");
				int age = rs.getInt("age");
				// 构建对象
				userinfo = new Userinfo(userId, username, password, age);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userinfo;
	}

}
