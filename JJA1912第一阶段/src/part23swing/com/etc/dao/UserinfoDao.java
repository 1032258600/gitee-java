package part23swing.com.etc.dao;

import part23swing.com.etc.pojo.Userinfo;

public interface UserinfoDao {

	/**
	 * 根据用户名和密码进行查询
	 * @param username 用户名
	 * @param password 密码
	 * @return 
	 */
	public Userinfo queryByUsernameAndPassword(String username, String password);
}
