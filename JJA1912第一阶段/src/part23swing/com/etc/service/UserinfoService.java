package part23swing.com.etc.service;

import part23swing.com.etc.pojo.Userinfo;

public interface UserinfoService {

	/**
	 * 登录业务
	 * @param username 用户名 
	 * @param password 密码
	 * @return 用户对象信息
	 */
	public Userinfo login(String username, String password);
}
