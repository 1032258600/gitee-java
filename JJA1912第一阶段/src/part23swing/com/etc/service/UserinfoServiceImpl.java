package part23swing.com.etc.service;

import part23swing.com.etc.dao.UserinfoDao;
import part23swing.com.etc.dao.UserinfoDaoImpl;
import part23swing.com.etc.pojo.Userinfo;

public class UserinfoServiceImpl implements UserinfoService{

	private UserinfoDao dao = new UserinfoDaoImpl();
	
	@Override
	public Userinfo login(String username, String password) {
		
		return dao.queryByUsernameAndPassword(username, password);
	}

}
