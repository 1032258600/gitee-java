package part23swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuessGameJFrame {

	private JFrame frame;
	private JTextField text_input;
	private JLabel label_showinfo;
	private JButton btn_guess;
	private JButton btn_again;
	private JButton btn_exit;
	private int randomNum = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessGameJFrame window = new GuessGameJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessGameJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		//窗口不允许调整大小
		frame.setResizable(false);
		//设置窗口名称
		frame.setTitle("猜游戏");
		
		JLabel label = new JLabel("请输入1-10数字：");
		label.setBounds(10, 10, 126, 15);
		frame.getContentPane().add(label);
		
		text_input = new JTextField();
		text_input.setBounds(123, 7, 216, 21);
		frame.getContentPane().add(text_input);
		text_input.setColumns(10);
		
		label_showinfo = new JLabel("欢迎加入游戏，请输入1-10的数字");
		label_showinfo.setBounds(92, 77, 275, 15);
		frame.getContentPane().add(label_showinfo);
		// 获取随机数
		randomNum = (int) (Math.random()*10 +1);
		btn_guess = new JButton("猜");
		btn_guess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// 1获取用户输入的数字
					String userNum = text_input.getText();
					// 字符串转整数
					int num = Integer.parseInt(userNum); // 可能会出现异常： 转换失败的异常
					if (num>=1 && num<=10) {
						// 2 和随机数进行比较
						if (num == randomNum) {
							// 3显示在指定位置
							label_showinfo.setText("恭喜你,猜中了");
						} else if (num > randomNum) {
							label_showinfo.setText("猜大了");
						} else{
							label_showinfo.setText("猜小了");
						}
					}else {
						JOptionPane.showMessageDialog(null, "只能输入1-10之间的整数");
						
					}
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
//					e1.printStackTrace();
					JOptionPane.showMessageDialog(null, "输入不合法，请输入1-10之间的整数");
				}
			}
		});
		btn_guess.setBounds(24, 179, 93, 23);
		frame.getContentPane().add(btn_guess);
		
		btn_again = new JButton("再来一局");
		btn_again.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1 重新对随机数赋值
				randomNum = (int) (Math.random()*10 +1);
				// 2 清空输入框
				text_input.setText("");
				// 3 恢复提示框内容
				label_showinfo.setText("欢迎加入游戏，请输入1-10的数字");
			}
		});
		btn_again.setBounds(149, 179, 93, 23);
		frame.getContentPane().add(btn_again);
		
		btn_exit = new JButton("退出游戏");
		btn_exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "确定退出游戏?", "退出提示", JOptionPane.YES_NO_OPTION) == 0) {
					// 退出系统
					System.exit(0);
				}
			}
		});
		btn_exit.setBounds(274, 179, 93, 23);
		frame.getContentPane().add(btn_exit);
		
		
		
	}

}
