package part23swing;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class 菜单 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					菜单 window = new 菜单();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public 菜单() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(frame.getContentPane(), popupMenu);
		
		JMenuItem mntmCopy = new JMenuItem("Copy");
		popupMenu.add(mntmCopy);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		popupMenu.add(mntmOpen);
		
		JMenuItem mntmCut = new JMenuItem("Cut");
		popupMenu.add(mntmCut);
		
		JMenu mnShow = new JMenu("Show");
		popupMenu.add(mnShow);
		
		JMenuItem menuItem_3 = new JMenuItem("展示数据");
		mnShow.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("展示图形");
		mnShow.add(menuItem_4);
		frame.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(10, 10, 105, 21);
		frame.getContentPane().add(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenu mnNew = new JMenu("New");
		mnFile.add(mnNew);
		
		JMenuItem mntmSystem = new JMenuItem("System");
		mnFile.add(mntmSystem);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem menuItem = new JMenuItem("编辑1");
		mnEdit.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("编辑2");
		mnEdit.add(menuItem_1);
		
		JMenu menu = new JMenu("编辑3");
		mnEdit.add(menu);
		
		JMenuItem menuItem_2 = new JMenuItem("文字");
		menu.add(menuItem_2);
		
		JMenu mnSource = new JMenu("Source");
		menuBar.add(mnSource);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
