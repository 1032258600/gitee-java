package part15工具包;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class SimpleDateFormat类 {
	public static void main(String[] args) {
		System.out.println("-- 字符串转换成Date类--");
		System.out.println("请输入一个日期 格式 yyyy-MM-dd HH:mm:ss");
		Scanner scanner = new Scanner(System.in);
		
		String strDate = scanner.nextLine();
		
		Date date = SimpleDateFormat类.stringToDate(strDate);
		System.out.println(date.toLocaleString());
		
		System.out.println("-- Date类 转换成 自定义格式的字符串--");
		Date date2 = new Date();
	    String str = SimpleDateFormat类.dateToString(date2);
	    System.out.println(str);
	}

	/**
	 * 指定格式的字符串 转换成 Date类
	 */
	public static Date stringToDate(String strDate) {
		
		// 创建一个SimpleDateFormat
		// 传递进来的字符串和yyyy-MM-dd HH:mm:ss格式相匹配
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 2019-10-15 15:30:10
		Date date = null;
		try {
			date = sdf.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	/**
	 * Date类 转换成 指定格式的字符串
	 */
	public static String dateToString(Date date) {
		
		// 创建一个SimpleDateFormat
		// date时间对象会 以yyyy年MM月dd HH时mm分ss秒字符串的形式进行输出
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒 E"); // 2019-10-15 15:30:10
		return sdf.format(date);
	}
}



