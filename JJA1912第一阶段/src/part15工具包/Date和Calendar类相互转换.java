package part15工具包;

import java.util.Calendar;
import java.util.Date;

public class Date和Calendar类相互转换 {

	public static void main(String[] args) {
		// 如何把日历类 转换成 Date类
		Calendar calendar = Calendar.getInstance();
		
		Date date = calendar.getTime();
		
		System.out.println(date.toLocaleString());
		
		// Date类 转换成 日历类
		Date date2 = new Date();
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(date2);
		
		System.out.println(calendar2.get(Calendar.HOUR_OF_DAY));
		
	}
}
