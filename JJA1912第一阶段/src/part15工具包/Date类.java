package part15工具包;

import java.util.Date;

public class Date类 {
	
	public static void main(String[] args) {
		
		// 如何获取当前系统时间
		Date date = new Date();
		System.out.println(date);
		System.out.println(date.toLocaleString());
		System.out.println("年：" + (date.getYear() + 1900));
		System.out.println("月：" + (date.getMonth()+1)); // 计算机中月份0-11，0是1月 1是2月 11是12月
		System.out.println("日期：" + date.getDate());
		System.out.println("星期：" + date.getDay());//3 计算机中星期0-6 0代表周天 1代表周1
		System.out.println("时：" + date.getHours());
		// 关于Date类中方法大部分被废弃，都是推荐使用Calendar类来代替日期时间的操作。
	}

}
