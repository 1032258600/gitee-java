package part15工具包;

import java.util.Calendar;

public class Calendar类 {

	public static void main(String[] args) {
		// Calendar类是一个抽象类，不能使用new关键字进行实例化
		Calendar cal = Calendar.getInstance(); // cal引用中包含了当前系统时间的日历对象
		
		System.out.println(cal);
		System.out.println("--获取日历对象中的各部分的信息--");
		System.out.println("年：" + cal.get(Calendar.YEAR));//2019
		System.out.println("月：" + (cal.get(Calendar.MONTH) + 1)); //8+1=9  MONTH 从0-11 
		System.out.println("日期：" + cal.get(Calendar.DAY_OF_MONTH)); // 4
		//  DAY_OF_WEEK本周的第几天， SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, and SATURDAY.
        //  1-7 1代表第一天1就是星期天   2代表第二天就是星期一   4代表周三
		System.out.println("本周第几天：" + cal.get(Calendar.DAY_OF_WEEK)); //4 
		System.out.println("星期：" + (cal.get(Calendar.DAY_OF_WEEK) - 1)); // 3
		
		// HOUR属性是12小时制 HOUR_OF_DAY是24小时制
		System.out.println("时：" + cal.get(Calendar.HOUR)); //2  12小时制
		System.out.println("时：" + cal.get(Calendar.HOUR_OF_DAY)); //14  24小时制
		
		// 本月的第几天?
		System.out.println("本月第几天：" + cal.get(Calendar.DAY_OF_MONTH)); // 4 
		
		// 本月有多少天? 等价于获取  本月的最大日期 
		System.out.println("本月的最大日期：" + cal.getActualMaximum(Calendar.DAY_OF_MONTH));// 30
		
	
		// 设置未来时间? 2019-10-5 17:30:20
		Calendar cal2 = Calendar.getInstance();
		cal2.set(Calendar.YEAR, 2019);
		cal2.set(Calendar.MONTH, 10-1); // MONTH 0-11  数字10代表11月
		cal2.set(Calendar.DAY_OF_MONTH, 5);
		cal2.set(Calendar.HOUR_OF_DAY, 17); // HOUR_OF_DAY 一天中某一个时
		cal2.set(Calendar.MINUTE, 30);
		cal2.set(Calendar.SECOND, 20);
		System.out.println("未来时间： " + cal2.getTime().toLocaleString());
		
		// 在当前系统时间基础上 添加30分钟
		// add(int field, int amount)  根据日历的规则，将指定的时间量添加或减去给定的日历字段。 
		cal.add(Calendar.MINUTE, 30);
		System.out.println("添加后日期时间： " + cal.getTime().toLocaleString());
		
		// 在当前系统时间基础上 减去1天
		cal.add(Calendar.DAY_OF_MONTH, -1);
		System.out.println("减去后日期时间： " + cal.getTime().toLocaleString());
		
	}
}
