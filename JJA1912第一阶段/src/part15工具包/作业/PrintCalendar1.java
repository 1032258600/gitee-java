package part15工具包.作业;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class PrintCalendar1 {
	/*[根据用户输入的年和月打印这个月的日历]
			从控制台输入一个指定日期时间格式的字符串(2019-09),然后根据用户的输入日期,打印该月的月历出来; */
	public static void main(String[] args) throws ParseException {
		// 1获取用户输入的日期 年-月
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入一个日期格式：2019-10:");
		String dateStr = scanner.nextLine();
		// 2将指定格式的字符串转成Date类
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		Date date = sdf.parse(dateStr);
		// 3 将Date类转换成Calendar日历类
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		// 4 第一天打印在什么位置->这个月的第一天是星期几? 假如显示10月1号
		int week = cal.get(Calendar.DAY_OF_WEEK);// 数字3代表星期2
		// 5 打印多少次?-->这个月总天数-->求这个月的最大日期
		int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		// 打印
		System.out.println("    "  + cal.get(Calendar.YEAR) +"年" + (cal.get(Calendar.MONTH)+1) + "月      ");
		System.out.println("日\t一\t二\t三\t四\t五\t六\t");
		
		// 先打印空格
		for (int i = 1; i < week; i++) {
			System.out.print("*\t");
		}
		
		// 打印对应的日期
		for (int i = 1; i <= days; i++) { //i就代表日期  1-28/29/30/31
			System.out.print(i+"\t");
			 
			// 判断什么时候换行?
			// 每打印出一次，我们的日历就已经变到下一天，所以重新设置日历。
			cal.set(Calendar.DAY_OF_MONTH, i); 
			// 方式1：
			// 判断当天是星期几？ 如果是星期六(数字是7)就得换行
			int week2 = cal.get(Calendar.DAY_OF_WEEK);
			if (week2 == 7) {
				System.out.println();
			}
			
		}
	}
}
