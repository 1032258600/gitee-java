package part15工具包.作业;

import java.util.Arrays;
import java.util.Random;

public class 作业_36选7 {
	
	public static void main(String[] args) {
//		new 作业_36选7().t1();
		new 作业_36选7().t2();
	}
	/**
	 * 方式1：使用2个数组
	 */
	public void t1() {
		// 定义一个数组 存放1-36
				int[] array = new int[36];
				
				// 往数组存放1-36
				for (int i = 1; i <= 36; i++) {
					array[i-1] = i; //下标从0开始
				}
				
				// 定义一个开奖结果的数组 存放7个数
				int[] result = new int[7];
				
				// 循环7次 从array数组随机取出不同的7个值放到result数组里面
				for (int i = 0; i < 7; i++) {
					
					// array数组下标是随机数
					int index = new Random().nextInt(36); // 【0-36)等价于【0-35】

					// 判断?? 如果从array数组中取的随机值 已经在result数组中存在，则不能赋值，循环也需要回退一次。
					if (array[index] != -1) {
						result[i] = array[index];
						array[index] = -1; // 将获取的值设置为-1
					}else { // array[index] == 1 说明现在随机取到的值已经被取过，这一次循环不算
						// 循环回退
						i--; 
					}
					
				}
				
				// 打印开奖
				for (int i : result) {
					System.out.println(i);
				}
	}
	/**
	 * 方式2：使用1个数组
	 */
	public void t2() {
		int arr[] = new int[7] ; // 定义一个数组，存储 开奖结果
		Random rd = new Random() ;
		for(int i = 0 ; i < 7 ; i ++)
		{
			arr[i] = rd.nextInt(36)+1 ; // 随机获取1-36，设置为结果数组
			
			for(int j =0 ; j < i ; j++)
			{
				if(arr[i] == arr[j]) // 当前的数和之前存进去的元素一一做对比
				{
					i--;
					//只要i--，那么一定会使j<i，这个条件不成立，从而跳出循环
				}
			}
		}
		
		System.out.println(Arrays.toString(arr));
	}
}
