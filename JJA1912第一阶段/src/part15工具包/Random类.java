package part15工具包;

import java.util.Random;

public class Random类 {
	
	public static void main(String[] args) {
		// 获取随机数方式1： 通过Math类.random
		Math.random(); // [0.0-1.0)
		int b = (int) (Math.random() * 3 +1); // [1-3]
		
		// Random类
		// 创建一个Random类的实例
		Random random = new Random();
		
		random.nextDouble();// [0.0-1.0)
		// [1-3]
		int nextInt = random.nextInt(3)+1;
		
		System.out.println(nextInt);
		
	}

}
