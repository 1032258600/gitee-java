package part09访问修饰符;

public class Test1 {

	// 成员属性
	public int a; // 公有的
	private int b; // 私有的
	int c; // 缺省的
	protected int d; // 保护的
	
	// 成员访问
	public void publicMethod() {
		System.out.println("全部类都可以访问");
	}
	protected void protectMethod() {
		System.out.println("同包下可以调用，不同包中的子类也可以调用");
	}
    void method_缺省() {
    	System.out.println("同包下可以调用");
	}
    private void privateMethod() {
    	System.out.println("仅在本类中可以调用");
	}
  
}
class A{ // 对类和接口的访问修饰符只能是public 和 缺省
	
}
interface B{
	
}