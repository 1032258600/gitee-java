package part09访问修饰符;

public class Test2 {
	
	public static void main(String[] args) {
		
		// 同包下访问修饰符测试：  只有private私有是无法调用的
		Test1 test1 = new Test1();
		test1.a = 10; // a 是public修饰
//		test1.b;// 编译错误  b 是private修饰
		test1.c = 15;// c 是缺省修饰
		test1.d = 15; //d 是protected修饰
		
		
	}

}
