package part06多态;

public class Teacher {

	private String name; // 教师姓名

	public Teacher() {
		
	}

	public Teacher(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 技能方法
	 */
	public String skill() {
		return "老师上课";
	}
}

