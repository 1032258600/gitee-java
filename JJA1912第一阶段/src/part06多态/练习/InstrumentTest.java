package part06多态.练习;

public class InstrumentTest {
	
	public void testPlay(Instrument ins) { // 形参是父类的引用 实际参数是子类
		ins.play();
	}

	public static void main(String[] args) {
		InstrumentTest i = new InstrumentTest();
		i.testPlay(new Piano()); // 实际传递进去是子类
		i.testPlay(new Violin());
	}
}
