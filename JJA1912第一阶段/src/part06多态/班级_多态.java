package part06多态;

public class 班级_多态 { // Class

	private String className; // 班级名
	public 班级_多态() {
	
	}

	public 班级_多态(String className) {
		super();
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	/**
	 * 老师上课方法
	 */
	public String showLesson(Teacher teacher) { // 参数 数据类型是父类，实际传递进来的是子类
		return "班级名：" + this.className + teacher.skill();
	}
	
}











