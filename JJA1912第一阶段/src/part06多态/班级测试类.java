package part06多态;

public class 班级测试类 {

	public static void main(String[] args) {

		// 构建班级
		班级_重载 c1 = new 班级_重载();
		c1.setClassName("JJA1912");

		JavaTeacher java = new JavaTeacher();
		java.setName("陈老师");
		System.out.println(c1.showLesson(java));

		PhpTeacher php = new PhpTeacher();
		php.setName("只老师");
		System.out.println(c1.showLesson(php));

		// 构建班级
		班级_重载 c2 = new 班级_重载("JJA1904");
		JavaTeacher java2 = new JavaTeacher("黄老师");
		System.out.println(c2.showLesson(java2));
		
		// 构建班级
		班级_重载 c3 = new 班级_重载("JJA1904");
		Teacher java3 = new JavaTeacher("莫老师");	
		Teacher php2 = new PhpTeacher("王老师");
		
	}
}
