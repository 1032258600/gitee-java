package part06多态;

public class 班级测试类_多态 {

	public static void main(String[] args) {
		
		// 构建班级
		班级_多态 c = new 班级_多态("JJA1904");
		
		// 上课 
		// 父类的引用指向子类
		Teacher java = new JavaTeacher("莫老师");	
		System.out.println(c.showLesson(java));
		// 父类的引用指向子类
		Teacher php = new PhpTeacher("王老师");
		System.out.println(c.showLesson(php));
		
		// 向上转型的缺点：父类引用不能访问子类的特有属性和方法
		Teacher java2 = new JavaTeacher("莫老师");
//		java2.ttt(); // 编译错误，父类的引用无法调用子类特有的ttt方法
		
		JavaTeacher java3 = new JavaTeacher("莫老师");	
		java3.ttt(); // 编译通过，子类的引用可以调用子类特有的ttt方法
	}
}
