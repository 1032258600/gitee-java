package part06多态;

public class 班级_重载 { // Class

	private String className; // 班级名
	public 班级_重载() {
	
	}

	public 班级_重载(String className) {
		super();
		this.className = className;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	/**
	 * java上课方法
	 */
	public String showLesson(JavaTeacher java) {
		return "班级名：" + this.className + java.skill();
	}
	
	/**
	 * php上课方法
	 */
	public String showLesson(PhpTeacher php) {
		return "班级名：" + this.className + php.skill();
	}
	
	/**
	 * ios上课方法
	 *//*
	public String showLesson(IosTeacher ios) {
		return "班级名：" + this.className + ios.skill();
	}*/
	
	
}


