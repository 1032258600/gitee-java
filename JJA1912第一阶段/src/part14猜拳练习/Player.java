package part14猜拳练习;

public abstract class Player {
	private String name; // 姓名
	private int score; // 积分
	
	public Player(String name, int score) {
		super();
		this.name = name;
		this.score = score;
	}
	public Player() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * 出拳方法
	 * 1 石头 2剪刀 3布
	 * @return
	 */
	public abstract int showFist();

}
