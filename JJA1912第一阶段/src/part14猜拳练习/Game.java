package part14猜拳练习;

public class Game {
	// 定义2个对象
	private Person person;
	private Computer computer;
	
	// 通过构造方法设置成员属性
	public Game(Person person, Computer computer) {
		super();
		this.person = person;
		this.computer = computer;
	}
	
	public void pk() {
		// 循环5次
		for (int i = 0; i < 5; i++) {
			System.out.println("当前第" + (i+1) +"轮");
			compare();
		}
		
		// 显示最终的积分结果
		System.out.println("---- 最终结果-----");
		System.out.println("用户：" + person.getName() + " 积分：" + person.getScore());
		System.out.println("电脑：" + computer.getName() + " 积分：" + computer.getScore());
	}
	
	/**
	 * 出拳判断和积分累加
	 */
	public void compare() {
		// 人 出拳数字
		int pFist = person.showFist();// 1石头 2剪刀 3布
		// 电脑 出拳数字
		int cFist = computer.showFist();// 1石头 2剪刀 3布
					
		// 将出拳数对应中文
		String pValue = changeStr(pFist);
		String cValue = changeStr(cFist);
		// 人赢
		if (pFist == 1 && cFist == 2 || pFist == 2 && cFist == 3 || pFist == 3 && cFist == 1) {
			System.out.println("用户：" + person.getName() + " 出：" + pValue);
			System.out.println("电脑：" + computer.getName() + " 出：" + cValue);
			System.out.println("用户：" + person.getName() + " 赢,积分+1");
			// 积分累加
			person.setScore(person.getScore() + 1);
		}else if (pFist == cFist) { // 平局
			System.out.println("用户：" + person.getName() + " 出：" + pValue);
			System.out.println("电脑：" + computer.getName() + " 出：" + cValue);
			System.out.println("平局");
		}else {
			System.out.println("用户：" + person.getName() + " 出：" + pValue);
			System.out.println("电脑：" + computer.getName() + " 出：" + cValue);
			System.out.println("电脑：" + computer.getName() + " 赢,积分+1");
			// 积分累加
			computer.setScore(computer.getScore() + 1);
		}
	}
	/**
	 * 将出拳数字 转换 对应中文
	 * @return
	 */
	public String changeStr(int fist) {
		String value = "";
		switch (fist) {
		case 1:
			value = "石头";
			break;
		case 2:
			value = "剪刀";
			break;		
		case 3:
			value = "布";
			break;
		default:
			break;
		}
		return value;
	}

	

}
