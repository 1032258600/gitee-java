package part14猜拳练习;

import java.util.Scanner;

public class Person extends Player{
	
	Scanner scanner = new Scanner(System.in);
	public Person() {
	}

	public Person(String name, int score) {
		super(name, score);
	}
	@Override
	public int showFist() {
		int fist = 0;
		while (true) {
			System.out.println("请出拳: 1石头 2剪刀 3布");
			fist = scanner.nextInt();
			if (1<=fist && fist<=3) {
				break; // 结束循环
			}else {
				System.out.println("出拳错误，请重新输入");
			}
		}
		return fist;
	}

}
