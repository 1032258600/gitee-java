package part14猜拳练习;

public class Test {

	public static void main(String[] args) {
		// 方式2：通过setter方法设置成员属性
		Person person = new Person();
		person.setName("小白");
		person.setScore(0);
		
		Computer computer = new Computer();
		computer.setName("AI");
		computer.setScore(0);
		
		Game game = new Game(person, computer);
		game.pk();
		
		// 方式2：通过构造方法设置成员属性
		Game game2 = new Game(new Person("大白", 0), new Computer("机器人", 0));
		game2.pk();
		
	}
}
