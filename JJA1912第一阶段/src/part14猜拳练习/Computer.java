package part14猜拳练习;

public class Computer extends Player {
	public Computer() {
	}

	public Computer(String name, int score) {
		super(name, score);
	}

	@Override
	public int showFist() {
		// 随机获取1-3整数之间  Math.random()随机数方法【0.0-1.0)
	    int fist = (int) (Math.random()*3 + 1);
		return fist;
	}

}
