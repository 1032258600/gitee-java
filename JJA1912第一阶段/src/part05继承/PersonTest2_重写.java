package part05继承;

public class PersonTest2_重写 {
	public static void main(String[] args) {

		// 子类的引用 指向子类
		Student student0 = new Student();
		student0.eat();//输出的是子类自己的方法实现，因为在Student子类中重写了父类的方法
		student0.sleep();//输出的是子类自己的方法实现，因为在Student子类中重写了父类的方法

		Teacher teacher0 = new Teacher();
		teacher0.eat();//输出的是父类方法的实现，因为在Teacher这个子类中并没有重写父类中的eat方法
		teacher0.sleep();//输出的是父类方法的实现，因为在Teacher这个子类中并没有重写父类中的sleep方法
		
		
		// 父类的引用 指向子类
		Person student = new Student();
		student.eat();//

		Person teacher = new Teacher();
		teacher.eat();//

		
		Person person = new Person();
		System.out.println(person.toString());
		
		Student student1 = new Student();
		System.out.println(student1);
	}
}
