package part05继承;

public class PersonTest {

	public static void main(String[] args) {
		
		// 子类的引用: 可以访问父类的公开的属性和方法
		Student student = new Student();  // 类 引用名 = new 类的构造方法()
		
		student.num = 10; // 父类中的公开的属性
		student.setPname("小叶");// 父类中的公开的属性
		student.setAge(18);// 父类中的公开的方法
		student.eat();// 父类中的公开的方法
		student.sleep();// 父类中的公开的方法
		
		student.setScore(100);// 子类中的特有方法
		student.study(); // 子类中的特有方法
		
		Teacher teacher = new Teacher();
		teacher.setPname("陈老师");
		teacher.setGender("女");
		teacher.setSalary(500000);
		System.out.println(teacher.getPname() + ",性别： " + teacher.getGender() + ",工资：   " + teacher.getSalary());
		
		
		// 父类的引用： 父类不能访问子类特有的属性和方法,只能访问父类中的属性和方法
		// 父类的引用指向子类   父类 引用名 = new 子类的构造方法()
		Person person = new Student();
	    person.setPname("小黄学生");
	    person.num = 100;
	    System.out.println( person.getPname() );
	    // 无法访问student类中特定公有study方法和setScore方法
	    
	    Person person2 = new Teacher();
	    person2.setPname("白老师");
	    person2.setGender("男");
	    System.out.println(person2.getPname() + person2.getGender() );
	    
	    
	}
}
