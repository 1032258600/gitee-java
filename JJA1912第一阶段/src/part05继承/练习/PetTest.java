package part05继承.练习;

import java.awt.Color;

public class PetTest {
	public static void main(String[] args) {
		
		Petpig petpig = new Petpig("佩奇", "女", 15, 10, Color.blue);
		System.out.println(petpig.toString());
		// 重写的方法
		petpig.eatFood();
		petpig.move();
		// 特有的方法
		System.out.println(petpig.rolling());

	}

}
