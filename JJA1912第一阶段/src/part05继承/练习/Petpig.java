package part05继承.练习;

import java.awt.Color;

public class Petpig extends Pet{
	
	private Color haircolor; // 头发颜色

	public Petpig() {
		
	}
	
	public Petpig(String name, String gender, int exp, int lever,Color haircolor) {
		super(name, gender, exp, lever); // 调用父类的有参构造
		this.haircolor = haircolor;
	}

	public Color getHaircolor() {
		return haircolor;
	}

	public void setHaircolor(Color haircolor) {
		this.haircolor = haircolor;
	}
	
	public String rolling() {
		return "头发颜色为" + this.haircolor + super.getName() + "在滚动~~";
	}
	
    @Override
    public void move() {
    	super.move();
    	System.out.println(super.getName() + "在慢慢移动");
    }
    
    @Override
    public void eatFood() {
    	super.eatFood();
    	System.out.println(super.getName() + "在吃西瓜");
    }

	@Override
	public String toString() {
		return "Petpig [haircolor=" + haircolor + ", toString()=" + super.toString() + "]";
	}
    
    
}
