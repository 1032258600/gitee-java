package part05继承.练习;

public class Pet {

	private String name;
	private String gender;
	private int exp;
	private int lever;
	
	public Pet() {
		super();
	}
	public Pet(String name, String gender, int exp, int lever) {
		super();
		this.name = name;
		this.gender = gender;
		this.exp = exp;
		this.lever = lever;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public int getLever() {
		return lever;
	}
	public void setLever(int lever) {
		this.lever = lever;
	}
	public void move() {
		System.out.println("父类中跟随主人的方法");
	}
	public void eatFood() {
		System.out.println("父类中吃食物的方法");
	}
	@Override
	public String toString() {
		return "Pet [name=" + name + ", gender=" + gender + ", exp=" + exp + ", lever=" + lever + "]";
	}
	
	
}
