package part05继承;

/**
 * 父类
 * @author Administrator
 *
 */
class Apple {
	private String color;
	public int num;
	
	public Apple() {
		System.out.println("父类的构造方法");
	}
	
	public Apple(String color) {
		super();// 调用父类Object类的无参构造
		this.color = color;
	}

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public void t() {
		System.out.println("父类 t");
	}
}
/**
 * 子类
 * @author Administrator
 */
class SmallApple  extends Apple {
	private String size;
	
	public SmallApple() {
		// 默认就会调用父类无参的构造方法，使用super()显式声明调用父类的无参构造方法 
		// 只能放在构造方法中第一个语句。
		super();// 父类Apple的无参构造
		System.out.println("子类的构造方法");
	}
	
	public SmallApple(String size, String color) {
		// 使用super(参数列表)显式声明调用父类的有参构造方法 
//		super("银色");
		super(color);// 将color属性传递给父类Apple的有参构造
		this.size = size;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	@Override
	public void t() {
		
		System.out.println("子类 t");
		// 显式调用父类的公开的方法
		super.t();
		// 显式调用父类的公开的属性
		super.num = 500;
		System.out.println(super.num);
	}
}


public class 继承中的构造和super {
	
    public static void main(String[] args) {
    	SmallApple  apple = new SmallApple();
    	apple.t();
    	
     	SmallApple  apple2 = new SmallApple("4.7尺寸","金色");
     	System.out.println(apple2.getColor() +"," + apple2.getSize());
	}
	
}

