package part05继承.作业;

public class Staff {
	private int num;
	private String name;
	private float rateOfAttend; // 出勤率
	private float basicSal; // 基本工资
	private float prize; // 奖金
	
	public Staff() {
		super();
	}

	public Staff(int num, String name, float rateOfAttend, float basicSal, float prize) {
		super();
		this.num = num;
		this.name = name;
		this.rateOfAttend = rateOfAttend;
		this.basicSal = basicSal;
		this.prize = prize;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getRateOfAttend() {
		return rateOfAttend;
	}

	public void setRateOfAttend(float rateOfAttend) {
		this.rateOfAttend = rateOfAttend;
	}

	public float getBasicSal() {
		return basicSal;
	}

	public void setBasicSal(float basicSal) {
		this.basicSal = basicSal;
	}

	public float getPrize() {
		return prize;
	}

	public void setPrize(float prize) {
		this.prize = prize;
	}
	


	/**
	 * 基本信息输出 
	 */
	public String showInfo() {
		return  "员工基本信息 [编号=" + num + ", 姓名=" + name + ", 出勤率=" + rateOfAttend + ", 基本工资=" + basicSal
				+ ", 奖金=" + prize + "]";
	}
	
	/**
	 * 实发工资信息
	 */
	public float wage() {
		return basicSal + prize * rateOfAttend; // 员工实发工资
	}
	
}
