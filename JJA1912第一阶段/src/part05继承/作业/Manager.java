package part05继承.作业;

public class Manager extends Staff {
	private float totalDeductRate; // 经理提成比例
	private float totalAmount; // 总销售额
	
	
	public Manager() {
		super();
	}
	
	public Manager(int num, String name, float rateOfAttend, float basicSal, float prize,float totalDeductRate, float totalAmount) {
		super(num, name, rateOfAttend, basicSal, prize);
		this.totalDeductRate = totalDeductRate;
		this.totalAmount = totalAmount;
	}

	public float getTotalDeductRate() {
		return totalDeductRate;
	}
	public void setTotalDeductRate(float totalDeductRate) {
		this.totalDeductRate = totalDeductRate;
	}
	public float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@Override
	public String showInfo() {
		// TODO Auto-generated method stub
		return super.showInfo() + "提成比例： " +  totalDeductRate +  " ，总销售额："  + totalAmount;
	}

	@Override
	public float wage() {
		// TODO Auto-generated method stub
		return super.wage() + totalDeductRate * totalAmount ;
	}
}
