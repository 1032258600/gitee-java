package part05继承.作业;

public class StaffTest {

	public static void main(String[] args) {

		// 构建一个员工
		Staff staff = new Staff(1, "叶鸿", 1.0f, 5000, 1100);
		System.out.println(staff.showInfo());
		System.out.println("实发工资：" + staff.wage());

		// 构建一个销售
		Staff sale = new Saleman(2, "惠敏", 1.0f, 6000, 5000, 0.2f, 10000);
		System.out.println(sale.showInfo());
		System.out.println("实发工资：" +sale.wage());
		
		// 构建一个经理
		Staff manager = new Manager(3, "志伟", 0.8f, 10000, 6000, 0.4f, 15000);
		System.out.println(manager.showInfo());
		System.out.println("实发工资：" +manager.wage());
	}
}
