package part05继承.作业;

public class Saleman extends Staff{

	private float deductRate; // 销售员提成比例
	private float personAmount; // 个人销售额
	
	public Saleman() {
	}

	public Saleman(int num, String name, float rateOfAttend, float basicSal, float prize,float deductRate, float personAmount) {
		super(num, name, rateOfAttend, basicSal, prize);
		this.deductRate = deductRate;
		this.personAmount = personAmount;
	}

	public float getDeductRate() {
		return deductRate;
	}

	public void setDeductRate(float deductRate) {
		this.deductRate = deductRate;
	}

	public float getPersonAmount() {
		return personAmount;
	}

	public void setPersonAmount(float personAmount) {
		this.personAmount = personAmount;
	}
	
	@Override
	public String showInfo() {
		// TODO Auto-generated method stub
		return super.showInfo() + "个人销售额： " +  personAmount + ",提成比例： " + deductRate;
	}
	
	@Override
	public float wage() {
		
		return super.wage() + personAmount * deductRate;
	}
	
}
