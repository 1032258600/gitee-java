package part05继承.案例1;

public class StudentTest {
	
	public static void main(String[] args) {
		SmallStudent smallStudent = new SmallStudent();
		smallStudent.setStuName("小学生");// 调用父类中的public修饰的方法
		smallStudent.setStuNo("001");// 调用父类中的public修饰的方法
		smallStudent.setGender("男");// 调用父类中的public修饰的方法
		smallStudent.num = 10;// 调用父类中的public修饰的属性
		smallStudent.setFlowers(100); // 调用子类中自己的方法
		
		smallStudent.study();// 调用父类中的public修饰的方法
		smallStudent.lol();// 调用子类中自己的方法
		System.out.println(smallStudent);
		
		BigStudent bigStudent = new BigStudent();
		bigStudent.setStuName("大学生");
		bigStudent.setStuNo("20190540052");
		bigStudent.setGender("男");
		bigStudent.num = 200;
		bigStudent.setScore(56);// 调用子类中自己的方法
		bigStudent.study();// 调用父类中的public修饰的方法
		
		System.out.println(bigStudent);
		
		
		System.out.println("----继承是单方向----");
		MiddleStudent mStudent = new MiddleStudent();
		mStudent.study(); // 子类访问父类的公有的方法
		mStudent.eat(); // 子类访问自己的方法
		
		Student student = new MiddleStudent();
		student.study(); 
		//student.eat();// 编译错误，因为父类的引用是无法访问子类自己的属性和方法。
		
		
	}

}
