package part05继承.案例1;
/**
 * 中学生
 * @author Administrator
 *
 */
public class MiddleStudent extends Student{
	
	private int friends; // 朋友数量
	

	public int getFriends() {
		return friends;
	}

	public void setFriends(int friends) {
		this.friends = friends;
	}
	
	public void eat() {
		System.out.println("中学生自己拥有eat的方法");
	}
}
