package part05继承.案例1;
/**
 * 学生父类
 * @author Administrator
 *
 */
public class Student extends Object { // 一个类如果显式声明继承哪一个类，默认继承最高类Object类
	
	private String stuNo;
	private String stuName;
	private String gender;
	// 公开的属性
	public int num;
	
	public String getStuNo() {
		return stuNo;
	}
	public void setStuNo(String stuNo) {
		this.stuNo = stuNo;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	// 学习方法
	public void study() {
		System.out.println("学习方法");
	}
	
}
