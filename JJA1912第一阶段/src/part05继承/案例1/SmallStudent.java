package part05继承.案例1;

/**
 * 小学生
 * 
 * @author Administrator
 *
 */
public class SmallStudent extends Student{

	// 子类特有的属性
	private int flowers; // 小红花数

	public int getFlowers() {
		return flowers;
	}

	public void setFlowers(int flowers) {
		this.flowers = flowers;
	}
	
	// 子类特有的方法
	public void lol() {
		System.out.println("小学生打英雄联盟");
	}
	
}
