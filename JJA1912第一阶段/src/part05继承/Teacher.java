package part05继承;
/**
 * 老师类
 * 子类
 * @author Administrator
 *
 */
public class Teacher extends Person{

	// 特定属性
	private int salary; // 教师工资

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	// 特定方法
	public void teach() {
		System.out.println("老师类中特定教书方法");
		// 老师的信息
		System.out.println("老师信息：姓名：" + getPname() + ",年龄：" + getAge() + ",性别：" + getGender() +",工资:" + getSalary());
		
	}
}
