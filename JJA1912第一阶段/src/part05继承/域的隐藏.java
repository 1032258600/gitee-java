package part05继承;

class A {
	public int x = 10;
	public int y = 20;
}

class B extends A {
	
	public int z = 30;
	// 子类拥有一个和父类中完全一样的数据成员
	public int x = 250;
	
	public void show() {
		System.out.println(x +","+ y +"," + z);// 此时x就是B子类中自己的属性值，而不是父类中x属性
		System.out.println(super.x);// 调用父类中的同名属性，需要使用super关键字
	}
}

public class 域的隐藏 {
	public static void main(String[] args) {
//		B b = new B();
//		b.show();
		
		new B().show();
	}

}
