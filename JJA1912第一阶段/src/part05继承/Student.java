package part05继承;
/**
 * 子类（派生类）
 * 学生类
 * @author Administrator
 *
 */
public class Student extends Person {
	 
	// 自动拥有了Person父类中的公开的属性和方法
	
	// 特定的属性
	private int score;

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	// 特定的方法
	public void study() {
		System.out.println("学生类中特定的学习方法");
		// 输出学生个人信息 姓名 年龄 性别 分数
		System.out.println("学生信息：姓名：" + getPname() + ",年龄：" + getAge() + ",性别：" + getGender() +",分数:" + getScore() );
	}

	/**
	 * 子类重写(覆写)父类的eat方法
	 * 方法的修饰符、返回值类型、方法名、参数项完全和父类中的方法一致
	 * 只有方法体的实现不同。
	 */
	@Override
	public void eat() {
		System.out.println("子类 学生类中的吃饭方法");
	}
	/**
	 * 子类重写(覆写)父类的sleep方法
	 */
	@Override
	public void sleep() {
		System.out.println("子类 学生类中的睡觉方法");
	}

	@Override
	public String toString() {
		return "Student [score=" + score + ", toString()=" + super.toString() + "]";
	}

	

	
	
}
