package part05继承;
/**
 * 父类 (超类 基类)
 * @author Administrator
 *
 */
public class Person { // class Person extends Object 默认继承Object类

	// 共同属性
	private String pname;
	private int age ;
	private String gender;
	// 公开的属性
	public int num;
	
	// 共同方法
	public void eat() {
		System.out.println("父类中的吃饭方法");
	}
	public void sleep() {
		System.out.println("父类中的睡觉方法");
	}
	
	/**
	 * 重写了父类Object中的方法
	 */
	@Override
	public String toString() {
		return "Person [pname=" + pname + ", age=" + age + ", gender=" + gender + ", num=" + num + "]";
	}
	
	public Person() {
		super();
	}
	
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	
	
}
