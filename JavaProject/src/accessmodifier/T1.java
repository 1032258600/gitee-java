package accessmodifier;

public class T1 {
	
	public static void main(String[] args) {
		A1 a1 = new A1();
		a1.a = 8;
		a1.b = 5;
		
		a1.f();
		a1.g();
	}

}
