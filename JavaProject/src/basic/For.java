package basic;

import java.util.Scanner;

public class For {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		Float sum = 0f;
		for (int i = 0; i < 5; i++) {

			Float score = scanner.nextFloat();

			sum = score + sum;
		}

		System.out.println(sum);

		int days = 0;
		for (
				int i = 2008;
				i < 2019;
				i++) {

			if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0) {
				days = days + 366;
			} else {
				days = days + 365;
			}

		}

		System.out.println(days);
		scanner.close();
	}

}
