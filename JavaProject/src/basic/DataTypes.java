package basic;

public class DataTypes {
	
	public static void main(String[] args) {
		
		byte a = 100;
		
		char b = 'S';
		
		boolean c = true;
		
		short d = 200;
		
		int e = 300;
		
		long f = 400L;
		
		float g = 500.1f;
		
		double h = 600.0;
		
		
	}
}
