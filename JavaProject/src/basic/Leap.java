package basic;

import java.util.Scanner;

public class Leap {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入：");

		int n = sc.nextInt();

		if ((n % 4 == 0 && n % 100 != 0) || (n % 400 ==0)) {
			System.out.println(n +"是闰年");
		} else {
			System.out.println(n +"不是闰年");
		}
		
		sc.close();
	}

}
