package basic;

import java.util.Scanner;

public class Switch {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int i = scanner.nextInt();
		switch (i) {
		case 1:
			System.out.println("注册");
			break;
		case 2:
			System.out.println("登录");
			break;
		case 3:
			System.out.println("退出");
			break;
		default:
			break;
		}
		System.out.println("年份");
		int year = scanner.nextInt();
		System.out.println("月份");
		int month = scanner.nextInt();

		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("31");
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("30");
			break;

		case 2:
			if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
				System.out.println("29");
			} else {
				System.out.println("28");
			}
			break;

		default:
			System.out.println("输入有误");
			break;
		}

		scanner.close();

	}
}
