package Encapsulation;

public class Book3 {
	
	private String title;
	private int pageNum;
	private String type;
	
	public Book3() {
		
	}
	
	public Book3(String title, int pageNum, String type) {
		super();
		this.title = title;
		this.pageNum = pageNum;
		this.type = type;
	}
	public Book3(String title, int pageNum) {
		super();
		this.title = title;
		this.pageNum = pageNum;
		this.type = "计算机";
	}
	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the pageNum
	 */
	public int getPageNum() {
		return pageNum;
	}
	/**
	 * @param pageNum the pageNum to set
	 */
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public void detail() {
		System.out.println("名称："+this.title+"页数："+this.pageNum+"种类"+this.type);
	}
}
