package Encapsulation;

public class EncapsulationTest {
	
	public static void main(String[] args) {
		Encapsulation encapsulation = new Encapsulation();
		
		encapsulation.setName("BBB");
		encapsulation.setAge(5);
		short weight = 55;
		encapsulation.setWeight(weight);
		System.out.println(encapsulation.toString());
		
		Encapsulation encapsulation2 = new Encapsulation("AAA",20);
		System.out.println(encapsulation2.toString());
		
		Encapsulation encapsulation3 = new Encapsulation("AAA");
		encapsulation3.setAge(30);
		System.out.println(encapsulation3.toString());
		
		Encapsulation encapsulation4 = new Encapsulation(60);
		encapsulation4.setName("WWW");
		System.out.println(encapsulation4.toString());
		
	}

}
