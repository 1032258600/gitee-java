package Encapsulation;

public class SE {
	
	private int id;
	private String name;
	private String gender;
	private double salary;
	private int hot;
	public SE() {
		super();
		this.id = 1;
		this.name = "e1";
		this.gender = "男";
		this.salary = 8000;
		this.hot = 85;
	}
	public SE(int id, String name, String gender, double salary, int hot) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.hot = hot;
	}
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
	/**
	 * @return the hot
	 */
	public int getHot() {
		return hot;
	}
	/**
	 * @param hot the hot to set
	 */
	public void setHot(int hot) {
		this.hot = hot;
	}
	public void show() {
		System.out.println("员工编号："+id+"姓名："+name+"性别："+gender+"工资："+salary+"关注度："+hot);
	}

}
