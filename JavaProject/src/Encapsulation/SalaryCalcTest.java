package Encapsulation;

public class SalaryCalcTest {
	
	public static void main(String[] args) {
		SE se = new SE();
		se.setId(1);
		se.setName("e1");
		se.setGender("男");
		se.setSalary(9000);
		se.setHot(85);
		se.show();
		
		SalaryCalc salaryCalc = new SalaryCalc();
		System.out.println(salaryCalc.pay(se));
		
		PM pm = new PM(2,"e2","女",9000,90,0.9);
		pm.show();
		System.out.println(salaryCalc.pay(pm));
	}

}
