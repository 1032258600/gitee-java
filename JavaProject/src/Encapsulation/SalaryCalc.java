package Encapsulation;

import Extends.Manager;
import Extends.Saleman;

public class SalaryCalc {
	
	public double pay(SE se) {
		return se.getSalary();
	}
	
	public double pay(PM pm) {
		return pm.getSalary() * (1 + pm.getBonus());
	}
	
	public double pay(Saleman saleman) {
		return saleman.basicPay() + saleman.getPersonAmount() * saleman.getDeductRate();
	}
	
	public double pay(Manager manager) {
		return manager.basicPay() + manager.getTotalAmount() * manager.getTotalDeductRate();
	}

}
