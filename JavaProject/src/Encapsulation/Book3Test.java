package Encapsulation;

public class Book3Test {
	
	public static void main(String[] args) {
		Book3 book = new Book3("基础", 5000);
		Book3 book3 = new Book3("基础", 5000,"JAVA");
		book.setType("信息安全");
		book.detail();
		book3.detail();
		
	}

}
