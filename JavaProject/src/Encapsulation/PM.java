package Encapsulation;

public class PM {

	private int id;
	private String name;
	private String gender;
	private double salary;
	private int exp;
	private double bonus;
	public PM() {
		super();
		this.id = 1;
		this.name = "e2";
		this.gender = "女";
		this.salary = 8000;
		this.exp = 10;
		this.bonus = 0.2;
	}
	public PM(int id, String name, String gender, double salary, int exp, double bonus) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.exp = exp;
		this.bonus = bonus;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the salary
	 */
	public double getSalary() {
		return this.salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
	/**
	 * @return the exp
	 */
	public int getExp() {
		return exp;
	}
	/**
	 * @param exp the exp to set
	 */
	public void setExp(int exp) {
		this.exp = exp;
	}
	/**
	 * @return the bonus
	 */
	public double getBonus() {
		return bonus;
	}
	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	
	public void show() {
		System.out.println("员工编号："+id+"姓名："+name+"性别："+gender+"工资："+salary+"项目经验："+exp+"项目分红："+bonus);
	}
}
