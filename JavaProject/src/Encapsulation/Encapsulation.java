package Encapsulation;

public class Encapsulation {
	
	private String name;
	private int age;
	private short weight;
	
	public Encapsulation() {
		super();
	}
	public Encapsulation(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	public Encapsulation(String name) {
		super();
		this.name = name;
	}
	public Encapsulation(int age) {
		super();
		this.age = age;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the weight
	 */
	public short getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(short weight) {
		this.weight = (short) (weight + 1);
		this.weight += 1;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Encapsulation [name=" + name + ", age=" + age + ", weight=" + weight + "]";
	}
	
	
	
}
