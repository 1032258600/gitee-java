package Interface;


/**
 * 一个类仅有一个接口
 */


public interface Ia {//①接口使用interface关键字修饰。可以用public 缺省和 abstract修饰

	
	/*public Ia() {//⑧接口中没有构造方法。
		
	}
	*/
	int a = 3;
	final float b = 3;
	static final float c = 3;
	public static final float d = 4;//②接口中只能有静态常量，默认使用static final,访问修饰默认public修饰 
	
	void func();//③接口、接口的常量、接口中的方法只能使用public、缺省访问修饰符
//	private void func2();
	
	/**
	 * ④接口中可以有抽象方法、static静态方法、default默认方法。
	 * 在jdk1.7之前接口中只能有抽象方法，jdk1.8之后可以有静态和默认方法。
	 */
	abstract void func3();
	public abstract void func4();// 抽象方法,默认public修饰
	public static void func5() {// 静态方法（jdk1.8+新增）
	}
	
	public default void func6() {// default关键字修饰的默认方法（jdk1.8+新增）
	}
	
	
}

class A implements Ia{
//	Ia ia = new Ia();//⑤接口不能实例化，只能被实现。通过implements 关键字

	@Override
	public void func() {
		// TODO Auto-generated method stub
		
	}

	/*@Override
	public void func2() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public void func3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void func4() {
		// TODO Auto-generated method stub
		
	}
	/**
	 * ⑩实现接口的类如果显式重写接口中的default方法，不需要再使用default修饰;
	 * 实现接口的类或者子接口不会继承接口中的static静态方法。
	 */
	@Override
	public void func6() {
		// TODO Auto-generated method stub
		Ia.super.func6();
	}
}


class B implements Ia{//⑦接口可以有多个实现类。

	@Override
	public void func() {
		// TODO Auto-generated method stub
		
	}

	/*@Override
	public void func2() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public void func3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void func4() {
		// TODO Auto-generated method stub
		
	}

}






