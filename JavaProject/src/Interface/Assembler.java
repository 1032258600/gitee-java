package Interface;

public class Assembler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MainBoard mainBoard = new MainBoard();
		
		SoundCard soundCard = new SoundCard();
		mainBoard.usePCICard(soundCard);
		
		NetworkCard networkCard = new NetworkCard();
		mainBoard.usePCICard(networkCard);
	}

}
