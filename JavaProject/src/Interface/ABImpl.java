package Interface;

/**
 * ⑥一个类可以实现多个接口，必须重写接口中的抽象方法，解决Java单继承的问题。
 * 如果不重写，该类就需要定义为一个抽象类。
 * @author Administrator
 *
 */
public class ABImpl implements Ia,Ib{

	@Override
	public void func() {
		// TODO Auto-generated method stub
		
	}

	/*@Override
	public void func2() {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public void func3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void func4() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void func7() {
		// TODO Auto-generated method stub
		
	}
	/**
	 * 实现类里面显式重写default的方法，而关于default的方法的重写
	 * 我们在实现类中不需要继续出现default关键字也不能出现default关键字。
	 */
	@Override
	public void func6() {
		// TODO Auto-generated method stub
		Ia.super.func6();
		Ib.super.func6();
	}

}
