package Interface;

public class GameTest {

	public static void main(String[] args) {

		Person person = new Person();
		AI ai = new AI();

		Game game = new Game();

		
		System.out.println("玩家" + person.name + "\t出拳:" + game.pk(person));
		System.out.println("电脑AI" + "\t出拳:" + game.pk(ai));

		if (game.pk(person).equals(game.pk(ai))) {
			System.out.println("平局");
		}

	}

}
