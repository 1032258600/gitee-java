package threekeywords;

public class FinalKey {

	//⑤final不能修饰构造方法。
	/*public final FinalKey(){
		
	}*/
	
	final int a = 1;
	
	public final void f() {

	}
	
	public final void ff(final int t) {
		//③final修饰参数：表示常量参数，只可以使用，不可以重新赋值。
		//t = 6;
	}

	public static void main(String[] args) {
		//FinalKey finalKey = new FinalKey();
		// ①final修饰变量：表示常量，赋值之后不能被修改。
		//finalKey.a = 3;
	}

}

final class Subclass extends FinalKey {

	public static void main(String[] args) {
		// ②final修饰方法：表示常量方法，可以被继承，子类可以调用。
		Subclass subclass = new Subclass();
		System.out.println(subclass.a);
		subclass.f();
	}

	// ②final修饰方法：不能被重写。
	/*public final void f() {

	}*/
}

//④final修饰类：表示常量类，不能被继承，没有子类。
/*class B extends Subclass{
	
}*/



