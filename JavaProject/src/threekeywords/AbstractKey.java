package threekeywords;

public abstract class AbstractKey {//①abstract只能修饰类和方法
	
	
	/*public abstract AbstractKey() {
		//④抽象类有构造方法，但是不能用abstract修饰
	}*/
	
	public abstract void method();//②一个类只要有一个抽象方法，这个类就必须为抽象类。
	
	public void func() {//③抽象类可以有普通方法，也可以有抽象方法
		
	}

}


abstract class AbstractKey2 {//
	
	
	public void func() {//③抽象类也可以没有抽象方法
		
	}

}

class TestAbstractKey{
	public static void main(String[] args) {
		//⑤抽象类不能被实例化
		//AbstractKey abstractKey = new AbstractKey();
	}
}

class T extends AbstractKey{
//⑤抽象类只能被继承，一个类继承抽象类，必须重写抽象类中的所有的抽象方法。
	@Override
	public void method() {
		// TODO Auto-generated method stub
		
	}
	
}




