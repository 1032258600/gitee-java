package threekeywords;


//①static修饰变量：表示静态变量，在内存只有一份拷贝。
//不依赖类的实例，被类的所有实例所共享。
//直接使用 类名.静态变量名 调用
public class StaticKey {

	static int a;
	int b;

	
	//②static修饰方法：表示静态方法，静态方法中只能调用静态变量和静态方法。
	//普通方法中可以调用静态变量和静态方法。直接使用 类名.静态方法名() 调用
	public void ff() {

		System.out.println(StaticKey.a);
		StaticKey.f();
	}

	public void tt() {

	}

	public static void f() {

	}

	static {
		f();
	}
	
	public static void t() {
		
		System.out.println(StaticKey.a);
		//System.out.println(b);
		StaticKey.f();
		// ff();
	}
	
	
	public static void main(String[] args) {
		
		StaticKey staticKey = new StaticKey();
		staticKey.ff();
		StaticKey.t();
	}
}
