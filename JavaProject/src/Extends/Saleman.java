package Extends;

public class Saleman extends Staff {

	private double deductRate;
	private double personAmount;

	public Saleman(String num, String name, Double rateOfAttend, Double basicSal, Double prize, double deductRate,
			double personAmount) {
		super(num, name, rateOfAttend, basicSal, prize);
		this.setDeductRate(deductRate);
		this.setPersonAmount(personAmount);
	}
	
	public double getDeductRate() {
		return deductRate;
	}

	public void setDeductRate(double deductRate) {
		this.deductRate = deductRate;
	}

	public double getPersonAmount() {
		return personAmount;
	}

	public void setPersonAmount(double personAmount) {
		this.personAmount = personAmount;
	}
	

}
