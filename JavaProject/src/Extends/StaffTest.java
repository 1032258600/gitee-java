package Extends;

import Encapsulation.SalaryCalc;

public class StaffTest {

	public StaffTest() {
	}

	public static void main(String[] args) {
		SalaryCalc salaryCalc = new SalaryCalc();
		Saleman saleman = new Saleman("101101", "LD", 0.88, 1200.0, 800.0, 0.0, 10000.0);
		Manager manager = new Manager("101102", "NXG", 0.9, 2500, 1000, 0.1, 20000);
		System.out.println("员工的基本信息：");
		System.out.println("编号 \t" + "姓名\t"+ "出勤率\t"+ "基本工资\t"+ "奖金\t");
		System.out.println(saleman.getNum()+"\t" + saleman.getName()+"\t" + saleman.getRateOfAttend()+ "\t" +saleman.getBasicSal()+ "\t" +saleman.getPrize()+"\t" );
		
		System.out.println("提成比例：" + saleman.getDeductRate());
		System.out.println("个人销售额：" + saleman.getPersonAmount());
		System.out.println("销售员的实发工资："+salaryCalc.pay(saleman));
		
		
		System.out.println("员工的基本信息：");
		System.out.println("编号 \t" + "姓名\t"+ "出勤率\t"+ "基本工资\t"+ "奖金\t");
		System.out.println(manager.getNum()+"\t" + manager.getName()+"\t" + manager.getRateOfAttend()+ "\t" +manager.getBasicSal()+ "\t" +manager.getPrize()+"\t" );
		
		System.out.println("提成比例：" + manager.getTotalDeductRate());
		System.out.println("总销售额：" + manager.getTotalAmount());
		System.out.println("经理的实发工资："+salaryCalc.pay(manager));
	}

}
