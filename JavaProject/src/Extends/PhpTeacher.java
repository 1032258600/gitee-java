package Extends;

public class PhpTeacher extends Teacher {

	public PhpTeacher() {
	}
	
	public PhpTeacher(String teacherName) {
		super(teacherName);
	}

	@Override
	public String teach() {
		return "Php老师上课方式";
	}

	@Override
	public String toString() {
		return "PhpTeacher [toString()=" + super.toString() + "]";
	}
	
	
}
