package Extends;

public class Petcat extends Pet {

	public Petcat(String name, String gender, int exp, int level, String food, String coatstyle) {
		super(name, gender, exp, level, food);
		this.coatstyle = coatstyle;
	}

	private String coatstyle;

	public void flying() {
		System.out.println("飞行的方法");
	}

	public String getCoatstyle() {
		return coatstyle;
	}

	@Override
	public String toString() {
		return "Petcat [coatstyle=" + coatstyle + ", toString()=" + super.toString() + "]";
	}

}
