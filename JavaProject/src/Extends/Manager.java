package Extends;

public class Manager extends Staff {

	private double totalDeductRate;
	private double totalAmount;

	public Manager(String num, String name, double rateOfAttend, double basicSal, double prize, double totalDeductRate,
			double totalAmount) {
		super(num, name, rateOfAttend, basicSal, prize);

		this.setTotalDeductRate(totalDeductRate);
		this.setTotalAmount(totalAmount);
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalDeductRate() {
		return totalDeductRate;
	}

	public void setTotalDeductRate(double totalDeductRate) {
		this.totalDeductRate = totalDeductRate;
	}

}
