package Extends;

public class TeachTest {

	public static void main(String[] args) {
		Class classObject = new Class("开发1：");
		Teacher javaTeacher = new JavaTeacher("陈老师");

		System.out.println(classObject.teachStyle(javaTeacher));

		Class classObject2 = new Class("开发2：");
		Teacher phpTeacher = new PhpTeacher("只老师");

		System.out.println(classObject2.teachStyle(phpTeacher));
	}
}
