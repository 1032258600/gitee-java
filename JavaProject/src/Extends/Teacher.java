package Extends;

public class Teacher {

	private String teacherName;
	public Teacher() {
	}
	
	public Teacher(String teacherName) {
		super();
		this.teacherName = teacherName;
	}

	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String teach(){
		return "老师上课方式";
	}

	@Override
	public String toString() {
		return "Teacher [teacherName=" + teacherName + "]";
	}
	
	
}
