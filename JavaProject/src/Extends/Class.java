package Extends;

public class Class {

	private String className;
	public Class() {
		
	}
	
	public Class(String className) {
		
		this.className = className;
	}

	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	
	

	@Override
	public String toString() {
		return "Class [className=" + className + "]";
	}

	public String teachStyle(Teacher teacher) {
		return this.getClassName() + teacher.getTeacherName() +"在" + teacher.teach() + teacher.toString() + this.toString();
	}
}
