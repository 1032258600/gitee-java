package Extends;

public class Pet {

	private String name;
	private String gender;
	private String food;
	private int exp;
	private int level;

	public Pet(String name, String gender, int exp, int level, String food) {
		super();
		this.name = name;
		this.gender = gender;
		this.exp = exp;
		this.level = level;
		this.food = food;
	}

	public void moveForMaster() {
		System.out.println("跟随主人的方法");
	}

	public String eatFood(Pet pet) {
		return pet.getFood();
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public int getExp() {
		return exp;
	}

	public int getLevel() {
		return level;
	}

	public String getFood() {
		return food;
	}

	@Override
	public String toString() {
		return "Pet [name=" + name + ", gender=" + gender + ", food=" + food + ", exp=" + exp + ", level=" + level
				+ "]";
	}

}
