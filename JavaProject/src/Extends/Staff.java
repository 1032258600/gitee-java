package Extends;

public class Staff {

	private String num;
	private String name;
	private Double rateOfAttend;
	private Double basicSal;
	private Double prize;

	
	
	public Staff() {
		
	}

	public Staff(String num, String name, Double rateOfAttend, Double basicSal, Double prize) {
		this.num = num;
		this.name = name;
		this.rateOfAttend = rateOfAttend;
		this.basicSal = basicSal;
		this.prize = prize;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getRateOfAttend() {
		return rateOfAttend;
	}

	public void setRateOfAttend(Double rateOfAttend) {
		this.rateOfAttend = rateOfAttend;
	}

	public Double getBasicSal() {
		return basicSal;
	}

	public void setBasicSal(Double basicSal) {
		this.basicSal = basicSal;
	}

	public Double getPrize() {
		return prize;
	}

	public void setPrize(Double prize) {
		this.prize = prize;
	}

	public double basicPay() {
		return this.getBasicSal() + this.getPrize() * this.getRateOfAttend();
	}
	
	
}
