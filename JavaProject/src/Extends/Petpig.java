package Extends;

import java.awt.Color;

public class Petpig extends Pet {

	public Petpig(String name, String gender, int exp, int level, String food, Color haircolor) {
		super(name, gender, exp, level, food);

		this.haircolor = haircolor;
	}

	private Color haircolor;

	public void rolling() {
		System.out.println("滚动的方法");
	}

	public Color getHaircolor() {
		return haircolor;
	}

	@Override
	public void moveForMaster() {
		System.out.println("鼻涕猪跟随主人的重写方法");
	}

	@Override
	public String toString() {
		return "Petpig [haircolor=" + haircolor + ", toString()=" + super.toString() + "]";
	}

}
