package singleton;

/**
 * 饿汉模式
 * 
 * @author Administrator
 *
 */
public class HungrySingleton {

	// 私有化构造方法
	private HungrySingleton() {

	}

	// 类的实例的静态成员变量
	private static HungrySingleton singleton = new HungrySingleton();

	// 返回单例类对象的公共静态方法
	public static HungrySingleton getSingleton() {

		return singleton;
	}
}
