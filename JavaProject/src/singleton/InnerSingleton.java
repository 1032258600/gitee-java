package singleton;

/**
 * 静态内部类
 * 
 * @author Administrator
 *
 */
public class InnerSingleton {

	// 私有化构造方法
	private InnerSingleton() {

	}

	// 静态内部类
	private static class SingletonHolder {
		// 类的实例的静态成员变量

		private static InnerSingleton singleton3 = new InnerSingleton();
	}

	// 返回单例类对象的公共静态方法

	public static InnerSingleton getSingleton() {

		return SingletonHolder.singleton3;
	}
}
