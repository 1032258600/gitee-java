package singleton;

/**
 * 懒汉模式
 * 
 * @author Administrator
 *
 */
public class LazySingleton {

	// 私有化构造方法
	private LazySingleton() {

	}

	// 类的实例的静态成员变量
	private static LazySingleton singleton;

	// 返回单例类对象的公共静态方法
	public static LazySingleton getSingleton() {
		if (singleton == null) {
			singleton = new LazySingleton();
		}
		return singleton;
	}
}
