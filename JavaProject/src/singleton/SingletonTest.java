package singleton;

public class SingletonTest {

	public static void main(String[] args) {

		HungrySingleton hungrySingleton = HungrySingleton.getSingleton();
		HungrySingleton hungrySingleton2 = HungrySingleton.getSingleton();
		HungrySingleton hungrySingleton3 = HungrySingleton.getSingleton();

		System.out.println(hungrySingleton.hashCode());
		System.out.println(hungrySingleton2.hashCode());
		System.out.println(hungrySingleton3.hashCode());

		LazySingleton lazySingleton = LazySingleton.getSingleton();
		LazySingleton lazySingleton2 = LazySingleton.getSingleton();
		LazySingleton lazySingleton3 = LazySingleton.getSingleton();

		System.out.println(lazySingleton.hashCode());
		System.out.println(lazySingleton2.hashCode());
		System.out.println(lazySingleton3.hashCode());

		InnerSingleton innerSingleton = InnerSingleton.getSingleton();
		InnerSingleton innerSingleton2 = InnerSingleton.getSingleton();
		InnerSingleton innerSingleton3 = InnerSingleton.getSingleton();

		System.out.println(innerSingleton.hashCode());
		System.out.println(innerSingleton2.hashCode());
		System.out.println(innerSingleton3.hashCode());
	}
}
