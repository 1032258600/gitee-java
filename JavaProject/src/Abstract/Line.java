package Abstract;

import com.sun.prism.paint.Color;

public class Line extends Draw {

	private float xAxis;
	private float yAxis;
	
	public Line(float x, float y, Color color, float xAxis,float yAxis) {
		super(x, y, color);
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}

	@Override
	public void drawing() {
		
		System.out.println(super.toString() + this.toString());
		
	}

	@Override
	public String toString() {
		return "Line [xAxis=" + xAxis + ", yAxis=" + yAxis + "]";
	}
	
	

}
