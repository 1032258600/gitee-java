package Abstract;

import com.sun.prism.paint.Color;

public class DrawTest {

	public void drawTest(Draw dw) {
		dw.drawing();
	}

	public static void main(String[] args) {

		DrawTest drawTest = new DrawTest();
		Draw dw = new Line(0, 0, Color.RED, 200, 200);
		drawTest.drawTest(dw);

	}

}
