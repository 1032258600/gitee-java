package Abstract;

import com.sun.prism.paint.Color;

public abstract class Draw {
	
	private float x;
	private float y;
	private Color color;
	
	
	
	public Draw(float x, float y, Color color) {
		super();
		this.x = x;
		this.y = y;
		this.color = color;
	}



	public abstract void drawing();



	@Override
	public String toString() {
		return "Draw [x=" + x + ", y=" + y + ", color=" + color + "]";
	}
	
	

}
