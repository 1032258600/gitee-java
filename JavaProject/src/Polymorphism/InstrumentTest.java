/**
 * 
 */
package Polymorphism;


/**
 * 测试类
 * 
 * @author Administrator
 *
 */
public class InstrumentTest {

	/**
	 * 乐器弹奏测试
	 * 
	 * @param instrument
	 * @return
	 */
	public String testPlay(Instrument instrument) {
		return instrument.play();
	}

	/**
	 * 主函数
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		InstrumentTest instrumentTest = new InstrumentTest();

		Instrument piano = new Piano();
		Instrument violin = new Violin();

		System.out.println(instrumentTest.testPlay(piano));
		System.out.println(instrumentTest.testPlay(violin));
	}

}
