/**
 * 
 */
package Polymorphism;

/**
 * @author Administrator
 * 钢琴
 */
public class Piano extends Instrument {

	/**
	 * 钢琴弹奏方法
	 */
	@Override
	public String play() {
		
		return "钢琴弹奏方法";
	}
}
