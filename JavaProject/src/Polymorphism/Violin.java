/**
 * 
 */
package Polymorphism;

/**
 * 小提琴
 * @author Administrator
 *
 */
public class Violin extends Instrument {

	/**
	 * 小提琴弹奏方法
	 */
	@Override
	public String play() {
		return "小提琴弹奏方法";
	}
}
