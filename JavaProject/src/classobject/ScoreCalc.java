package classobject;

public class ScoreCalc {

	public float javaScore;
	public float csharpScore;
	public float dBScore;

	/**
	 * 计算平均成绩 平均成绩
	 * 
	 * @return 返回平均成绩
	 */
	public float average() {

		return (javaScore + csharpScore + dBScore) / 3;

	}

	/**
	 * 计算总成绩 总成绩
	 * 
	 * @return 返回总成绩
	 */
	public float total() {

		return javaScore + csharpScore + dBScore;
	}

	public static void main(String[] args) {

		ScoreCalc scoreCalc = new ScoreCalc();
		scoreCalc.javaScore = 50.5f;
		scoreCalc.csharpScore = 60;
		scoreCalc.dBScore = 90;
		float avg = scoreCalc.average();
		float scoreTotal = scoreCalc.total();
		System.out.println(avg);
		System.out.println(scoreTotal);

	}

}
