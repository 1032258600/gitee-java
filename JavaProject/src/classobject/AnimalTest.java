package classobject;

public class AnimalTest {
	
	public static void main(String[] args) {
		
		Animal cat = new Animal();
		Animal wolf = new Animal();

		
		cat.name = "猫";
		cat.color = "黑色";
		cat.weight = "1Kg";
		
		cat.showInfo();
		
		wolf.name = "狼";
		wolf.color = "黄色";
		wolf.weight = "20Kg";
		
		wolf.showInfo();
		
	}

}
