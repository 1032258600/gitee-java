package classobject;

public class Method {

	public void max() {
		int a = 10;
		int b = 20;
		if (a > b) {
			System.out.println(a);
		} else {
			System.out.println(b);
		}
	}
	
	public int max2() {
		
		int a = 10;
		int b = 20;
		if (a > b) {
			return a;
		} else {
			return b;
		}
		
		
	}
	
	public static void main(String[] args) {
		
		Method object = new Method();
		
		object.max();
		int c = object.max2();
		System.out.println(c);
	}

}
