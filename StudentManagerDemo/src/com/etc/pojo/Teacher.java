package com.etc.pojo;

public class Teacher {

	private int teacherId; //编号
	private String teacherNo;//教师工号
	private String teacherName;//教室名
	private String password;//登录密码
	
	public Teacher(int teacherId, String teacherNo, String teacherName, String password) {
		super();
		this.teacherId = teacherId;
		this.teacherNo = teacherNo;
		this.teacherName = teacherName;
		this.password = password;
	}
	public Teacher() {
		super();
	}
	public int getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherNo() {
		return teacherNo;
	}
	public void setTeacherNo(String teacherNo) {
		this.teacherNo = teacherNo;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
