package com.etc.pojo;

public class Student {
	private String studentNo; //学号
	private String studentName;//学生姓名
	private String gender;//性别
	private String birth; //出生日期
	private String password;//登录密码
	private int status;//状态 0正常 1注销
	public Student() {
		super();
		
	}
	public Student(String studentNo, String studentName, String gender, String birth, String password, int status) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.gender = gender;
		this.birth = birth;
		this.password = password;
		this.status = status;
	}
	public String getStudentNo() {
		return studentNo;
	}
	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Student [studentNo=" + studentNo + ", studentName=" + studentName + ", gender=" + gender + ", birth="
				+ birth + ", password=" + password + ", status=" + status + "]";
	}
	
}
