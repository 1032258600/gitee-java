package com.etc.pojo;
/**
 * 存储多张表的查询结果
 * 学生表 成绩表 课程表
 * @author Administrator
 *
 */
public class ScoreDetailInfo {
	// 定义查询结果的所有字段
	// 学生表
	private String studentName;//学生姓名
	private String gender;//性别
	private String birth; //出生日期
	private String password;//登录密码
	private int status;//状态 0正常 1注销
	// 成绩表
	private int scoreId; //成绩编号
	private String studentNo;//学生编号
	private int courseNo;//课程编号
	private float score;//分数
	private String pudDate;//录入时间
	// 课程表
	private String courseName;//课程名
	private float courseScore;//课程学分
	
	
	public ScoreDetailInfo() {
		super();
	}
	
	public ScoreDetailInfo( String courseName, float courseScore,float score) {
		super();
		this.score = score;
		this.courseName = courseName;
		this.courseScore = courseScore;
	}

	public ScoreDetailInfo(int scoreId, String studentNo, String studentName, String courseName, float score) {
		this.scoreId = scoreId;
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.courseName = courseName;
		this.score = score;
	}

	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getScoreId() {
		return scoreId;
	}
	public void setScoreId(int scoreId) {
		this.scoreId = scoreId;
	}
	public String getStudentNo() {
		return studentNo;
	}
	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	public int getCourseNo() {
		return courseNo;
	}
	public void setCourseNo(int courseNo) {
		this.courseNo = courseNo;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getPudDate() {
		return pudDate;
	}
	public void setPudDate(String pudDate) {
		this.pudDate = pudDate;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public float getCourseScore() {
		return courseScore;
	}
	public void setCourseScore(float courseScore) {
		this.courseScore = courseScore;
	}
	
	
}
