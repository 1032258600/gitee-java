package com.etc.pojo;

public class Course {

	private int courseNo;//课程编号
	private String courseName;//课程名
	private float courseScore;//课程学分
	
	public int getCourseNo() {
		return courseNo;
	}
	public void setCourseNo(int courseNo) {
		this.courseNo = courseNo;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public float getCourseScore() {
		return courseScore;
	}
	public void setCourseScore(float courseScore) {
		this.courseScore = courseScore;
	}
	public Course(int courseNo, String courseName, float courseScore) {
		super();
		this.courseNo = courseNo;
		this.courseName = courseName;
		this.courseScore = courseScore;
	}
	public Course() {
		super();
	}
	
}
