package com.etc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.etc.pojo.Teacher;
import com.etc.utils.DBUtil;

public class TeacherDaoImpl implements TeacherDao {

	@Override
	public Teacher queryByTeacherNoAndPassword(String teacherNo, String password) {
		Teacher teacher = null;
		ResultSet rs = DBUtil.doQuery("select * from tab_teacher where teacher_no = ? and password = ?", teacherNo, password);
		try {
			while (rs.next()) {
				int teacherId = rs.getInt("teacher_id");
				String teacherName = rs.getString("teacher_name");
				teacher = new Teacher(teacherId, teacherNo, teacherName, password);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return teacher;
	}

}
