package com.etc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.etc.pojo.Student;
import com.etc.utils.DBUtil;

public class StudentDaoImpl implements StudentDao {

	@Override
	public Student queryByStudentNoAndPassword(String studentNo, String password) {
		Student student = null;
		ResultSet rs = DBUtil.doQuery("select * from tab_student where student_no = ? and password = ?", studentNo,
				password);
		try {
			while (rs.next()) {
				String studentName = rs.getString("student_name");
				String gender = rs.getString("gender");
				String birth = rs.getString("birth");
				int status = rs.getInt("status");
				student = new Student(studentNo, studentName, gender, birth, password, status);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student;
	}

	@Override
	public List<Student> query() {
		List<Student> list = new ArrayList<>();
		ResultSet rs = DBUtil.doQuery("select * from tab_student");
		try {
			while (rs.next()) {
				String studentNo = rs.getString("student_no");
				String studentName = rs.getString("student_name");
				String password = rs.getString("password");
				String gender = rs.getString("gender");
				String birth = rs.getString("birth");
				int status = rs.getInt("status");
				list.add(new Student(studentNo, studentName, gender, birth, password, status));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Student queryByStudentNo(String studentNo) {
		Student student = null;
		ResultSet rs = DBUtil.doQuery("select * from tab_student where student_no = ?", studentNo);
		try {
			while (rs.next()) {
				String studentName = rs.getString("student_name");
				String password = rs.getString("password");
				String gender = rs.getString("gender");
				String birth = rs.getString("birth");
				int status = rs.getInt("status");
				student = new Student(studentNo, studentName, gender, birth, password, status);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return student;
	}

	@Override
	public List<Student> queryLikeByStudentName(String key) {
		List<Student> list = new ArrayList<>();
		ResultSet rs = DBUtil.doQuery("select * from tab_student where student_name like ?", "%" + key + "%");
		try {
			while (rs.next()) {
				String studentNo = rs.getString("student_no");
				String studentName = rs.getString("student_name");
				String password = rs.getString("password");
				String gender = rs.getString("gender");
				String birth = rs.getString("birth");
				int status = rs.getInt("status");
				list.add(new Student(studentNo, studentName, gender, birth, password, status));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int save(Student student) {
		return DBUtil.doUpdate("insert into tab_student values(?,?,?,?,?,?)", student.getStudentNo(), student.getStudentName(), student.getGender(),student.getBirth(),
				student.getPassword(), student.getStatus());
	}

	@Override
	public int updateByStudentNo(Student student) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updatePassword(String studentNo, String newPassword) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateStatus(String studentNo, int status) {
		// TODO Auto-generated method stub
		return 0;
	}
}
