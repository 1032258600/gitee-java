package com.etc.dao;

import java.util.List;
import java.util.Map;

import com.etc.pojo.ScoreDetailInfo;

public interface ScoreDao {
	

	/**
	 * 根据学号获取个人课程成绩列表
	 * @param studentNo 学号
	 * @return 课程成绩列表
	 */
	public List<ScoreDetailInfo> queryScoreByStudentNo(String studentNo); 
	
	/**
	 * 获取所有学生的课程成绩列表
	 * @return 课程成绩列表
	 */
	public List<ScoreDetailInfo> queryAllScore();
	
	/**
	 * 根据不同条件进行查询部分学生的课程成绩列表
	 * 学号，课程，学号+课程
	 * @return
	 */
	public List<ScoreDetailInfo> queryByCondition(Map<String, Object> params);
	
	/**
	 * 添加成绩记录
	 * @param studentNo 学号
	 * @param courseNo 课程编号
	 * @param score 分数
	 * @return
	 */
	public int saveScore(String studentNo, int courseNo, float score );
}
