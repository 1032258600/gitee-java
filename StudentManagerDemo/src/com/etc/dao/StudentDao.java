package com.etc.dao;

import java.util.List;

import com.etc.pojo.Student;

public interface StudentDao {

	/**
	 * 根据学号和密码查询
	 * @param studentNo 学号
	 * @param password 密码
	 * @return 学生对象
	 */
	public Student queryByStudentNoAndPassword(String studentNo, String password);
	/**
	 * 查询所有学生
	 * @return 学生列表
	 */
	public List<Student> query();
	
	/**
	 * 根据学号查询
	 * @param studentNo 学号
	 * @return 学生对象
	 */
	public Student queryByStudentNo(String studentNo);
	/**
	 * 根据学生姓名模糊查询
	 * @return 学生列表
	 */
	public List<Student> queryLikeByStudentName(String key);
	/**
	 * 填写一条学生记录
	 * @param student
	 * @return
	 */
	public int save(Student student);
	
	/**
	 * 更新学生信息
	 */
	public int updateByStudentNo(Student student);
	
	/**
	 * 更新学生密码
	 */
	public int updatePassword(String studentNo, String newPassword);
	
	/**
	 * 更新学生状态
	 */
	public int updateStatus(String studentNo,int status);
}
