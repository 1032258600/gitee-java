package com.etc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.etc.pojo.Course;
import com.etc.utils.DBUtil;

public class CourseDaoImpl implements CourseDao{

	@Override
	public List<Course> queryAllCourse() {
		List<Course> list = new ArrayList<>();
		// 获取课程表信息
		ResultSet res = DBUtil.doQuery("select course_no, course_name,course_score from tab_course");
		try {
			while (res.next()) {
				int courseNo = res.getInt("course_no");
				String courseName = res.getString("course_name");
				float courseScore = res.getFloat("course_score");
				list.add(new Course(courseNo, courseName, courseScore));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
