package com.etc.dao;

import com.etc.pojo.Teacher;

public interface TeacherDao {
	/**
	 * 根据职工号和密码查询
	 * @param teacherNo 职工号
	 * @param password 密码
	 * @return 老师对象
	 */
	public Teacher queryByTeacherNoAndPassword(String teacherNo, String password);
}
