package com.etc.dao;

import java.util.List;

import com.etc.pojo.Course;

public interface CourseDao {

	// 查询所有的课程
	List<Course> queryAllCourse();
}
