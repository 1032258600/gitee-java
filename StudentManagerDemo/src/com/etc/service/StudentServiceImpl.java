package com.etc.service;

import java.util.List;

import com.etc.dao.StudentDao;
import com.etc.dao.StudentDaoImpl;
import com.etc.pojo.Student;

public class StudentServiceImpl implements StudentService{

	private StudentDao dao = new StudentDaoImpl();
	@Override
	public Student login(String studentNo, String password) {
		
		return dao.queryByStudentNoAndPassword(studentNo, password);
	}
	@Override
	public List<Student> getAll() {
		return dao.query();
	}
	@Override
	public Student getByStudentNo(String studentNo) {
		// TODO Auto-generated method stub
		return dao.queryByStudentNo(studentNo);
	}
	@Override
	public List<Student> getLikeByStudentName(String key) {
		// TODO Auto-generated method stub
		return dao.queryLikeByStudentName(key);
	}
	@Override
	public int add(Student student) {
		// TODO Auto-generated method stub
		return dao.save(student);
	}
	@Override
	public int modifyByStudentNo(Student student) {
		// TODO Auto-generated method stub
		return dao.updateByStudentNo(student);
	}
	@Override
	public int modifyPassword(String studentNo, String newPassword) {
		// TODO Auto-generated method stub
		return dao.updatePassword(studentNo, newPassword);
	}
	@Override
	public int modifyStatus(String studentNo, int status) {
		// TODO Auto-generated method stub
		return dao.updateStatus(studentNo, status);
	}

}
