package com.etc.service;

import com.etc.pojo.Teacher;

public interface TeacherService {

	/**
	 * 登录
	 * @param teacherNo 职工号
	 * @param password 密码
	 * @return 学生对象
	 */
	public Teacher login(String teacherNo,String password);
}
