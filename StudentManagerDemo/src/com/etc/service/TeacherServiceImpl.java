package com.etc.service;

import com.etc.dao.TeacherDao;
import com.etc.dao.TeacherDaoImpl;
import com.etc.pojo.Teacher;

public class TeacherServiceImpl implements TeacherService{

	private TeacherDao dao= new TeacherDaoImpl();
	@Override
	public Teacher login(String teacherNo, String password) {
	
		return dao.queryByTeacherNoAndPassword(teacherNo, password);
	}

}
