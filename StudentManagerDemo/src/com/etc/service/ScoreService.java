package com.etc.service;

import java.util.List;
import java.util.Map;

import com.etc.pojo.ScoreDetailInfo;

public interface ScoreService {

	/**
	 * 根据学号获取个人课程成绩列表
	 * @param studentNo 学号
	 * @return 课程成绩列表
	 */
	public List<ScoreDetailInfo> getScoreByStudentNo(String studentNo);
	
	/**
	 * 获取所有学生的课程成绩列表
	 * @return 课程成绩列表
	 */
	public List<ScoreDetailInfo> getAllScore();
	
	/**
	 * 根据不同条件进行查询部分学生的课程成绩列表业务
	 * 学号，课程，学号+课程
	 * @return
	 */
	public List<ScoreDetailInfo> getByCondition(Map<String, Object> params);
	
	/**
	 * 录入成绩记录
	 * @param studentNo 学号
	 * @param courseNo 课程编号
	 * @param score 分数
	 * @return
	 */
	public int addScore(String studentNo, int courseNo, float score );

}
