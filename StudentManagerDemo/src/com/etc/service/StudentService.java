package com.etc.service;

import java.util.List;

import com.etc.pojo.Student;

public interface StudentService {

	/**
	 * 登录
	 * @param studentNo 学号
	 * @param password 密码
	 * @return 学生对象
	 */
	public Student login(String studentNo,String password);
	
	/**
	 * 获取所有学生
	 * @return 学生列表
	 */
	public List<Student> getAll();
	
	/**
	 * 根据学号获取学生信息
	 * @param studentNo 学号
	 * @return 学生对象
	 */
	public Student getByStudentNo(String studentNo);
	/**
	 * 根据学生姓名模糊查询获取学生列表
	 * @return 学生列表
	 */
	public List<Student> getLikeByStudentName(String key);
	
	/**
	 * 新增学生业务
	 * @param student
	 * @return
	 */
	public int add(Student student);
	
	/**
	 * 更新学生信息业务
	 */
	public int modifyByStudentNo(Student student);
	
	/**
	 * 更新学生密码业务
	 */
	public int modifyPassword(String studentNo, String newPassword);
	
	/**
	 * 更新学生状态业务
	 */
	public int modifyStatus(String studentNo,int status);
}
