package com.etc.service;

import java.util.List;

import com.etc.pojo.Course;

public interface CourseService {

	//  查询所有的课程
	List<Course> getAllCourse();
}
