package com.etc.service;

import java.util.List;
import java.util.Map;

import com.etc.dao.ScoreDao;
import com.etc.dao.ScoreDaoImpl;
import com.etc.pojo.ScoreDetailInfo;

public class ScoreServiceImpl implements ScoreService{
	// 调用dao
	private ScoreDao scoreDao = new ScoreDaoImpl();

	@Override
	public List<ScoreDetailInfo> getScoreByStudentNo(String studentNo) {
		// TODO Auto-generated method stub
		return scoreDao.queryScoreByStudentNo(studentNo);
	}

	@Override
	public List<ScoreDetailInfo> getAllScore() {
		// TODO Auto-generated method stub
		return scoreDao.queryAllScore();
	}

	@Override
	public List<ScoreDetailInfo> getByCondition(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return scoreDao.queryByCondition(params);
	}

	@Override
	public int addScore(String studentNo, int courseNo, float score) {
		// TODO Auto-generated method stub
		return scoreDao.saveScore(studentNo, courseNo, score);
	}

	

}
