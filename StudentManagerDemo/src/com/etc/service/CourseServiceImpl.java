package com.etc.service;

import java.util.List;

import com.etc.dao.CourseDao;
import com.etc.dao.CourseDaoImpl;
import com.etc.pojo.Course;

public class CourseServiceImpl implements CourseService{

	private CourseDao courseDao =new CourseDaoImpl();
	@Override
	public List<Course> getAllCourse() {
		return courseDao.queryAllCourse();
	}

}
