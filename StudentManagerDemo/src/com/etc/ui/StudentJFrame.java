package com.etc.ui;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.etc.pojo.ScoreDetailInfo;
import com.etc.pojo.Student;
import com.etc.service.ScoreServiceImpl;
import com.etc.service.StudentServiceImpl;

import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class StudentJFrame {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentJFrame window = new StudentJFrame(new Student("201901", "xxxx", "男", null, null, 0));
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StudentJFrame(Student student) {
		initialize(student);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Student student) {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("学生个人页面");
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("学    号：");
		label.setBounds(20, 23, 71, 15);
		frame.getContentPane().add(label);
		
		JLabel label2 = new JLabel("学生姓名：");
		label2.setBounds(20, 48, 94, 15);
		frame.getContentPane().add(label2);
		
		JLabel lab_no = new JLabel("New label");
		lab_no.setBounds(101, 23, 54, 15);
		frame.getContentPane().add(lab_no);
		
		JLabel lab_name = new JLabel("New label");
		lab_name.setBounds(101, 48, 54, 15);
		frame.getContentPane().add(lab_name);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(68, 116, 295, 106);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_2 = new JLabel("个人课程成绩列表");
		lblNewLabel_2.setBounds(161, 86, 136, 15);
		frame.getContentPane().add(lblNewLabel_2);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(317, 0, 105, 21);
		frame.getContentPane().add(menuBar);
		
		JMenu menu = new JMenu("个人中心");
		menuBar.add(menu);
		
		JMenuItem menu_updatePwd = new JMenuItem("修改密码");
		menu.add(menu_updatePwd);
		
		JMenuItem menu_logout = new JMenuItem("退出登录");
		menu.add(menu_logout);
		frame.setVisible(true);
		
		// 功能1：显示学生学号和学生姓名
		lab_no.setText(student.getStudentNo());
		lab_name.setText(student.getStudentName());
		
		// 功能2： 对两个菜单项分别进行点击事件 自行拓展
		// 2.1 修改密码--调用修改业务
		// 2.2 退出登录--回到登录界面
		
		// 功能3：显示个人课程成绩列表
		// 填充表格数据
		// 查询学生个人课程成绩业务
		List<ScoreDetailInfo> list = new ScoreServiceImpl().getScoreByStudentNo(student.getStudentNo());
		Object[][] data = new Object[list.size()][3];
		// 集合的数据填充到二维数组
		for (int i = 0; i < list.size(); i++) {
			ScoreDetailInfo scoreDetailInfo = list.get(i);
			data[i][0] = scoreDetailInfo.getCourseName();
			data[i][1] = scoreDetailInfo.getCourseScore();
			data[i][2] = scoreDetailInfo.getScore();
		}
		table.setModel(new DefaultTableModel(
				data,
				new String[] {
					"课程名", "课程学分", "成绩"
				}
		));
	}
}
