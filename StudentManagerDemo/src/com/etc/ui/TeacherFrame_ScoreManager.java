package com.etc.ui;

import java.awt.EventQueue;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import com.etc.pojo.Course;
import com.etc.pojo.ScoreDetailInfo;
import com.etc.pojo.Student;
import com.etc.service.CourseService;
import com.etc.service.CourseServiceImpl;
import com.etc.service.ScoreService;
import com.etc.service.ScoreServiceImpl;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class TeacherFrame_ScoreManager {

	private JFrame frame;
	private JTextField stuNo_txt;
	private JTextField add_stuNo_txt;
	private JTextField add_score_txt;
	private JScrollPane scrollPane;
	private JTable table;
	private ScoreService scoreService = new ScoreServiceImpl();
	private CourseService courseService = new CourseServiceImpl();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherFrame_ScoreManager window = new TeacherFrame_ScoreManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TeacherFrame_ScoreManager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 574, 477);
		//窗口默认关闭操作  ， 修改成点击关闭按钮只是销毁当前窗口，不会关闭整个程序
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("教师成绩管理窗口");
		frame.getContentPane().setLayout(null);

		JLabel label = new JLabel("学号：");
		label.setBounds(25, 24, 54, 15);
		frame.getContentPane().add(label);

		stuNo_txt = new JTextField();
		stuNo_txt.setBounds(71, 21, 66, 21);
		frame.getContentPane().add(stuNo_txt);
		stuNo_txt.setColumns(10);

		

		JLabel label_2 = new JLabel("课程：");
		label_2.setBounds(147, 24, 44, 15);
		frame.getContentPane().add(label_2);

		JButton btn_search = new JButton("查询");
		
		btn_search.setBounds(430, 21, 74, 23);
		frame.getContentPane().add(btn_search);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 87, 498, 133);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(25, 230, 273, 193);
		frame.getContentPane().add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("录入成绩", null, panel, null);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("学号：");
		lblNewLabel.setBounds(63, 10, 54, 15);
		panel.add(lblNewLabel);

		add_stuNo_txt = new JTextField();
		add_stuNo_txt.setBounds(101, 7, 80, 21);
		panel.add(add_stuNo_txt);
		add_stuNo_txt.setColumns(10);

		JLabel label_3 = new JLabel("课程：");
		label_3.setBounds(63, 45, 54, 15);
		panel.add(label_3);

		JLabel label_4 = new JLabel("成绩：");
		label_4.setBounds(63, 82, 54, 15);
		panel.add(label_4);

		add_score_txt = new JTextField();
		add_score_txt.setBounds(101, 79, 80, 21);
		panel.add(add_score_txt);
		add_score_txt.setColumns(10);

		JButton btn_addScore = new JButton("录入");
		
		btn_addScore.setBounds(87, 131, 66, 23);
		panel.add(btn_addScore);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(101, 42, 80, 21);
		panel.add(comboBox_1);

	
		JButton btn_clear = new JButton("清除搜索条件");
	
		btn_clear.setBounds(304, 21, 105, 23);
		frame.getContentPane().add(btn_clear);
		
		JComboBox comboBox = new JComboBox();
		
		comboBox.setBounds(183, 21, 83, 21);
		frame.getContentPane().add(comboBox);
		frame.setVisible(true);
		
		// 功能1：页面一旦进入，就需要显示所有学生的课程成绩
		List<ScoreDetailInfo> list = scoreService.getAllScore();
		showData(list);
		
		// 功能2：显示课程列表
		// 需要获取课程列表项
		List<Course> allCourse = courseService.getAllCourse();
		String[] courseItem =  new String[allCourse.size()+1];
		courseItem[0] = "全部课程";
		// 对一维数组赋值
		for (int i = 0; i < allCourse.size(); i++) {
			courseItem[i+1] = allCourse.get(i).getCourseName();
		}
		comboBox.setModel(new DefaultComboBoxModel(courseItem));
		comboBox_1.setModel(new DefaultComboBoxModel(courseItem));
		
		// 功能3：查询功能
		// 3种情况：按照学号查询、按照课程查询、按照学号+课程
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1 获取用户输入的学号
				String studentNo = stuNo_txt.getText().trim();
				// 2 获取用户选中的课程编号
				String courseNo = "";
				// 如何知道当前选中的是哪一个下拉菜单项目
				int selectedIndex = comboBox.getSelectedIndex(); 
				if (selectedIndex==0) {// 选中"全部课程"
					courseNo ="";
				}else {
					// 从课程列表集合中获取对应的对象
					Course course = allCourse.get(selectedIndex-1);
					courseNo = course.getCourseNo()+"";
				}
				// 调用查询业务;
				Map<String, Object> params = new HashMap<>();
				params.put("studentNo", studentNo);
				params.put("courseNo", courseNo);
				List<ScoreDetailInfo> byCondition = scoreService.getByCondition(params);
				// 填充表达数据
				showData(byCondition);
			}
		});
		// 功能4：清除功能
		btn_clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1. 清除学号输入框内容
				stuNo_txt.setText("");
				// 2. 恢复下拉框默认选项
				comboBox.setSelectedIndex(0);
			}
		});
		
		// 功能5：录入成绩
		btn_addScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1获取学号(先判断学号是否存在)
				// 2获取选择的课程编号 (判断该课程编号是否已经有对应的成绩)
				// 3获取输入成绩
				// 4调用添加成绩的方法
			}
		});
		// 显示
		frame.setVisible(true);
	}
	
	/**
	 * 为表格填充数据
	 */
	private void showData(List<ScoreDetailInfo> list) {
		Object[][] data = new Object[list.size()][5];
		for (int i = 0; i < list.size(); i++) {
			ScoreDetailInfo info = list.get(i);
			data[i][0] = info.getScoreId();
			data[i][1] = info.getStudentNo();
			data[i][2] = info.getStudentName();
			data[i][3] = info.getCourseName();
			data[i][4] = info.getScore();
		}

		table.setModel(new DefaultTableModel(data, new String[] { "成绩编号", "学号", "学生姓名", "课程名", "成绩"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		scrollPane.setViewportView(table);
	}
}
