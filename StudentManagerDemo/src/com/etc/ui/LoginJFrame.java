package com.etc.ui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.etc.pojo.Student;
import com.etc.pojo.Teacher;
import com.etc.service.StudentService;
import com.etc.service.StudentServiceImpl;
import com.etc.service.TeacherService;
import com.etc.service.TeacherServiceImpl;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginJFrame {

	private JFrame frame;
	private JTextField text_no;
	private JPasswordField password;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton radio_student,radio_teacher;
	private StudentService studentService = new StudentServiceImpl();
	private TeacherService teacherService = new TeacherServiceImpl();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginJFrame window = new LoginJFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginJFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("登录页面");
		JLabel label = new JLabel("学生成绩管理系统");
		label.setBounds(156, 21, 138, 15);
		frame.getContentPane().add(label);

		JLabel label_1 = new JLabel("学号/工号：");
		label_1.setBounds(69, 68, 90, 15);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("密码：");
		label_2.setBounds(69, 109, 54, 15);
		frame.getContentPane().add(label_2);

		text_no = new JTextField();
		text_no.setBounds(145, 65, 130, 21);
		frame.getContentPane().add(text_no);
		text_no.setColumns(10);

		password = new JPasswordField();
		password.setBounds(145, 106, 130, 21);
		frame.getContentPane().add(password);

		JButton btn_login = new JButton("登录");
		// 登录事件
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 获取用户输入的学号/工号
				String no = text_no.getText();
				// 获取用户输入的密码
				char[] c = password.getPassword();
				String password = new String(c);
				// 判断单选框被选中的状态
				if (radio_student.isSelected()) { // 学生被选中
					// 调用学生登录业务
					Student student = studentService.login(no, password);
					if (student!=null) { // 登录成功
						JOptionPane.showMessageDialog(null,"学生登录成功");
						// 跳转页面--前往学生个人主页，并且将学生对象信息传递到个人主页
						StudentJFrame window = new StudentJFrame(student);
						// 当前登录页面需要关闭
						frame.dispose();
					}else {// 登录是吧
						JOptionPane.showMessageDialog(null,"学号或密码错误");
					}
				}else if(radio_teacher.isSelected()) {// 教师被选中
					// 调用老师登录业务
					// 调用学生登录业务
					Teacher teacher = teacherService.login(no, password);
					if (teacher!=null) { // 登录成功
						JOptionPane.showMessageDialog(null,"教师登录成功");
						// 跳转页面--前往老师管理页面
						TeacherFrame_StudentManager window = new TeacherFrame_StudentManager();
						// 当前登录页面需要关闭
						frame.dispose();
					}else {// 登录是吧
						JOptionPane.showMessageDialog(null,"职工号或密码错误");
					}
				}
			}
		});
		btn_login.setBounds(147, 193, 93, 23);
		frame.getContentPane().add(btn_login);

		JLabel label_3 = new JLabel("角色：");
		label_3.setBounds(69, 146, 54, 15);
		frame.getContentPane().add(label_3);

		radio_student = new JRadioButton("学生");
		radio_student.setSelected(true);
		buttonGroup.add(radio_student);
		radio_student.setBounds(141, 150, 63, 23);
		frame.getContentPane().add(radio_student);

		radio_teacher = new JRadioButton("教师");
		buttonGroup.add(radio_teacher);
		radio_teacher.setBounds(223, 150, 121, 23);
		frame.getContentPane().add(radio_teacher);
	}
}
