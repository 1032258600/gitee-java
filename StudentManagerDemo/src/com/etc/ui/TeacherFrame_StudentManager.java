package com.etc.ui;

import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import com.etc.pojo.Student;
import com.etc.service.StudentService;
import com.etc.service.StudentServiceImpl;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TeacherFrame_StudentManager {

	private JFrame frame;
	private JTextField txt_noSearch;
	private JLabel label_1;
	private JTextField txt_nameKey;
	private JButton btn_search;
	private JLabel label_2;
	private JTextField add_no;
	private JLabel label_3;
	private JTextField add_name;
	private JLabel label_4;
	private JRadioButton add_man_radio;
	private JRadioButton add_woman_radio;
	private JLabel label_5;
	private JTextField add_birth;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel add_pwd;
	private JButton add_btn;
	private JLabel label_8;
	private JLabel update_no;
	private JLabel label_9;
	private JTextField update_name;
	private JLabel label_10;
	private JRadioButton up_man_radio;
	private JRadioButton up_woman_radio;
	private JLabel label_11;
	private JTextField update_birth;
	private JLabel label_12;
	private JTextField update_pwd;
	private JButton update_btn;
	private JButton cancel_btn;

	// 定义学生业务层
	private StudentService studentService = new StudentServiceImpl();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JScrollPane scrollPane;
	private JTable table;
	private boolean flag = true; // 新增学号通过;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TeacherFrame_StudentManager window = new TeacherFrame_StudentManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TeacherFrame_StudentManager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 645, 491);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 窗口显示在页面中间
		frame.setLocationRelativeTo(null);
		// 窗口不允许调整大小
		frame.setResizable(false);
		// 设置窗口名称
		frame.setTitle("教师管理主页面-学生管理");
		frame.getContentPane().setLayout(null);

		JLabel label = new JLabel("按学号:");
		label.setBounds(51, 29, 90, 15);
		frame.getContentPane().add(label);

		txt_noSearch = new JTextField();
		txt_noSearch.setBounds(116, 26, 66, 21);
		frame.getContentPane().add(txt_noSearch);
		txt_noSearch.setColumns(10);

		label_1 = new JLabel("按姓名关键字：");
		label_1.setBounds(216, 29, 90, 15);
		frame.getContentPane().add(label_1);

		txt_nameKey = new JTextField();
		txt_nameKey.setBounds(299, 26, 66, 21);
		frame.getContentPane().add(txt_nameKey);
		txt_nameKey.setColumns(10);

		btn_search = new JButton("查询");
		btn_search.setBounds(400, 25, 93, 23);
		frame.getContentPane().add(btn_search);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(51, 231, 524, 158);
		frame.getContentPane().add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("新增学生", null, panel, null);
		panel.setLayout(null);

		label_2 = new JLabel("学号：");
		label_2.setBounds(10, 21, 54, 15);
		panel.add(label_2);

		add_no = new JTextField();

		add_no.setBounds(51, 18, 66, 21);
		panel.add(add_no);
		add_no.setColumns(10);

		label_3 = new JLabel("学生姓名：");
		label_3.setBounds(154, 21, 62, 15);
		panel.add(label_3);

		add_name = new JTextField();
		add_name.setBounds(218, 18, 66, 21);
		panel.add(add_name);
		add_name.setColumns(10);

		label_4 = new JLabel("性别：");
		label_4.setBounds(324, 21, 54, 15);
		panel.add(label_4);

		add_man_radio = new JRadioButton("男");
		buttonGroup.add(add_man_radio);
		add_man_radio.setSelected(true);
		add_man_radio.setBounds(368, 17, 54, 23);
		panel.add(add_man_radio);

		add_woman_radio = new JRadioButton("女");
		buttonGroup.add(add_woman_radio);
		add_woman_radio.setBounds(445, 17, 62, 23);
		panel.add(add_woman_radio);

		label_5 = new JLabel("出生日期：");
		label_5.setBounds(10, 60, 81, 15);
		panel.add(label_5);

		add_birth = new JTextField();
		add_birth.setBounds(77, 57, 66, 21);
		panel.add(add_birth);
		add_birth.setColumns(10);

		label_6 = new JLabel("格式：2008-01-01");
		label_6.setBounds(154, 60, 130, 15);
		panel.add(label_6);

		label_7 = new JLabel("密码：");
		label_7.setBounds(294, 60, 54, 15);
		panel.add(label_7);

		add_pwd = new JLabel("123456");
		add_pwd.setBounds(342, 60, 54, 15);
		panel.add(add_pwd);

		add_btn = new JButton("添加学生");

		add_btn.setBounds(414, 56, 93, 23);
		panel.add(add_btn);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("修改学生", null, panel_1, null);
		panel_1.setLayout(null);

		label_8 = new JLabel("学号：");
		label_8.setBounds(20, 10, 54, 15);
		panel_1.add(label_8);

		update_no = new JLabel("");
		update_no.setBounds(64, 10, 54, 15);
		panel_1.add(update_no);

		label_9 = new JLabel("学生姓名：");
		label_9.setBounds(227, 10, 66, 15);
		panel_1.add(label_9);

		update_name = new JTextField();
		update_name.setBounds(342, 7, 66, 21);
		panel_1.add(update_name);
		update_name.setColumns(10);

		label_10 = new JLabel("性别：");
		label_10.setBounds(20, 51, 54, 15);
		panel_1.add(label_10);

		up_man_radio = new JRadioButton("男");
		buttonGroup_1.add(up_man_radio);
		up_man_radio.setBounds(62, 47, 56, 23);
		panel_1.add(up_man_radio);

		up_woman_radio = new JRadioButton("女");
		buttonGroup_1.add(up_woman_radio);
		up_woman_radio.setBounds(121, 47, 46, 23);
		panel_1.add(up_woman_radio);

		label_11 = new JLabel("出生日期：");
		label_11.setBounds(229, 51, 77, 15);
		panel_1.add(label_11);

		update_birth = new JTextField();
		update_birth.setBounds(342, 48, 66, 21);
		panel_1.add(update_birth);
		update_birth.setColumns(10);

		label_12 = new JLabel("密码：");
		label_12.setBounds(20, 87, 54, 15);
		panel_1.add(label_12);

		update_pwd = new JTextField();
		update_pwd.setBounds(69, 84, 66, 21);
		panel_1.add(update_pwd);
		update_pwd.setColumns(10);

		update_btn = new JButton("修改学生");
		update_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		update_btn.setBounds(184, 83, 93, 23);
		panel_1.add(update_btn);

		cancel_btn = new JButton("注销学生");
		
		cancel_btn.setBounds(333, 83, 93, 23);
		panel_1.add(cancel_btn);
		// 显示
		frame.setVisible(true);

		JButton btn_toScoreManager = new JButton("进入成绩管理");
	
		btn_toScoreManager.setBounds(503, 25, 126, 23);
		frame.getContentPane().add(btn_toScoreManager);
		scrollPane = new JScrollPane();
		scrollPane.setBounds(51, 93, 524, 112);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		
		// 功能1：一进来页面显示所有的学生列表
		// 填充表格数据
		List<Student> list = studentService.getAll();
		showData(list);

		// 功能2：查询按钮功能
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 获取输入的学号
				String studentNo = txt_noSearch.getText().trim();
				// 获取输入的姓名关键字
				String key = txt_nameKey.getText().trim();

				// 判断
				if (!"".equals(studentNo) && "".equals(key)) { // 只填写学号 没有填写关键字
					Student student = studentService.getByStudentNo(studentNo);
					if (student != null) {
						// 填充表格数据
						List<Student> list = new ArrayList<>();
						list.add(student);
						showData(list);
					} else {
						JOptionPane.showMessageDialog(null, "查无此人");
					}
				} else if ("".equals(studentNo) && !"".equals(key)) { // 没有填写学号 只填写关键字
					List<Student> list = studentService.getLikeByStudentName(key);
					if (list.size() > 0) {
						// 填充表格数据
						showData(list);
					} else {
						JOptionPane.showMessageDialog(null, "查无相关数据");
					}
				} else {
					// 查所有
					List<Student> list = studentService.getAll();
					showData(list);
				}
			}
		});

		// 功能3：新增学生
		// 3.1对学号做失去焦点事件
		add_no.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				String studentNo = add_no.getText().trim();
				Student byStudentNo = studentService.getByStudentNo(studentNo);
				if (byStudentNo != null) {
					JOptionPane.showMessageDialog(null, "学号已经存在，请重新输入");
					flag = false;
				} else {
					flag = true;
				}
			}
		});
		// 3.2添加
		add_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// 获取新增表单中输入的数据
				String studentNo = add_no.getText().trim();
				String studentName = add_name.getText().trim();
				String birth = add_birth.getText().trim();

				// 输入合法
				if (!"".equals(studentNo) && !"".equals(studentName) && !"".equals(birth) && flag == true) {
					String password = add_pwd.getText().trim();
					// 性别获取
					String gender = null;
					if (add_man_radio.isSelected()) {
						gender = "男";
					} else if (add_woman_radio.isSelected()) {
						gender = "女";
					}
					// 调用新增学生的业务
					int n = studentService.add(new Student(studentNo, studentName, gender, birth, password, 0));
					if (n > 0) {
						JOptionPane.showMessageDialog(null, "新增学生成功");
					} else {
						JOptionPane.showMessageDialog(null, "新增失败");
					}
				} else {
					JOptionPane.showMessageDialog(null, "请输入完整的数据");
				}
			}
		});
		
		// 功能4：修改学生信息
		// 4.1 先选中学生栏，填充到学生修改表单中
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				/*if(e.getClickCount()==2) {
					// 双击
				}*/
				// 获取当前表格中点中的行
				int index = table.getSelectedRow();
				// 根据点中的行获取该行的每个列 
				String studentNo = (String) table.getValueAt(index, 0);
				String studentName = (String) table.getValueAt(index, 1);
				String gender = (String) table.getValueAt(index, 2);
				String birth = (String) table.getValueAt(index, 3);
				String password = (String) table.getValueAt(index, 4);
				// 将取到的值赋值到指定的位置上
				update_no.setText(studentNo);
				update_name.setText(studentName);
				update_birth.setText(birth);
				update_pwd.setText(password);
				if ("男".equals(gender)) {
					up_man_radio.setSelected(true);
				}else {
					up_woman_radio.setSelected(true);
				}
			}
		});
		// 4.2 修改功能
		update_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 获取修改表单中输入的内容
				
				// 调用修改业务
			}
		});
		// 功能5：注销
		cancel_btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 获取学号
				
				// 调用根据学号修改状态的业务
			}
		});
		
		// 功能6：切换到成绩管理界面
		btn_toScoreManager.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TeacherFrame_ScoreManager window = new TeacherFrame_ScoreManager();
			}
		});
	}

	/**
	 * 为表格填充数据
	 */
	private void showData(List<Student> list) {
		Object[][] data = new Object[list.size()][6];
		for (int i = 0; i < list.size(); i++) {
			Student student = list.get(i);
			data[i][0] = student.getStudentNo();
			data[i][1] = student.getStudentName();
			data[i][2] = student.getGender();
			data[i][3] = student.getBirth();
			data[i][4] = student.getPassword();
			data[i][5] = student.getStatus();
		}

		table.setModel(new DefaultTableModel(data, new String[] { "学生编号", "学生姓名", "性别", "出生日期", "密码", "状态" }) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		scrollPane.setViewportView(table);
	}

}
