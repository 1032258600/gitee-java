package com.etc.service;

import java.util.List;
import com.etc.pojo.Product;

public interface ProductService {
	/**
	 * 查询所有商品
	 * 
	 * @param
	 * @return 商品对象集合
	 */
	List<Product> getAllProducts();

	/**
	 * 根据ID查询单个商品
	 * 
	 * @param  商品id
	 * @return 商品对象
	 */
	Product getProductById(Integer productId);

	/**
	 * ③增加商品业务
	 * @param product
	 * @return true修改成功 false修改失败
	 *         可以是int也可以是boolean，也可以是增加完的对象
	 */
	boolean addProduct(Product product);

	/**
	 * ④根据商品ID修改商品业务
	 * 
	 * @param product 修改的新对象信息
	 * @return 修改后的商品对象
	 */
	Product updateProductById(Product product);

	/**
	 * ④根据商品ID删除商品业务
	 * 
	 * @param product
	 * @return true修改成功 false修改失败
	 */
	boolean deleteProductById(Integer productId);

	/**
	 * 根据商品名模糊查询
	 * 
	 * @param 商品名关键词
	 * @return 集合
	 */
	List<Product> getProductsLikeName(String productNameKey);

	/**
	 * 查询商品列表总数
	 * 
	 * @return 总记录数
	 */
	int getCount();

	/**
	 * 根据商品数量排序 降序
	 * 
	 * @param
	 * @return 集合
	 */
	List<Product> getListOrderByCountDesc();

	/**
	 * 根据商品价格排序降序
	 * 
	 * @param
	 * @return 集合
	 */
	List<Product> getListOrderByPriceDesc();
	
	/**
	 * 拓展：
	 * 排序业务，排序的条件不确定
	 * 有可能数量，有可能价格
	 * @param condition 排序的字段
	 * @return 排序结果列表
	 */
	List<Product> getListOrderByCondition(String condition);
	
	
}
