package com.etc.service.impl;

import com.etc.dao.UserinfoDao;
import com.etc.dao.impl.UserinfoDaoImpl;
import com.etc.pojo.Userinfo;
import com.etc.service.UserinfoService;
/**
 * Userinfo业务接口的实现类
 * 
 * @author Administrator
 *
 */
public class UserinfoServiceImpl implements UserinfoService{

	// 声明Dao
	private UserinfoDao userDao = new UserinfoDaoImpl();
	
	@Override
	public Userinfo register(Userinfo userinfo) {
		// 先去判断用户名是否已经存在
		Userinfo user = userDao.queryByUsername(userinfo.getUsername());
		if (user == null) { // 用户名不存在
			// 才能去调用添加方法
			userDao.insert(userinfo);
			user = userDao.queryByUsername(userinfo.getUsername());
			return user; // 返回注册成功后的对象
		}
		return null;// 没有注册成功
	}

	@Override
	public Userinfo login(String username, String password) {

		return userDao.queryByUsernameAndPwd(username, password);
	}

	@Override
	public boolean delete(Integer userId) {
		
		return userDao.delete(userId) > 0 ? true : false;
	}

	@Override
	public Userinfo modifyInfo(Userinfo userinfo) {
	
		int n = userDao.update(userinfo);
		if (n > 0) {
			// 修改成功 查询修改后的记录
			return userDao.queryById(userinfo.getUserId());
		}
		return null;
	}

	@Override
	public Userinfo modifyPassword(Integer userId, String password) {
		int n = userDao.updatePassword(userId, password);
		if (n > 0) {
			// 修改成功 查询修改后的记录
			return userDao.queryById(userId);
		}
		return null;
	}

}
