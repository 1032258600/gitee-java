package com.etc.service.impl;

import java.util.List;
import com.etc.dao.ProductDao;
import com.etc.dao.impl.ProductDaoImpl;
import com.etc.pojo.Product;
import com.etc.service.ProductService;

public class ProductServiceImpl implements ProductService {

	// 声明Dao
	ProductDao dao = new ProductDaoImpl();
	
	@Override
	public List<Product> getAllProducts() {
		return dao.query();
	}

	@Override
	public Product getProductById(Integer productId) {
		return dao.queryById(productId);
	}

	@Override
	public boolean addProduct(Product product) {
		return dao.add(product) > 0  ? true : false;
	}

	@Override
	public Product updateProductById(Product product) {
		int n = dao.updateById(product);
		if (n > 0) {
			// 修改后的对象
			return dao.queryById(product.getProductId());
		}
		return null;
	}

	@Override
	public boolean deleteProductById(Integer productId) {
		return dao.deleteById(productId)> 0  ? true : false;
	}

	@Override
	public List<Product> getProductsLikeName(String productNameKey) {
		return dao.queryLikeName(productNameKey);
	}

	@Override
	public int getCount() {
		return dao.count();
	}

	@Override
	public List<Product> getListOrderByCountDesc() {
		return dao.orderByCountDesc();
	}

	@Override
	public List<Product> getListOrderByPriceDesc() {
		return dao.orderByPriceDesc();
	}

	@Override
	public List<Product> getListOrderByCondition(String condition) {
		
		return dao.orderByColumn(condition);
	}
}
