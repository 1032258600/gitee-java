package com.etc.service;

import com.etc.pojo.Userinfo;

/**
 * Userinfo业务接口
 * @author Administrator
 *
 */
public interface UserinfoService {

	/**
	 * 用户注册
	 * @param userinfo 用户对象
	 * @return 注册后的对象
	 */
	public Userinfo register(Userinfo userinfo);
	
	/**
	 * 登录
	 * @param username 用户名
	 * @param password 密码
	 * @return 用户对象
	 */
	public Userinfo login(String username, String password);
	
	/**
	 * 用户注销
	 * @param userId 用户编号
	 * @return 是否注销成功
	 */
	public boolean delete(Integer userId);
	
	/**
	 * 修改用户信息(用户名/密码/年龄)
	 * @param userinfo
	 * @return 修改后的对象
	 */
	public Userinfo modifyInfo(Userinfo userinfo);
	
	/**
	 * 修改用户信息密码
	 * @param userinfo
	 * @return 修改后的对象
	 */
	public Userinfo modifyPassword(Integer userId, String password);
	
}
