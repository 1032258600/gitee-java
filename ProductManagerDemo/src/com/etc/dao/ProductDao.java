package com.etc.dao;

import java.util.List;

import com.etc.pojo.Product;

public interface ProductDao {
	/**
	 * 查询所有商品
	 * @param  
	 * @return 集合
	 */
	List<Product> query();
	/**
	 * 根据id查询商品
	 * @param  id
	 * @return 商品对象
	 */
	Product queryById(Integer productId);
	/**
	 * 添加商品
	 * @param product
	 * @return 影响行数
	 */
	int add(Product product);
	/**
	 * ④修改商品
	 * @param product
	 * @return 影响行数
	 */
	int updateById(Product product);
	/**
	 * 根据id删除商品
	 * @param  id
	 * @return 影响行数
	 */
	int deleteById(Integer productId);
	/**
	 * 根据商品名关键词查询商品
	 * @param  关键词
	 * @return 集合
	 */
	List<Product> queryLikeName(String productNameKey);
	
	/**
	 * 查询商品列表总数
	 * @return 总数
	 */
	int count();
	/**
	 * 根据商品数量排序 降序
	 * @param  关键词
	 * @return 集合
	 */
	List<Product> orderByCountDesc();
	/**
	 * 根据商品价格排序降序
	 * @param  关键词
	 * @return 集合
	 */
	List<Product> orderByPriceDesc();
	
	/**
	 * 拓展：专门的排序方法
	 * 根据不同列字段进行排序
	 * @return 
	 */
	List<Product> orderByColumn(String column);
}
