package com.etc.dao;

import com.etc.pojo.Userinfo;

/**
 * 持久层(数据库操作层)
 * 针对Userinfo表
 * @author Administrator
 *
 */
public interface UserinfoDao {

	/**
	 * 用户添加
	 * @param userinfo 用户对象
	 * @return 影响行数
	 */
	public int insert(Userinfo userinfo);
	
	/**
	 * 根据用户名和密码进行查询
	 * @param username 用户名
	 * @param password 密码
	 * @return 用户对象
	 */
	public Userinfo queryByUsernameAndPwd(String username, String password);
	
	
	/**
	 * 用户修改
	 * @param userinfo 用户对象
	 * @return 影响行数
	 */
	public int update(Userinfo userinfo); 
	
	/**
	 * 修改密码字段
	 * @param userId 用户编号
	 * @param password 密码
	 * @return 影响行数
	 */
	public int updatePassword(Integer userId, String password);
	
	
	/**
	 * 根据ID删除的方法
	 * @param  用户编号
	 * @return 影响行数
	 */
	public int delete(Integer userId); 
	
	/**
	 * 根据用户名查询
	 */
	public Userinfo queryByUsername(String username);
	
	/**
	 * 根据编号查询
	 */
	public Userinfo queryById(Integer userId);
}
