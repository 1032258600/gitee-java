package com.etc.pojo;

public class Userinfo {
	
	private Integer userId; // 用户编号
	private String username; // 用户名
	private String password; // 用户密码
	private Integer age; // 年龄
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Userinfo(Integer userId, String username, String password, Integer age) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.age = age;
	}
	public Userinfo() {
		super();
		
	}
	@Override
	public String toString() {
		return "Userinfo [userId=" + userId + ", username=" + username + ", password=" + password + ", age=" + age
				+ "]";
	}
	
}
