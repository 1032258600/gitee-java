package com.etc.pojo;

public class Product {
	private Integer productId; // 商品编号
	private String productName; // 商品名
	private Float price; // 用户密码
	private Integer count; // 年龄
	public Product(Integer productId, String productName, Float price, Integer count) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.count = count;
	}
	public Product() {
		
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price + ", count="
				+ count + "]";
	}
	
}
