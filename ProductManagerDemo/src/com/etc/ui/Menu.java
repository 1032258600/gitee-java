package com.etc.ui;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.etc.pojo.Product;
import com.etc.pojo.Userinfo;
import com.etc.service.ProductService;
import com.etc.service.UserinfoService;
import com.etc.service.impl.ProductServiceImpl;
import com.etc.service.impl.UserinfoServiceImpl;

public class Menu {

	// 调用业务层中的方法
	UserinfoService userService = new UserinfoServiceImpl();
	ProductService productService = new ProductServiceImpl();
	
	@Test
	public void 注册() {
		Userinfo userinfo = new Userinfo(null, "XXX", "111", 19);
		Userinfo newUser = userService.register(userinfo);
		System.out.println(newUser != null ? "注册成功" : "注册失败 用户名重复"  );
		System.out.println(newUser);
	}
	@Test
	public void 登录() {
		Userinfo loginUser = userService.login("XXX", "1115");
		System.out.println(loginUser != null ? "登录成功" : "登录失败,用户名或者密码"  );
		System.out.println(loginUser);
	}
	
	@Test
	public void 修改密码() {
		Userinfo user = userService.modifyPassword(18, "123456");
		System.out.println(user != null ? "修改密码成功" : "修改密码失败 可能不存在该用户"  );
		System.out.println(user);
	}
	
	@Test
	public void 修改() {
		Userinfo userinfo = new Userinfo(8, "XXX", "789", 39);
		Userinfo newUser = userService.modifyInfo(userinfo);
		System.out.println(newUser != null ? "修改成功" : "修改失败 可能不存在该用户");
		System.out.println(newUser);
	}
	@Test
	public void 删除用户() {
		
		boolean flag = userService.delete(18);
		System.out.println(flag ? "删除成功" : "删除失败可能不存在该用户");
		System.out.println(flag);
	}
	
	@Test
	public void 模糊查询商品() {
		List<Product> list = productService.getProductsLikeName("西");
		System.out.println(list);
	}
	@Test
	public void 商品数量() {
		int n =  productService.getCount();
		System.out.println(n);
	}
	@Test
	public void 排序1() {
		List<Product> list = productService.getListOrderByCountDesc();
		System.out.println(list);
	}
	@Test
	public void 排序2() {
		List<Product> list = productService.getListOrderByPriceDesc();
		System.out.println(list);
	}
	
	@Test
	public void 通用的排序方法() {
		List<Product> list = productService.getListOrderByCondition("count");
		System.out.println(list);
	}
	
}
